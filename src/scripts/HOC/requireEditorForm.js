import React, { Component } from "react";
import { connect } from "react-redux";
import { getFormValues } from "redux-form";
import * as actions from "redux/actions";
import { getClip } from "storage/Storage";
import _ from "lodash";

export default ChildComponent => {
  class ComposedComponent extends Component {
    state = {
      shouldRender: true
    };

    // Our component will render
    componentWillMount() {
      let data = _.merge(getFormValues("editor")(this.state), getClip());
      _.each(data, (item, key) => {
        this.props.updateForm("editor", key, item);
      });
    }

    render() {
      return (
        <ChildComponent
          {...this.props}
          editorForm={this.props.editorForm || getClip()}
        />
      );
    }
  }

  function mapStateToProps(state) {
    return {
      editorForm: getFormValues("editor")(state)
    };
  }

  return connect(
    mapStateToProps,
    actions
  )(ComposedComponent);
};
