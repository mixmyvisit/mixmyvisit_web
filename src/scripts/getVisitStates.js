const visitStates = {
  1: {
    title: "Work in progress",
    description:
      "Esta visita encontra-se em progresso, é possível editá-la no editor (adicionar imagens ou vídeos) e enviá-la para o render de forma a obter a visita.",
  },
  2: {
    title: "Pronta para renderizar",
    description:
      "Esta visita encontra-se em pronta para ser 'renderizada', a visita será enviada para o processo de renderização em poucos minutos.",
  },
  3: {
    title: "Renderizada",
    description:
      "Esta visita encontra-se 'renderizada', é possível fazer download da mesma, partilhá-la em redes sociais, sendo possível editá-la mais tarde no editor.",
  },
  4: {
    title: "À espera de aprovação",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non egestas ipsum. Curabitur vel elit dignissim, congue sapien quis, blandit lectus. Donec viverra mauris nec libero commodo rutrum.",
  },
  5: {
    title: "Aprovada",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non egestas ipsum. Curabitur vel elit dignissim, congue sapien quis, blandit lectus. Donec viverra mauris nec libero commodo rutrum.",
  },
  6: {
    title: "Rejeitda",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non egestas ipsum. Curabitur vel elit dignissim, congue sapien quis, blandit lectus. Donec viverra mauris nec libero commodo rutrum.",
  },
  7: {
    title: "Publicada",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non egestas ipsum. Curabitur vel elit dignissim, congue sapien quis, blandit lectus. Donec viverra mauris nec libero commodo rutrum.",
  },
  8: {
    title: "A renderizar",
    description:
      "Esta visita encontra-se no processo de renderização, este processo pode demorar alguns minutos, quando o processo terminar será notificado via Facebook Messenger e email.",
  },
  9: {
    title: "A ser processada no Youtube",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non egestas ipsum. Curabitur vel elit dignissim, congue sapien quis, blandit lectus. Donec viverra mauris nec libero commodo rutrum.",
  },
  10: {
    title: "Erro na renderização",
    description:
      "Esta visita obteve um erro durante o processo de renderiazação, tente novamente mais tarde e se o erro persistir contacte-nos via email.",
  },
  11: {
    title: "Erro no youtube",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non egestas ipsum. Curabitur vel elit dignissim, congue sapien quis, blandit lectus. Donec viverra mauris nec libero commodo rutrum.",
  },
  12: {
    title: "Erro a fazer upload",
    description:
      "Esta visita obteve um erro durante o processo de upload, tente novamente mais tarde e se o erro persistir contacte-nos via email.",
  },
  13: {
    title: "A ser processada no Youtube",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non egestas ipsum. Curabitur vel elit dignissim, congue sapien quis, blandit lectus. Donec viverra mauris nec libero commodo rutrum.",
  },
  14: {
    title: "Enviada para o renderer",
    description:
      "Esta visita acabou de ser enviada para o processo de renderização, sendo que o processo começará depois de uns segundos deste envio.",
  },
  15: {
    title: "To be removed",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam non egestas ipsum. Curabitur vel elit dignissim, congue sapien quis, blandit lectus. Donec viverra mauris nec libero commodo rutrum.",
  },
  16: {
    title: "Submetida",
    description:
      "Esta visita encontra-se submetida na plataforma MixMyVisit, é possível editá-la no editor (adicionar imagens ou vídeos) e enviá-la para o render de forma a obter a visita.",
  },
  17: {
    title: "A fazer download de assets",
    description:
      "Esta visita encontra-se no processo de download das suas assets, este processo pode demorar alguns minutos para terminar.",
  },
};

export default visitStates;
