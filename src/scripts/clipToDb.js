import _ from "lodash";

export const structureCreateClip = (name, formData) => {
  return {
    name: name,
    video_details: JSON.stringify({ title: name }),
    editorForm: JSON.stringify({ title: name }),
  };
};

export const structureSaveClip = (formData) => {
  let platforms = {};
  let template;
  let platformStructure = {
    clips: [],
    overlays: {
      titles: [],
      images: [],
    },
    audios: [],
  };
  let editorFormStructure = {
    id: {},
    title: {},
    style: {},
    platforms: [],
    video: [],
    overlays: [],
    audios: [],
  };
  let editor = _.cloneDeep(editorFormStructure);
  let videos = [];
  let overlays = [];
  let audios = [];
  editor.id = formData.id;
  editor.title = formData.title;
  editor.style = formData.style;

  formData.platforms = {
    Facebook: true,
  };

  //add platform
  if (formData.platforms) {
    editor.platforms = formData.platforms;
    _.each(formData.platforms, (platformData, platform) => {
      if (platformData === true) {
        platforms[platform.toLowerCase()] = _.cloneDeep(platformStructure);

        //add clips to platform
        if (formData.video) {
          let vol;
          _.each(formData.video, (videoData) => {
            if (videoData.muted === false || videoData.muted === 0) {
              vol = 1;
            } else {
              vol = 0;
            }
            platforms[platform.toLowerCase()].clips.push({
              url: videoData.url,
              trimStart: videoData.trimStart,
              duration: videoData.trimEnd - videoData.trimStart,
              volume: vol,
              comment: videoData.comment,
              location: videoData.location || null,
              timestamp: videoData.timestamp || null,
            });
          });
          editor.video = formData.video;
          videos = _.cloneDeep(formData.video);
        }
      }
    });
  }
  if (formData.style !== undefined) {
    template = formData.style.name;
  } else {
    template = formData.style;
  }
  return {
    name: formData.title,
    video_details: JSON.stringify({
      title: formData.title,
      template: template,
      platforms: platforms,
      processing: {
        transitions: [],
        effects: [],
      },
    }),
    editor_form: JSON.stringify({
      id: formData.id,
      title: formData.title,
      style: formData.style,
      platforms: formData.platforms,
      video: videos,
      overlays: overlays,
      audios: audios,
    }),
  };
};
