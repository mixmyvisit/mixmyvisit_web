export const setUser = (user) => {
  localStorage.setItem("user", JSON.stringify(user));
};

export const getUser = () => {
  return JSON.parse(localStorage.getItem("user"));
};

export const removeUser = () => {
  localStorage.removeItem("user");
};

export const setClip = (clip) => {
  const videoForm = [];

  // this formats the visit in a way that we'll be able to compare without any issues
  // like comparing two visits to find out one visit has a property that the other hasn't
  // we check first if the length of the clip exists and make the object to each clip/image that the visit has
  if (clip?.video?.length) {
    clip.video.forEach((item, index) => {
      videoForm.push({
        id: item.id ? item.id : index,
        comment: item.comment,
        duration: item.duration,
        img: item.img,
        muted: item.muted,
        thumbnail: item.thumbnail,
        trimEnd: item.trimEnd,
        trimStart: item.trimStart,
        type: item.type,
        url: item.url,
        volume: item.volume,
        location: item.location || null,
        timestamp: item.timestamp,
      });
    });

    // ascending sort the clips/images by their id
    videoForm.sort((a, b) => a.id - b.id);
    clip.video = videoForm;
  }

  localStorage.setItem("clip", JSON.stringify(clip));
};

export const getClip = () => {
  return JSON.parse(localStorage.getItem("clip")) || {};
};

export const removeClip = (callback) => {
  localStorage.removeItem("clip");
  callback();
};
