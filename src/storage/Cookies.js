export const getToken = () => {
  return getCookie("access_token");
};

export const getRefreshToken = () => {
  return getCookie("refreshToken");
};

export const setToken = (token, expires, refresh, refreshExpires) => {
  let duration = new Date();
  duration.setSeconds(duration.getSeconds() + expires);
  document.cookie = `access_token=${token}; max-age=${expires}; path=/;`;
  document.cookie = `refreshToken=${refresh}; max-age=${refreshExpires}; path=/;`;
};

export const removeToken = () => {
  document.cookie = `access_token=;path=/`;
  document.cookie = `refreshToken=;path=/`;
};

export const hasCookie = () => {
  if (!!getCookie("access_token")) {
    return true;
  } else {
    return false;
  }
};

export const getCookie = (cname) => {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) === " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
};
