import React from "react";
import { Route, Switch } from "react-router-dom";

import Frontpage from "pages/frontpage/Frontpage";
import FrontpageUserInfo from "pages/frontpage/components/FrontpageUserInfo";
import Editor from "pages/editor/Editor";
import Collection from "pages/collection/Collection";
import VisitItemEditor from "pages/VisitItemEditor";
import VisitConcluded from "pages/VisitConcluded";
import Login from "pages/auth/Login";
import ResetPassword from "pages/auth/ResetPassword";
import MessengerUploadContent from "pages/MessengerUploadContent";
import RedirectVisitUser from "pages/RedirectVisitUser";
import RedirectVisitNonUser from "pages/RedirectVisitNonUser";
import About from "pages/About";
import Team from "pages/Team";
import NoMatch from "pages/NoMatch";

// import TestEditor from "pages/editor/TestEditor";

const Routes = ({ location }) => {
  return (
    <Switch location={location}>
      <Route exact path="/" component={Frontpage} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/info" component={FrontpageUserInfo} />
      <Route exact path="/sobre" component={About} />
      <Route exact path="/equipa" component={Team} />
      <Route exact path="/colecao" component={Collection} />
      <Route exact path="/visita-concluida/:clip" component={VisitConcluded} />
      <Route exact path="/editor/:clip" component={Editor} />
      <Route exact path="/password-reset/:token" component={ResetPassword} />
      <Route
        exact
        path="/redirect/messenger/upload-conteudo/:tagId"
        component={MessengerUploadContent}
      />
      <Route
        exact
        path="/redirect/visita/personalizada/:tagId"
        component={RedirectVisitUser}
      />
      <Route
        exact
        path="/redirect/visita/nao-personalizada/:tagId"
        component={RedirectVisitNonUser}
      />
      <Route
        exact
        path="/editor/:clip/:specificClip"
        component={VisitItemEditor}
      />
      {/* <Route exact path="/test-editor/:clip" component={TestEditor} /> */}

      <Route component={NoMatch} />
    </Switch>
  );
};

export default Routes;
