import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import reduxThunk from "redux-thunk";
import reducers from "redux/reducers";
import reduxAxiosMiddleware from "redux/reduxAxiosMiddleware";
import { composeWithDevTools } from "redux-devtools-extension";

import { getUser } from "storage/Storage";
import ThemeContextProvider from "theme/ThemeContext";
// import { EditorContextProvider } from "scripts/useEditor";
import App from "./App";

const composeEnhancers = composeWithDevTools({
  trace: true,
  traceLimit: 25,
});

const store = createStore(
  reducers,
  { user: { userSuccess: getUser() } },
  composeEnhancers(applyMiddleware(reduxThunk, reduxAxiosMiddleware))
);

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <ThemeContextProvider>
        {/* <EditorContextProvider> */}
        <App />
      </ThemeContextProvider>
    </BrowserRouter>
  </Provider>,
  document.querySelector("#root")
);
