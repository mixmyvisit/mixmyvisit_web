import React, { Component, createRef } from "react";
import { withRouter } from "react-router-dom";
import axios from "axios";
import { compose } from "redux";
import { connect } from "react-redux";
import * as actions from "redux/actions";
import {
  Box,
  Flex,
  Button,
  Heading,
  Icon,
  Text,
  CircularProgress,
  Image,
} from "@chakra-ui/react";

import ReactPlayer from "react-player";
import { FiDownload, FiRotateCw } from "react-icons/fi";

import SEO from "components/Seo";
import PlayerControlsEdition from "components/players/elements/PlayerControlsEdition";
import PlayerPlayButton from "components/players/elements/PlayerPlayButton";
import VisitShare from "components/feedback/VisitShare";
import Error from "components/feedback/Error";

class DisplayVisitNonUser extends Component {
  visitItem = createRef();

  state = {
    errorRendering: false,
    isVideoRendered: null,
    error: false,
    visitState: 1,
    isBuffering: false,
    isPlaying: false,
    currentTime: 0.0,
    totalTime: 0.0,
    volume: 1,
  };

  render() {
    const {
      error,
      isVideoRendered,
      errorRendering,
      isBuffering,
      isPlaying,
      currentTime,
      volume,
      totalTime,
    } = this.state;

    const { visit } = this.props;

    return (
      <>
        <Box h="100%" py={[12, 20]} px={[8, 16, 20, 56, 60]}>
          <Flex flexDir="column" justify="center" align="center">
            {error ? (
              <Error />
            ) : isVideoRendered === false ? (
              <>
                <SEO title="Redirecionamento para Visita" />

                <CircularProgress
                  thickness="5px"
                  capIsRound={true}
                  isIndeterminate
                  trackColor="primary.100"
                  color="primary.500"
                  size={30}
                />
                <Heading as="h1" textAlign="center" pt={6}>
                  A sua visita encontra-se a ser processada no nosso{" "}
                  <i>render</i>.
                </Heading>
                <Text mb={4} fontWeight="bold">
                  Por favor não feche esta janela até este processo terminar.
                </Text>
                <Text mb={4}>
                  Este processo de renderização pode demorar alguns minutos pelo
                  que a página irá atualizar-se de 2 em 2 minutos
                  automaticamente ou então pode carregar no botão de
                  "recarregar" para verificar se o processo terminou.
                </Text>
                <Button
                  h="auto"
                  variant="transparent"
                  leftIcon={<Icon as={FiRotateCw} h={7} w={7} mr={3} />}
                  onClick={() => this._reloadPage()}
                >
                  Recarregar página
                </Button>
              </>
            ) : (
              <>
                {errorRendering ? (
                  <>
                    <SEO title="Erro no processo de render" />
                    <Image
                      w={24}
                      h="auto"
                      alt="Erro"
                      src={require(`assets/icons/ErrorVisits.svg`)}
                    />
                    <Heading as="h1" py={4}>
                      Pedimos desculpa mas ocorreu um erro a renderizar o seu
                      vídeo
                    </Heading>
                    <Text textAlign="center" pb={8}>
                      Algo correu mal no processo de renderização. Para tentar
                      novamente o processo terá de dirigir-se mais uma vez à
                      aplicação do MixMyVisit e terminar novamente a visita.
                    </Text>
                  </>
                ) : (
                  <>
                    <SEO title="Visita Simples Concluída" />

                    <Heading pb={6} as="h1">
                      A sua visita está pronta!
                    </Heading>
                    <Heading pb={10} as="h4">
                      Sugerimos que seja breve ao fazer download da mesma e, se
                      pretender, realizar a sua partilha nas diferentes redes
                      sociais visto que este tipo de visitas simples são
                      apagadas automaticamente após 15 minutos depois de serem
                      processadas no nosso <i>render</i>
                    </Heading>
                    <Box boxShadow="default" mb={8}>
                      <Flex justify="center" align="center" position="relative">
                        <ReactPlayer
                          width="100%"
                          height="100%"
                          ref={this.visitItem}
                          url={visit.download_url}
                          controls={false}
                          playing={isPlaying}
                          volume={volume}
                          onStart={this._play}
                          onEnded={this._onEnded}
                          onPlay={this._play}
                          onPause={this._pause}
                          onProgress={this._onProgress}
                          onDuration={this._onDuration}
                          onBuffer={() => this.setState({ isBuffering: true })}
                          onBufferEnd={() =>
                            this.setState({ isBuffering: false })
                          }
                          onClick={isPlaying ? this._play : this._pause}
                        />
                        {isBuffering && isPlaying && (
                          <Box
                            zIndex="3"
                            minWidth="2.5rem"
                            position="absolute"
                            bg="rgba(255, 255, 255, 0.5)"
                            py="1.5rem"
                            px={0}
                            h={["4.5rem", "5rem", "7rem"]}
                            w={["4.5rem", "5rem", "7rem"]}
                            borderRadius="9999px"
                            display="inline-flex"
                            justifyContent="center"
                            alignItems="center"
                            boxShadow="0 1px 1px rgb(0 0 0 / 8%), 0 2px 2px rgb(0 0 0 / 8%), 0 4px 4px rgb(0 0 0 / 8%), 0 6px 6px rgb(0 0 0 / 8%), 0 8px 10px rgb(0 0 0 / 8%)"
                          >
                            <CircularProgress
                              isIndeterminate
                              thickness="10px"
                              color="primary.500"
                              trackColor="primary.100"
                            />
                          </Box>
                        )}
                        {!isPlaying && (
                          <PlayerPlayButton onClick={() => this._play()} />
                        )}
                      </Flex>
                      <PlayerControlsEdition
                        onVolumeChange={(value) => this._onVolumeChange(value)}
                        volume={volume}
                        duration={totalTime}
                        onPlay={this._play}
                        isPlaying={isPlaying}
                        onPause={this._pause}
                        currentTime={currentTime}
                        onSeek={this._onSeek}
                      />
                    </Box>
                    <Flex pb={6} w="100%" justify="center" align="center">
                      <a href={visit.download_url} download>
                        <Button
                          py={2}
                          px={6}
                          h="auto"
                          mb={[6, 0]}
                          variant="transparent"
                          leftIcon={
                            <Icon as={FiDownload} boxSize="30px" mr={2} />
                          }
                        >
                          Download
                        </Button>
                      </a>
                    </Flex>
                    <Flex>
                      <VisitShare
                        url={visit.download_url}
                        title="A minha visita"
                      />
                    </Flex>
                  </>
                )}
              </>
            )}
          </Flex>
        </Box>
      </>
    );
  }

  componentDidMount() {
    const getParams = this.props.match;
    const { tagId } = getParams.params;

    this.props.getVisitWithTagIdNonUser(
      { tagID: tagId },
      async (visit) => {
        try {
          await axios.post(
            `https://mixmyvisit.web.ua.pt/api/mixmvs/verify-redirect-token`,
            { tagID: tagId }
          );

          if (visit.states_id === 3) {
            this.setState({
              isVideoRendered: true,
              errorRendering: false,
            });
          } else if (
            visit.states_id === 10 ||
            visit.states_id === 11 ||
            visit.states_id === 12
          ) {
            this.setState({
              isVideoRendered: false,
              errorRendering: true,
            });
          } else {
            setTimeout(() => {
              this._reloadPage();
            }, 120000);

            this.setState({ isVideoRendered: false, errorRendering: false });
          }
        } catch (_) {
          this.setState({ error: true });
        }
      },
      () => {
        this.setState({ error: true });
      }
    );
  }

  _reloadPage = () => {
    window.location.reload();
  };

  _onProgress = (progress) => {
    this.setState({ currentTime: progress.playedSeconds });
  };

  _onDuration = (durationSeconds) => {
    this.setState({ totalTime: durationSeconds });
  };

  _onStalled = () => {
    this.setState({ isPlaying: false });
  };

  _onEnded = () => {
    if (this.props.onEnded) {
      this.props.onEnded();
    }
  };

  _pause = () => this.setState({ isPlaying: false });

  _play = () => this.setState({ isPlaying: true });

  _onSeek = (time) => {
    this.setState({ currentTime: time }, () => {
      this.visitItem.current.seekTo(time);
    });
  };

  _onVolumeChange = (value) => this.setState({ volume: value });
}

function mapStateToProps(state) {
  return {
    visit: state.visitTagNonUser.getVisitTagNonUserSuccess,
  };
}

export default compose(
  withRouter,
  connect(mapStateToProps, actions)
)(DisplayVisitNonUser);
