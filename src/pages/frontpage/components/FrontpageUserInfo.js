import React, { useRef } from "react";
import { useIntersection } from "react-use";
import { config } from "react-spring";
import { Spring, animated } from "react-spring/renderprops";
import { Box, Image, Heading, Text } from "@chakra-ui/react";
import { Link } from "react-router-dom";

const FrontpageUserInfo = () => {
  const intersection = {
    root: null,
    rootMargin: "-50px",
    threshold: 0.5,
  };

  const aboutRef = useRef(null);
  const teamRef = useRef(null);
  const aboutIntersection = useIntersection(aboutRef, { ...intersection });
  const teamIntersection = useIntersection(teamRef, { ...intersection });

  return (
    <>
      <Box h="100vh" display="grid" placeItems="center">
        <Box textAlign="center">
          <Image
            h={64}
            mb={8}
            objectFit="contain"
            src="/logo/svg/mmv-logo-with-letters.svg"
            alt="MixMyVisit Logo"
          />
        </Box>
      </Box>

      <Box ref={aboutRef} py={16}>
        <Spring
          from={{ opacity: 0, y: -25 }}
          to={aboutIntersection?.isIntersecting && { opacity: 1, y: 0 }}
          config={config.slow}
        >
          {(animation) => (
            <animated.div style={animation}>
              <Heading as="h1" textAlign="center" pb={10}>
                Sobre
              </Heading>
              <Text pb={8}>
                A solução MixMyVisit permite a criação de vídeos automáticos das
                visitas efetuadas a espaços culturais (ex. museus, parques,
                exposições, entre outros) com base nos percursos e locais
                visitados pelos utilizadores. Combinando tecnologia NFC com
                soluções de criação de vídeos automáticos, o sistema permite a
                cada visitante receber um vídeo personalizado e adaptado à sua
                visita. Pode, ainda, partilhar as suas fotos e vídeos que serão
                automaticamente integrados no vídeo da sua visita.
              </Text>
              <Text textAlign="center">
                <Link to="/team">Vê mais na página Sobre</Link>
              </Text>
            </animated.div>
          )}
        </Spring>
      </Box>

      <Box ref={teamRef} py={16}>
        <Spring
          from={{ opacity: 0, y: -25 }}
          to={teamIntersection?.isIntersecting && { opacity: 1, y: 0 }}
          config={config.slow}
        >
          {(animation) => (
            <animated.div style={animation}>
              <Heading as="h1" textAlign="center" pb={10}>
                Equipa
              </Heading>
              <Text pb={8}>
                O projeto MixMyVisit é o resultado da colaboração de uma equipa
                do grupo Social iTV da unidade de investigação Digimedia -
                Digital Media and Interaction da Universidade de Aveiro com a
                equipa da Altice Labs - Aveiro.
              </Text>
              <Text textAlign="center">
                <Link to="/team">Vê mais na página Equipa</Link>
              </Text>
            </animated.div>
          )}
        </Spring>
      </Box>
    </>
  );
};

export default FrontpageUserInfo;
