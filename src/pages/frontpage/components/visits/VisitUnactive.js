import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import * as actions from "redux/actions";
import { useLocation, withRouter } from "react-router";
import moment from "moment";
import localization from "moment/locale/pt";

import getVisitStates from "scripts/getVisitStates";
import EditButton from "components/buttons/EditButton";

import {
  Box,
  Flex,
  SimpleGrid,
  Icon,
  IconButton,
  Tag,
  Image,
  Heading,
  Text,
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverArrow,
  PopoverCloseButton,
  PopoverBody,
  HStack,
} from "@chakra-ui/react";
import { Link } from "react-router-dom";
import { FiDownload } from "react-icons/fi";

const UnactiveVisit = (props) => {
  const location = useLocation();
  const visitState = props.data.states_id;
  const editorForm = JSON.parse(props.data.editor_form);

  const imgUrl = editorForm?.video?.length
    ? editorForm?.video[0]?.thumbnail
    : require("assets/img/mvm-thumbnail-preview.png");

  const imagesInVisit = editorForm?.video?.length
    ? editorForm?.video.filter((item) => item.type === "image").length
    : 0;
  const videosInVisit = editorForm?.video?.length
    ? editorForm?.video.filter((item) => item.type === "video").length
    : 0;

  return (
    <SimpleGrid
      pb={6}
      px={[2, null, 12, 28]}
      columns={[1, null, 2]}
      gridGap={[6, 12]}
    >
      <Box
        w="100%"
        d="inline-block"
        position="relative"
        _after={{
          paddingTop: "56.25%",
          display: "block",
          content: "'*'",
        }}
        boxShadow="default"
      >
        <Box position="absolute" top="0" left="0" right="0" bottom="0">
          {visitState === 3 ? (
            <Link
              to={{
                pathname: `/visita-concluida/${props.data.name}=${props.data.id}`,
                state: { prevPath: location.pathname },
              }}
              onClick={() => props.saveCliptoStorage(editorForm)}
            >
              <IconButton
                py={0}
                px={0}
                bg="white"
                isRound={true}
                position="absolute"
                boxShadow="default"
                transition="all 0.5s ease-in-out"
                boxSize="50px"
                top="5%"
                right="3%"
                _hover={{
                  bg: "#bfbfbf",
                }}
                _active={{
                  bg: "#bfbfbf",
                }}
              >
                <Icon as={FiDownload} color="primary.500" boxSize={6} />
              </IconButton>
            </Link>
          ) : (
            <EditButton
              destinyLink={`/editor/${props.data.name}=${props.data.id}`}
              onClick={() => props.saveCliptoStorage(editorForm)}
            />
          )}
          <Image
            w="100%"
            h="100%"
            objectFit="cover"
            alt="Unactive Visit"
            src={
              imgUrl ? imgUrl : require("assets/img/mvm-thumbnail-preview.png")
            }
          />
        </Box>
      </Box>
      <Flex
        flexDir="column"
        justifyContent="center"
        placeItems="flex-start"
        textAlign={["center", "left"]}
      >
        <Heading pb={3} as="h3">
          {props.data.name}
        </Heading>
        <HStack spacing={3} mb={3}>
          <Popover>
            <PopoverTrigger>
              <Tag
                p={2}
                as="button"
                size="md"
                variant="solid"
                colorScheme={
                  [6, 10, 11, 12].indexOf(visitState) > -1
                    ? "red"
                    : [4, 8, 9, 13, 14, 17].indexOf(visitState) > -1
                    ? "yellow"
                    : "green"
                }
              >
                {getVisitStates[visitState].title}
              </Tag>
            </PopoverTrigger>
            <PopoverContent>
              <PopoverArrow />
              <PopoverCloseButton />
              <PopoverBody mt={3} p={4}>
                {getVisitStates[visitState].description}
              </PopoverBody>
            </PopoverContent>
          </Popover>
          <Tag
            p={2}
            size="md"
            variant="solid"
            bgColor={videosInVisit === 0 ? "grey.400" : "primary.300"}
          >
            {videosInVisit} vídeos
          </Tag>
          <Tag
            p={2}
            size="md"
            variant="solid"
            bgColor={imagesInVisit === 0 ? "grey.400" : "primary.300"}
          >
            {imagesInVisit} imagens
          </Tag>
        </HStack>

        <Text>
          {moment(props.data.created_at)
            .locale("pt", localization)
            .format("D MMMM YYYY")}
        </Text>
      </Flex>
    </SimpleGrid>
  );
};

export default compose(connect(null, actions), withRouter)(UnactiveVisit);
