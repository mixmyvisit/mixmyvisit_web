import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import * as actions from "redux/actions";
import { useWindowSize } from "react-use";
import { withRouter } from "react-router";
import moment from "moment";
import localization from "moment/locale/pt";

import getVisitStates from "scripts/getVisitStates";
import EditButton from "components/buttons/EditButton";
import {
  Box,
  Tag,
  Image,
  Heading,
  Text,
  HStack,
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverArrow,
  PopoverCloseButton,
  PopoverBody,
} from "@chakra-ui/react";

const ActiveVisit = (props) => {
  // using UseWindowSize hook to get the window width to make conditional styling
  // in this case if the width is less than 768px (medium breakpoint in theme) the info of the visit, title and submitted_at aren't positioned in a overlay in the top of the image
  // its simply a new element beneath the image with some background
  // the rest of the conditional styling consists in determining the box-shadow of the containers
  const visitState = props.data.states_id;
  const editorForm = JSON.parse(props.data.editor_form);
  console.log(editorForm);
  console.log(props.data.video);
  const imgUrl = editorForm?.video?.length
    ? editorForm?.video[0]?.thumbnail
    : require("assets/img/mvm-thumbnail-preview.png");

  const imagesInVisit = editorForm?.video?.length
    ? editorForm?.video.filter((item) => item.type === "image").length
    : 0;
  const videosInVisit = editorForm?.video?.length
    ? editorForm?.video.filter((item) => item.type === "video").length
    : 0;

  const { width } = useWindowSize();

  return (
    <Box
      mb={[10, 12]}
      mx={[2, null, 0]}
      px={[0, null, 12, 28]}
      boxShadow={width >= 768 ? "none" : "default"}
    >
      <Box
        w="100%"
        d="inline-block"
        position="relative"
        _after={{
          paddingTop: "56.25%",
          display: "block",
          content: "'*'",
        }}
        boxShadow={width >= 768 ? "default" : "none"}
      >
        <Box position="absolute" top="0" left="0" right="0" bottom="0">
          <EditButton
            destinyLink={`/editor/${props.data.name}=${props.data.id}`}
            onClick={() => props.saveCliptoStorage(editorForm)}
          />
          {width >= 768 && (
            <Box
              p={6}
              w="100%"
              left={0}
              bottom={0}
              textAlign="center"
              position="absolute"
              bg="rgba(255, 255, 255, 0.40)"
            >
              <Heading pb={3} as="h3">
                {props.data.name}
              </Heading>
              <HStack justify="center" spacing={3} mb={3}>
                <Popover>
                  <PopoverTrigger>
                    <Tag
                      p={2}
                      as="button"
                      size={["md"]}
                      variant="solid"
                      colorScheme={
                        [6, 10, 11, 12].indexOf(visitState) > -1
                          ? "red"
                          : [4, 8, 9, 13, 14, 17].indexOf(visitState) > -1
                          ? "yellow"
                          : "green"
                      }
                    >
                      {getVisitStates[visitState].title}
                    </Tag>
                  </PopoverTrigger>
                  <PopoverContent>
                    <PopoverArrow />
                    <PopoverCloseButton />
                    <PopoverBody mt={3} p={4}>
                      {getVisitStates[visitState].description}
                    </PopoverBody>
                  </PopoverContent>
                </Popover>
                <Tag
                  p={2}
                  size={["md"]}
                  variant="solid"
                  bgColor={videosInVisit === 0 ? "grey.400" : "primary.300"}
                >
                  {videosInVisit} vídeos
                </Tag>
                <Tag
                  p={2}
                  size={["md"]}
                  variant="solid"
                  bgColor={imagesInVisit === 0 ? "grey.400" : "primary.300"}
                >
                  {imagesInVisit} imagens
                </Tag>
              </HStack>
              <Text>
                {moment(props.data.created_at)
                  .locale("pt", localization)
                  .format("D MMMM YYYY")}
              </Text>
            </Box>
          )}
          <Image
            w="100%"
            h="100%"
            objectFit="cover"
            alt="Active Visit"
            src={
              imgUrl ? imgUrl : require("assets/img/mvm-thumbnail-preview.png")
            }
          />
        </Box>
      </Box>
      {width < 768 && (
        <Box bg="grey.400" textAlign="center" p={6}>
          <Heading pb={2} as="h2">
            {props.data.name}
          </Heading>
          <HStack justify="center" spacing={3} mb={3}>
            <Popover>
              <PopoverTrigger>
                <Tag
                  p={2}
                  as="button"
                  size="md"
                  variant="solid"
                  colorScheme={
                    [6, 10, 11, 12].indexOf(visitState) > -1
                      ? "red"
                      : [4, 8, 9, 13, 14, 17].indexOf(visitState) > -1
                      ? "yellow"
                      : "green"
                  }
                >
                  {getVisitStates[visitState].title}
                </Tag>
              </PopoverTrigger>
              <PopoverContent>
                <PopoverArrow />
                <PopoverCloseButton />
                <PopoverBody mt={3} p={4}>
                  {getVisitStates[visitState].description}
                </PopoverBody>
              </PopoverContent>
            </Popover>
            <Tag
              p={2}
              size="md"
              variant="solid"
              bgColor={videosInVisit === 0 ? "grey.400" : "primary.300"}
            >
              {videosInVisit}
              vídeos
            </Tag>
            <Tag
              p={2}
              size="md"
              variant="solid"
              bgColor={imagesInVisit === 0 ? "grey.400" : "primary.300"}
            >
              {imagesInVisit} imagens
            </Tag>
          </HStack>
          <Text>
            {moment(props.data.created_at)
              .locale("pt", localization)
              .format("D MMMM YYYY")}
          </Text>
        </Box>
      )}
    </Box>
  );
};

export default compose(connect(null, actions), withRouter)(ActiveVisit);
