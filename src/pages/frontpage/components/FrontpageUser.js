import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import * as actions from "redux/actions";
import { Link } from "react-router-dom";
import { Box, Flex, Button, Image, Heading } from "@chakra-ui/react";
import _ from "lodash";

import VisitActive from "pages/frontpage/components/visits/VisitActive";
import VisitUnactive from "pages/frontpage/components/visits/VisitUnactive";
import InfoContactBot from "components/feedback/InfoContactBot";
import Loading from "components/feedback/Loading";
import Error from "components/feedback/Error";
import SEO from "components/Seo";
import { getClip } from "storage/Storage";

const FrontpageUser = (props) => {
  const [state, setState] = useState({
    isLoading: true,
    error: null,
  });

  useEffect(() => {
    // make request to get visits
    // check if there was made a request earlier, if so compare with the visit that's in local storage
    // if there's something different make a new request, otherwise use the data from the past request

    if (props.userVisits.length > 0) {
      // if there are visits in the local storage make sure if the visits are equal
      const visit = getClip();

      if (!_.isEmpty(visit)) {
        const getVisit = props.userVisits.find((item) => item.id === visit.id);
        const parseGetVisit = JSON.parse(getVisit.editor_form);
        const checkIfVisitsAreEqual = _.isEqual(
          visit.video,
          parseGetVisit.video
        );

        if (checkIfVisitsAreEqual) {
          setState({ ...state, isLoading: false });
        } else {
          props.getUserClips(
            props.userData.id,
            () => setState({ ...state, isLoading: false }),
            () => {
              setState({ ...state, isLoading: false, error: true });
            }
          );
        }
      } else {
        setState({ ...state, isLoading: false });
      }
    } else {
      // if there are no visits in the local storage make a request to the API to get visits
      props.getUserClips(
        props.userData.id,
        () => setState({ ...state, isLoading: false }),
        () => {
          setState({ ...state, isLoading: false, error: true });
        }
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const { userVisits } = props;
  const { isLoading, error } = state;

  const activeVisit = Array.isArray(userVisits) ? userVisits?.filter((visit) => visit.active) : null;
  const unactiveVisits = Array.isArray(userVisits) ? userVisits?.filter((visit) => !visit.active) : null;
  const latestUnactiveVisits = Array.isArray(unactiveVisits) ? unactiveVisits?.slice(0, 4) : null;

  return isLoading ? (
    <Loading />
  ) : error ? (
    <Error />
  ) : (
    <>
      <SEO title="Home" />
      <Flex flexDir="column" justify="center" align="center">
        {!userVisits.length ? (
          <>
            <Heading as="h1">Olá!</Heading>
            <Heading as="h2">
              Parece que ainda não começou nenhuma visita
            </Heading>
            <InfoContactBot />
          </>
        ) : (
          <>
            <Box w="100%" pb={[8, 10]}>
              <Heading as="h1">Visita Ativa</Heading>
              {activeVisit?.length > 0 ? (
                <VisitActive data={activeVisit[0]} />
              ) : (
                <Box my={[8, 10]} px={[2, null, 12, 32]}>
                  <Heading as="h3" pb={2} w="100%" textAlign="center">
                    Neste momento não tem nenhuma visita ativa
                  </Heading>
                  <InfoContactBot />
                </Box>
              )}
            </Box>
            <Box w="100%">
              <Heading as="h2">Visitas Passadas Recentes</Heading>
              {unactiveVisits?.length > 0 ? (
                <>
                  {latestUnactiveVisits.map((visit) => (
                    <VisitUnactive key={visit.id} data={visit} />
                  ))}
                  {unactiveVisits.length > latestUnactiveVisits.length && (
                    <Flex
                      w="100%"
                      justify={["center", "flex-end"]}
                      align="center"
                    >
                      <Link to="/collection">
                        <Button
                          py={2}
                          px={6}
                          h="auto"
                          mb={[6, 0]}
                          variant="transparent"
                          rightIcon={
                            <Image
                              h={8}
                              ml={3}
                              alt="Back"
                              objectFit="contain"
                              transform="rotate(-180deg)"
                              src={require("assets/icons/Back.svg")}
                            />
                          }
                        >
                          Ver mais visitas na sua Coleção
                        </Button>
                      </Link>
                    </Flex>
                  )}
                </>
              ) : (
                <Box pb={6} px={[2, null, 12, 32]}>
                  <Heading as="h3" pb={2} w="100%" textAlign="center">
                    Neste momento não tem nenhuma visita passada
                  </Heading>
                  <InfoContactBot />
                </Box>
              )}
            </Box>
          </>
        )}
      </Flex>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    userData: state.user.userSuccess,
    userVisits: state.clipsUser.getUserClipsSuccess,
  };
};

export default connect(mapStateToProps, actions)(FrontpageUser);
