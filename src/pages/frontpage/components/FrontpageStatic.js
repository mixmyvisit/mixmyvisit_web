import React, { useRef } from "react";
import { useIntersection } from "react-use";
import { config } from "react-spring";
import { Spring, animated } from "react-spring/renderprops";
import { Box, Container, Image, Heading, Text } from "@chakra-ui/react";
import SEO from "components/Seo";

const FrontpageStatic = () => {
  const intersection = {
    root: null,
    rootMargin: "-50px",
    threshold: 0.5,
  };

  const aboutRef = useRef(null);
  const teamRef = useRef(null);
  const aboutIntersection = useIntersection(aboutRef, { ...intersection });
  const teamIntersection = useIntersection(teamRef, { ...intersection });

  return (
    <>
      <SEO title="Home" />

      <Box
        h="100vh"
        display="grid"
        placeItems="center"
        position="relative"
        _after={{
          content: '" "',
          bg: "url('/imgs/museum-frontpage-1.jpg') no-repeat center center",
          bgSize: "cover",
          opacity: 0.5,
          top: 0,
          left: 0,
          bottom: 0,
          right: 0,
          position: "absolute",
          zIndex: -1,
        }}
      >
        <Box textAlign="center">
          <Image
            h={64}
            mb={8}
            objectFit="contain"
            src="/logo/svg/mmv-logo-with-letters.svg"
            alt="MixMyVisit Logo"
          />
        </Box>
      </Box>
      <Container ref={aboutRef} px={4} py={24}>
        <Spring
          from={{ opacity: 0, y: -25 }}
          to={aboutIntersection?.isIntersecting && { opacity: 1, y: 0 }}
          config={config.slow}
        >
          {(animation) => (
            <animated.div style={animation}>
              <Heading as="h1" textAlign="center" pb={10}>
                Sobre
              </Heading>
              <Text>
                A solução MixMyVisit permite a criação de vídeos automáticos das
                visitas efetuadas a espaços culturais (ex. museus, parques,
                exposições, entre outros) com base nos percursos e locais
                visitados pelos utilizadores. Combinando tecnologia NFC com
                soluções de criação de vídeos automáticos, o sistema permite a
                cada visitante receber um vídeo personalizado e adaptado à sua
                visita. Pode, ainda, partilhar as suas fotos e vídeos que serão
                automaticamente integrados no vídeo da sua visita.
              </Text>
            </animated.div>
          )}
        </Spring>
      </Container>

      <Box
        h="50vh"
        bg="url('/imgs/museum-frontpage-2.jpg') no-repeat center center"
        bgSize="cover"
        boxShadow="inset 0 12px 12px -12px #696868, inset 0 -12px 12px -12px #696868"
      />

      <Container ref={teamRef} px={4} py={24}>
        <Spring
          from={{ opacity: 0, y: -25 }}
          to={teamIntersection?.isIntersecting && { opacity: 1, y: 0 }}
          config={config.slow}
        >
          {(animation) => (
            <animated.div style={animation}>
              <Heading as="h1" textAlign="center" pb={10}>
                Equipa
              </Heading>
              <Text>
                O projeto MixMyVisit é o resultado da colaboração de uma equipa
                do grupo Social iTV da unidade de investigação Digimedia -
                Digital Media and Interaction da Universidade de Aveiro com a
                equipa da Altice Labs - Aveiro.
              </Text>
            </animated.div>
          )}
        </Spring>
      </Container>
    </>
  );
};

export default FrontpageStatic;
