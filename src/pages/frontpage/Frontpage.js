import React from "react";
import { connect } from "react-redux";
import FrontpageStatic from "./components/FrontpageStatic";
import FrontpageUser from "./components/FrontpageUser";

const Frontpage = (props) => {
  const { userData } = props;

  if (!userData) {
    return <FrontpageStatic />;
  } else {
    return <FrontpageUser />;
  }
};

const mapStateToProps = (state) => {
  return {
    userData: state.user.userSuccess,
  };
};

export default connect(mapStateToProps, null)(Frontpage);
