import React, { Component, createRef } from "react";
import * as actions from "redux/actions";
import {
  Box,
  Button,
  CircularProgress,
  Divider,
  Flex,
  Heading,
  Icon,
  IconButton,
  Image,
  SimpleGrid,
  Text,
} from "@chakra-ui/react";

import { Helmet } from "react-helmet";
import ReactPlayer from "react-player";
import { FiDownload, FiRotateCw } from "react-icons/fi";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { compose } from "redux";
import moment from "moment";
import localization from "moment/locale/pt";

import SEO from "components/Seo";
import PlayerControlsEdition from "components/players/elements/PlayerControls";
import PlayerPlayButton from "components/players/elements/PlayerPlayButton";
import Loading from "components/feedback/Loading";
import Error from "components/feedback/Error";
import VisitShare from "components/feedback/VisitShare";
import requireAuth from "components/auth/requireAuth";
import requireEditorForm from "scripts/HOC/requireEditorForm";

class VisitConcluded extends Component {
  visitItem = createRef();

  state = {
    isLoading: true,
    error: false,
    visitEditorForm: null,
    visitId: null,
    createdAt: null,
    visitState: null,
    downloadUrl: null,
    isBuffering: false,
    isPlaying: false,
    currentTime: 0.0,
    totalTime: 0.0,
    volume: 1,
  };

  componentDidMount() {
    // verify if there is content in editorForm, if not its needed to do a call to the API to get the form info
    // this happens when the user refresh the page on the browser
    const visitId = this.props.match.params.clip.split("=");
    this.props.getClip(
      parseInt(visitId[1]),
      () => {
        const editorForm = JSON.parse(this.props.clip.editor_form);
        this.props.saveCliptoStorage(editorForm);

        this.setState({
          isLoading: false,
          visitEditorForm: editorForm,
          visitId: this.props.clip.id,
          createdAt: this.props.clip.created_at,
          visitState: this.props.clip.states_id,
          downloadUrl: this.props.clip.download_url,
        });

        const visitState = this.props.clip.states_id;
        if (visitState === 8 || visitState === 14 || visitState === 2) {
          setTimeout(() => {
            this._reloadPage();
          }, 120000);
        }
      },
      () => this.setState({ isLoading: false, error: true })
    );
  }

  render() {
    const {
      isLoading,
      error,
      downloadUrl,
      visitEditorForm,
      visitId,
      createdAt,
      visitState,
      isPlaying,
      currentTime,
      volume,
      totalTime,
      isBuffering,
    } = this.state;

    return isLoading ? (
      <Loading />
    ) : error ? (
      <Error />
    ) : visitState === 8 || visitState === 14 || visitState === 2 ? (
      <>
        <SEO title="Visita concluída" />
        <Flex justify="center" align="center" flexDir="column">
          <CircularProgress
            thickness="5px"
            capIsRound={true}
            isIndeterminate
            trackColor="primary.100"
            color="primary.500"
            size={20}
          />
          <Heading as="h1" textAlign="center" pt={6}>
            A sua visita, {visitEditorForm.title}, encontra-se a ser processada
            no nosso <i>render</i>.
          </Heading>
          <Text mb={4}>
            Quando o processo de renderização da sua visita terminar será
            notificado via <b>email</b> e pelo <b>Facebook Messenger</b>.
          </Text>
          <Text mb={4}>
            Este processo de renderização pode demorar alguns minutos pelo que a
            página irá atualizar-se de 2 em 2 minutos automaticamente ou então
            pode carregar no botão de "recarregar" para verificar se o processo
            terminou.
          </Text>
          <Button
            h="auto"
            variant="transparent"
            leftIcon={<Icon as={FiRotateCw} h={7} w={7} mr={3} />}
            onClick={() => this._reloadPage()}
          >
            Recarregar página
          </Button>
        </Flex>
      </>
    ) : visitState !== 3 ? (
      <>
        <SEO title={`Visita - ${visitEditorForm.title}`} />

        <Flex justify="center" align="center" flexDir="column">
          <Image
            w={24}
            h="auto"
            alt="Erro"
            src={require(`assets/icons/ErrorVisits.svg`)}
          />
          <Heading as="h1" textAlign="center" pt={6}>
            A sua visita, {visitEditorForm.title}, ainda não se encontra
            renderizada o que impede o acesso a esta página.
          </Heading>
          <Text mb={10}>
            Para renderizar a sua visita necessita de ir editar a mesma e
            carregar no botão, "Renderizar". Também pode começar o processo de
            renderização a partir do nosso bot no Facebook Messenger com o
            comando !concluir_visita, se a visita em questão estiver ativa.
          </Text>
          <SimpleGrid
            columns={[1, 2]}
            w="100%"
            justifyItems="center"
            alignItems="center"
          >
            <Flex
              justify="center"
              as={Link}
              to={this.props.location?.state?.prevPath || "/"}
            >
              <Button
                display="flex"
                variant="solid"
                alignItems="center"
                leftIcon={
                  <Image
                    alt="Voltar"
                    src={require("assets/icons/Back.svg")}
                    objectFit="contain"
                    mr={2}
                    h={5}
                  />
                }
              >
                Voltar
              </Button>
            </Flex>

            <Flex
              justify="center"
              as={Link}
              to={`/editor/${visitEditorForm.title}=${visitId}`}
              onClick={() => this.props.saveCliptoStorage(visitEditorForm)}
            >
              <Button
                display="flex"
                variant="solid"
                alignItems="center"
                leftIcon={
                  <Image
                    alt="Editor"
                    src={require("assets/icons/Edit.svg")}
                    objectFit="contain"
                    mr={2}
                    h={6}
                  />
                }
              >
                Ir para o editor
              </Button>
            </Flex>
          </SimpleGrid>
        </Flex>
      </>
    ) : (
      <>
        <Box pb={8}>
          <Helmet
            htmlAttributes={{ lang: "pt" }}
            title={`Visita - ${visitEditorForm.title}`}
            titleTemplate={`Visita - ${visitEditorForm.title} - MixMyVisit`}
            meta={[
              {
                name: `description`,
                content: `A solução MixMyVisit permite a criação de vídeos automáticos das
              visitas efetuadas a espaços culturais (ex. museus, parques,
              exposições, entre outros) com base nos percursos e locais
              visitados pelos utilizadores.`,
              },
              {
                property: `og:title`,
                content: `Visita - ${visitEditorForm.title}`,
              },
              {
                property: `og:description`,
                content: `A solução MixMyVisit permite a criação de vídeos automáticos das
              visitas efetuadas a espaços culturais (ex. museus, parques,
              exposições, entre outros) com base nos percursos e locais
              visitados pelos utilizadores.`,
              },
              {
                property: `og:type`,
                content: `website`,
              },
              {
                property: `og:video`,
                content: downloadUrl,
              },
              {
                property: `og:video:secure_url`,
                content: downloadUrl,
              },
              {
                property: `og:video:type`,
                content: `video/mp4`,
              },
              {
                property: `og:video:width`,
                content: `1920px`,
              },
              {
                property: `og:video:height`,
                content: `1080px`,
              },
              {
                property: `og:image`,
                content: visitEditorForm.video[0].thumbnail,
              },
              {
                property: `twitter:card`,
                content: `summary_large_image`,
              },
              {
                property: `twitter:image:alt`,
                content: `summary_large_image`,
              },
            ]}
          />

          <Flex
            flexDir="row"
            wrap="wrap"
            align="center"
            pb={[8, 12]}
            justify={["space-between", "center"]}
          >
            <Flex
              align="center"
              as={Link}
              to={this.props.location?.state?.prevPath || "/"}
            >
              <Button
                py={2}
                px={6}
                h="auto"
                mb={[6, 0]}
                left={[null, "5%"]}
                variant="transparent"
                position={["relative", "absolute"]}
                leftIcon={
                  <Image
                    h={8}
                    mr={3}
                    alt="Back"
                    objectFit="contain"
                    src={require("assets/icons/Back.svg")}
                  />
                }
              >
                Voltar
              </Button>
            </Flex>

            <Flex
              flexDir="column"
              justify="center"
              order="3"
              textAlign="center"
              w={["100%", "auto"]}
            >
              <Heading pb={2} as="h1">
                {visitEditorForm.title}
              </Heading>
              <Divider
                my={2}
                mx="auto"
                w={["50%", "100%"]}
                borderColor="primary.300"
              />
              <p>
                {moment(createdAt)
                  .locale("pt", localization)
                  .format("D MMMM YYYY")}
              </p>
            </Flex>

            <IconButton
              px={2}
              mb={[6, 0]}
              order="2"
              right={[null, "5%"]}
              variant="transparent"
              position={["relative", "absolute"]}
              icon={
                <Image
                  h={12}
                  alt="Info"
                  src={require("assets/icons/Info.svg")}
                  objectFit="contain"
                />
              }
            />
          </Flex>

          <Box boxShadow="default">
            <Flex justify="center" align="center" position="relative">
              <ReactPlayer
                width="100%"
                height="100%"
                ref={this.visitItem}
                url={downloadUrl}
                controls={false}
                playing={isPlaying}
                volume={volume}
                onStart={this._play}
                onEnded={this._onEnded}
                onPlay={this._play}
                onPause={this._pause}
                onProgress={this._onProgress}
                onDuration={this._onDuration}
                onBuffer={() => this.setState({ isBuffering: true })}
                onBufferEnd={() => this.setState({ isBuffering: false })}
                onClick={isPlaying ? this._play : this._pause}
              />
              {isBuffering && isPlaying && (
                <Box
                  zIndex="3"
                  minWidth="2.5rem"
                  position="absolute"
                  bg="rgba(255, 255, 255, 0.5)"
                  py="1.5rem"
                  px={0}
                  h={["4.5rem", "5rem", "7rem"]}
                  w={["4.5rem", "5rem", "7rem"]}
                  borderRadius="9999px"
                  display="inline-flex"
                  justifyContent="center"
                  alignItems="center"
                  boxShadow="0 1px 1px rgb(0 0 0 / 8%), 0 2px 2px rgb(0 0 0 / 8%), 0 4px 4px rgb(0 0 0 / 8%), 0 6px 6px rgb(0 0 0 / 8%), 0 8px 10px rgb(0 0 0 / 8%)"
                >
                  <CircularProgress
                    isIndeterminate
                    thickness="10px"
                    color="primary.500"
                    trackColor="primary.100"
                  />
                </Box>
              )}
              {!isPlaying && <PlayerPlayButton onClick={() => this._play()} />}
            </Flex>
            <PlayerControlsEdition
              onVolumeChange={(value) => this._onVolumeChange(value)}
              volume={volume}
              duration={totalTime}
              currentTime={currentTime}
              onPlay={this._play}
              isPlaying={isPlaying}
              onPause={this._pause}
              onSeek={this._onSeek}
            />
          </Box>
        </Box>
        <SimpleGrid
          columns={[1, 2]}
          pb={6}
          w="100%"
          justifyItems="center"
          alignItems="center"
        >
          <a href={downloadUrl}>
            <Button
              py={2}
              px={6}
              h="auto"
              mb={[6, 0]}
              variant="transparent"
              leftIcon={
                <Icon
                  as={FiDownload}
                  color="primary.500"
                  boxSize="30px"
                  mr={2}
                />
              }
            >
              <Heading pb={0} as="h2">
                Download
              </Heading>
            </Button>
          </a>
          <Link
            to={`/editor/${visitEditorForm.title}=${visitId}`}
            onClick={() => this.props.saveCliptoStorage(visitEditorForm)}
          >
            <Button
              py={2}
              px={6}
              h="auto"
              mb={[6, 0]}
              variant="transparent"
              leftIcon={
                <Image
                  htmlHeight="40px"
                  htmlWidth="40px"
                  alt="Edit"
                  src={require("assets/icons/Edit.svg")}
                />
              }
            >
              <Heading letterSpacing="0.04em" pb={0} as="h2">
                Editor
              </Heading>
            </Button>
          </Link>
        </SimpleGrid>
        <Flex w="100%" justify="center" align="center">
          <VisitShare url={downloadUrl} title={visitEditorForm.title} />
        </Flex>
      </>
    );
  }

  _reloadPage = () => {
    window.location.reload();
  };

  _onProgress = (progress) => {
    this.setState({ currentTime: progress.playedSeconds });
  };

  _onDuration = (durationSeconds) => {
    this.setState({ totalTime: durationSeconds });
  };

  _onStalled = () => {
    this.setState({ isPlaying: false });
  };

  _onEnded = () => {
    if (this.props.onEnded) {
      this.props.onEnded();
    }
  };

  _pause = () => this.setState({ isPlaying: false });

  _play = () => this.setState({ isPlaying: true });

  _onSeek = (time) => {
    this.setState({ currentTime: time }, () => {
      this.visitItem.current.seekTo(time);
    });
  };

  _onVolumeChange = (value) => this.setState({ volume: value });
}

function mapStateToProps(state) {
  return {
    clip: state.clip.getClipSuccess,
  };
}

export default compose(
  requireAuth,
  requireEditorForm,
  connect(mapStateToProps, actions)
)(VisitConcluded);
