import React, { useState, useEffect } from "react";

import requireAuth from "components/auth/requireAuth";
import { connect } from "react-redux";
import * as actions from "redux/actions";
import { compose } from "redux";

import _ from "lodash";
import { withRouter } from "react-router-dom";
import { motion, AnimatePresence } from "framer-motion";
import { Box, SimpleGrid, Button, Image, Heading } from "@chakra-ui/react";
import Error from "components/feedback/Error";
import Loading from "components/feedback/Loading";

import Dashboard from "./components/Dashboard";
import RenderedVisits from "./components/RenderedVisits";
import ErrorVisits from "./components/ErrorVisits";
import SEO from "components/Seo";

const states = {
  WIP: 1,
  "READY TO RENDER": 2,
  RENDERED: 3,
  "PENDING APPROVAL": 4,
  APPROVED: 5,
  REJECTED: 6,
  "PUBLISHED ON PLATFORM": 7,
  RENDERING: 8,
  "PROCESSING ON YOUTUBE": 9,
  "ERROR RENDERING": 10,
  "ERROR ON YOUTUBE": 11,
  "ERROR UPLOADING": 12,
  "PROCESSED ON YOUTUBE": 13,
  "SENT TO RENDER": 14,
  "TO BE REMOVED": 15,
  SUBMITTED: 16,
  "DOWNLOADING ASSETS": 17,
};

const Collection = (props) => {
  const [section, setSection] = useState(0);
  const [state, setState] = useState({
    isLoading: true,
    error: null,
  });

  const { visitsUser } = props;
  const visits = _.cloneDeep(visitsUser);
  // filter visits
  const allVisits = Array.isArray(visits) ? visits.filter(
    (visit) => visit.states_id !== states["REJECTED"]
  ) : null;
  const renderedVisits = Array.isArray(visits) ? visits.filter(
    (visit) =>
      visit.states_id === states["READY TO RENDER"] ||
      visit.states_id === states["RENDERED"] ||
      visit.states_id === states["RENDERING"] ||
      visit.states_id === states["PROCESSING ON YOUTUBE"] ||
      visit.states_id === states["PROCESSED ON YOUTUBE"] ||
      visit.states_id === states["SENT TO RENDER"]
  ) : null;
  const errorVisits = Array.isArray(visits) ? visits.filter(
    (visit) =>
      visit.states_id === states["ERROR RENDERING"] ||
      visit.states_id === states["ERROR ON YOUTUBE"] ||
      visit.states_id === states["ERROR UPLOADING"]
  ) : null;

  useEffect(() => {
    props.getUserClips(
      props.userData.id,
      () => setState({ isLoading: false, error: null }),
      () => setState({ isLoading: false, error: true })
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const collectionItems = [
    {
      name: "Todas as Visitas",
      img: "Dashboard",
      Component: () => {
        return (
          <Dashboard
            history={props.history}
            requestError={state.error}
            allVisits={allVisits}
            renderedVisits={renderedVisits}
            errorVisits={errorVisits}
          />
        );
      },
    },
    {
      name: "Visitas Renderizadas",
      img: "PublishedVisits",
      Component: () => (
        <RenderedVisits data={renderedVisits} history={props.history} />
      ),
    },
    {
      name: "Visitas com Erros",
      img: "ErrorVisits",
      Component: () => (
        <ErrorVisits data={errorVisits} history={props.history} />
      ),
    },
  ];

  const variants = {
    enter: {
      x: -1000,
      opacity: 0,
      position: "static",
      transform: "translateX(100%)",
      display: "block",
    },
    center: {
      zIndex: 1,
      opacity: 1,
      x: 0,
    },

    exit: {
      zIndex: 0,
      x: 1000,
      display: "none",
      opacity: 0,
    },
  };

  // We only have 3 images, but we paginate them absolutely (ie 1, 2, 3, 4, 5...) and
  // then wrap that within 0-2 to find our image ID in the array below. By passing an
  // absolute page index as the `motion` component's `key` prop, `AnimatePresence` will
  // detect it as an entirely new image. So you can infinitely paginate as few as 1 images.

  const toggleNewSection = (newSection) => {
    section !== newSection && setSection(newSection);
  };

  return (
    <Box as="article">
      <SEO title="Coleção" />
      <Heading as="h1">Coleção</Heading>
      {state.isLoading ? (
        <Box pt={4}>
          <Loading />
        </Box>
      ) : state.error ? (
        <Box pt={4}>
          <Error />
        </Box>
      ) : (
        <>
          <SimpleGrid columns={[1, null, null, 3]} spacing={8} mb={10}>
            {collectionItems.map((item, index) => (
              <Button
                key={index}
                h="auto"
                whiteSpace="normal"
                display="flex"
                alignItems="center"
                boxShadow="default"
                bgColor={section === index ? "primary.300" : "grey.300"}
                flexDir={["row", null, null, "column"]}
                justifyContent={["flex-start", null, null, "center"]}
                onClick={() => toggleNewSection(index)}
                leftIcon={
                  <Image
                    h={12}
                    mb={[0, null, null, 3]}
                    mr={[4, null, null, 0]}
                    objectFit="contain"
                    alt={item.name}
                    src={require(`assets/icons/${item.img}.svg`)}
                  />
                }
                _hover={{
                  bg: "#537FB9",
                }}
              >
                <Heading as="h3" pb={0} lineHeight="2rem">
                  {item.name}
                </Heading>
              </Button>
            ))}
          </SimpleGrid>

          <AnimatePresence initial={true}>
            <motion.div
              key={section}
              variants={variants}
              initial="enter"
              animate="center"
              exit="exit"
              transition={{
                x: { type: "spring", stiffness: 300, damping: 30 },
                opacity: { duration: 0.2 },
              }}
            >
              {section === 0 ? (
                  <>
                    {allVisits?.length ?
                        <Dashboard
                            history={props.history}
                            requestError={state.error}
                            allVisits={allVisits}
                            renderedVisits={renderedVisits}
                            errorVisits={errorVisits}
                        /> :
                        <div>Não tem nenhuma visita de momento na sua conta!</div>
                    }
                  </>
              ) : section === 1 ? (
                <RenderedVisits data={renderedVisits} history={props.history} />
              ) : (
                <ErrorVisits data={errorVisits} history={props.history} />
              )}
            </motion.div>
          </AnimatePresence>
        </>
      )}
    </Box>
  );
};

function mapStateToProps(state) {
  return {
    visitsUser: state.clipsUser.getUserClipsSuccess,
    userData: state.user.userSuccess,
  };
}

export default compose(
  connect(mapStateToProps, actions),
  withRouter,
  requireAuth
)(Collection);
