import React from "react";
import { Flex, Image, Heading } from "@chakra-ui/react";

import InfoContactBot from "components/feedback/InfoContactBot";
import VisitsList from "./VisitsList";

const RenderedVisits = (props) => {
  return !props.data?.length ? (
    <Flex
      flexDir="column"
      justify="center"
      align="center"
      textAlign="center"
      h="100%"
      mx={[null, null, null, "auto"]}
      w={["100%", null, null, "75%"]}
    >
      <Image
        pt={4}
        h={32}
        objectFit="contain"
        src={require("assets/icons/Info.svg")}
      />
      <Heading py={4} as="h2">
        Neste momento não possuí nenhuma visita renderizada
      </Heading>
      <InfoContactBot />
    </Flex>
  ) : (
    <VisitsList
      title={"Visitas renderizadas"}
      allVisits={props.data}
      isLoading={null}
    />
  );
};

export default RenderedVisits;
