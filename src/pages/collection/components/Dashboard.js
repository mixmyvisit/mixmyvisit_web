import React from "react";
import * as actions from "redux/actions";
import { connect } from "react-redux";

import VisitsList from "./VisitsList";

const Dashboard = (props) => {
  const { isLoading, allVisits } = props;

  return (
    <>
      <VisitsList
        title={"Visitas"}
        allVisits={allVisits}
        isLoading={isLoading}
      />
    </>
  );
};

export default connect(null, actions)(Dashboard);
