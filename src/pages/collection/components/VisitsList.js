import React, { useState, useEffect } from "react";
import * as actions from "redux/actions";
import { connect } from "react-redux";
import { Link, useLocation } from "react-router-dom";
import Moment from "react-moment";
import {
  Box,
  Tag,
  Flex,
  SimpleGrid,
  HStack,
  Button,
  Image,
  Heading,
  Text,
  Select,
  IconButton,
  Icon,
  Tooltip,
} from "@chakra-ui/react";
import { FiDownload } from "react-icons/fi";
import _ from "lodash";

import getVisitStates from "scripts/getVisitStates";
import EditButton from "components/buttons/EditButton";

const VisitsList = (props) => {
  const { title, allVisits, isLoading } = props;
  const location = useLocation();
  const [state, setState] = useState({
    displayVisits: [],
    visitsPerBatch: 6,
    index: 1,
    order: "descendant",
    orderType: "submitted_at",
  });

  const sortVisits = (visitsArray, order, orderType) => {
    visitsArray.sort((visitA, visitB) => {
      if (orderType === "name") {
        const visitNameA = visitA[orderType].toUpperCase();
        const visitNameB = visitB[orderType].toUpperCase();

        visitNameA.localeCompare(visitNameB, "pt", { ignorePunctuation: true });
        visitNameB.localeCompare(visitNameA, "pt", { ignorePunctuation: true });

        return order === "descendant"
          ? visitNameA.localeCompare(visitNameB, "pt", {
              ignorePunctuation: true,
            })
          : visitNameB.localeCompare(visitNameA, "pt", {
              ignorePunctuation: true,
            });
      } else {
        const visitValueA = new Date(visitA[orderType]).getTime();
        const visitValueB = new Date(visitA[orderType]).getTime();

        return order === "descendant"
          ? visitValueB - visitValueA
          : visitValueA - visitValueB;
      }
    });

    return visitsArray;
  };

  useEffect(() => {
    if (!isLoading) {
      const getSortedVisits = sortVisits(
        allVisits,
        state.order,
        state.orderType
      );
      setState({
        ...state,
        displayVisits: calculateVisitsBatch(getSortedVisits, state.index),
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const calculateVisitsBatch = (visits, index) => {
    const indexOfLastResults = index * state.visitsPerBatch;
    const indexOfFirstResults = indexOfLastResults - state.visitsPerBatch;
    const currentVisits = visits.slice(indexOfFirstResults, indexOfLastResults);

    return currentVisits;
  };

  const handleOrderChange = () => {
    const { order, orderType, displayVisits } = state;
    const getOrder = order === "descendant" ? "ascendant" : "descendant";

    sortVisits(allVisits, getOrder, orderType);
    const getSortedVisits = sortVisits(displayVisits, getOrder, orderType);

    setState({
      ...state,
      order: getOrder,
      displayVisits: getSortedVisits,
    });
  };

  const handleSelectChange = (ev) => {
    const selectValue = ev.target.value;
    const getSortedVisits = sortVisits(allVisits, state.order, selectValue);

    setState({
      ...state,
      orderType: selectValue,
      index: 1,
      displayVisits: calculateVisitsBatch(getSortedVisits, 1),
    });
  };

  const getBatchOfVisits = () => {
    const nextIndex = _.add(state.index, 1);
    setState({
      ...state,
      index: nextIndex,
      displayVisits: state.displayVisits.concat(
        calculateVisitsBatch(allVisits, nextIndex)
      ),
    });
  };

  const { displayVisits, visitsPerBatch, index, order } = state;

  return (
    <>
      <Flex
        flexDir={["column", "row"]}
        justify={["flex-start", "space-between"]}
        align="center"
        pb={8}
      >
        <Heading as="h2" pb={[6, 0]}>
          {title}
        </Heading>

        <Flex flexDir="row" align="center">
          <Text w={["100%", "auto"]} fontSize="xs" mr={4}>
            Ordernar por:
          </Text>
          <Select
            mr={2}
            defaultValue="date"
            onChange={(ev) => handleSelectChange(ev)}
          >
            <option value="submitted_at">Data de submissão</option>
            <option value="updated_at">Data de atualização</option>
            <option value="name">Nome</option>
          </Select>
          <Tooltip
            hasArrow
            bg="primary.300"
            color="white"
            label={
              <Text p={1}>
                {order === "descendant"
                  ? "Ordem descendente"
                  : "Ordem ascendente"}
              </Text>
            }
          >
            <IconButton
              variant="transparent"
              onClick={() => handleOrderChange()}
              icon={
                <Image
                  h={6}
                  alt="Order"
                  objectFit="contain"
                  src={require("assets/icons/Back.svg")}
                  transform={
                    order === "descendant" ? "rotate(-90deg)" : "rotate(90deg)"
                  }
                />
              }
            />
          </Tooltip>
        </Flex>
      </Flex>
      <SimpleGrid columns={[1, 2, null, 3, 4]} gridGap={[10, 8, null, 12]}>
        {displayVisits.map((visit, index) => {
          const visitDetails = JSON.parse(visit.editor_form);
          const imgUrl = visitDetails?.video?.length
            ? visitDetails?.video[0]?.thumbnail
            : require("assets/img/mvm-thumbnail-preview.png");

          const visitState = visit.states_id;
          const imagesInVisit = visitDetails?.video?.length
            ? visitDetails?.video.filter((item) => item.type === "image").length
            : 0;
          const videosInVisit = visitDetails?.video?.length
            ? visitDetails?.video.filter((item) => item.type === "video").length
            : 0;

          const hasIntroVideo = visitDetails?.video.find(
            (item) => item.location === "start"
          );

          return (
            <Link
              key={index}
              to={{
                pathname:
                  visitState === 3
                    ? `/visita-concluida/${visit.name}=${visit.id}`
                    : `/editor/${visit.name}=${visit.id}`,
                state: { prevPath: location.pathname },
              }}
            >
              <Flex
                flexDir="column"
                justify="center"
                align="center"
                bg="gray.300"
                borderRadius="15px"
                boxShadow="default"
                cursor="pointer"
              >
                <Box position="relative" w="100%">
                  {visitState === 3 ? (
                    <IconButton
                      py={0}
                      px={0}
                      bg="white"
                      isRound={true}
                      position="absolute"
                      boxShadow="default"
                      transition="all 0.5s ease-in-out"
                      boxSize="50px"
                      top="5%"
                      right="3%"
                      _hover={{
                        bg: "#bfbfbf",
                      }}
                      _active={{
                        bg: "#bfbfbf",
                      }}
                    >
                      <Icon as={FiDownload} color="primary.500" boxSize={6} />
                    </IconButton>
                  ) : (
                    <EditButton
                      destinyLink={`/editor/${visit.name}=${visit.id}`}
                      onClick={() => props.saveCliptoStorage(visitDetails)}
                    />
                  )}

                  <Image
                    h={[40, 24]}
                    w="100%"
                    objectFit="cover"
                    alt="Imagem da visita"
                    src={
                      !imgUrl
                        ? require("assets/img/mvm-thumbnail-preview.png")
                        : imgUrl
                    }
                    borderTopLeftRadius="15px"
                    borderTopRightRadius="15px"
                  />
                </Box>
                <Flex
                  textAlign="center"
                  justify="center"
                  flexDir="column"
                  p={4}
                >
                  <Heading as="h3" pb={2}>
                    {visit.name}
                  </Heading>
                  {visit.active && (
                    <Text fontWeight="bold">Esta visita encontra-se ativa</Text>
                  )}
                  <HStack spacing={3} mb={3}>
                    <Tag
                      p={2}
                      as="button"
                      size={["md"]}
                      variant="solid"
                      colorScheme={
                        [6, 10, 11, 12].indexOf(visitState) > -1
                          ? "red"
                          : [4, 8, 9, 13, 14, 17].indexOf(visitState) > -1
                          ? "yellow"
                          : "green"
                      }
                    >
                      {getVisitStates[visitState].title}
                    </Tag>
                    <Tag
                      p={2}
                      as="button"
                      size={["md"]}
                      variant="solid"
                      bgColor={imagesInVisit === 0 ? "grey.400" : "primary.300"}
                    >
                      {hasIntroVideo
                        ? _.subtract(videosInVisit - 1)
                        : videosInVisit}{" "}
                      vídeos
                    </Tag>
                    <Tag
                      p={2}
                      as="button"
                      size={["md"]}
                      variant="solid"
                      bgColor={imagesInVisit === 0 ? "grey.400" : "primary.300"}
                    >
                      {imagesInVisit} imagens
                    </Tag>
                  </HStack>
                  <Text pb={2}>
                    <Moment locale="pt" format="DD/MM/YYYY">
                      {visit.created_at}
                    </Moment>
                  </Text>
                </Flex>
              </Flex>
            </Link>
          );
        })}
      </SimpleGrid>
      {allVisits?.length > index * visitsPerBatch && (
        <Flex pt={[8, 10]} w="100%" justify="center" align="center">
          <Button
            variant="transparent"
            onClick={() => getBatchOfVisits()}
            leftIcon={
              <Image
                h={6}
                alt="More Visits"
                objectFit="contain"
                src={require("assets/icons/Plus.svg")}
              />
            }
          >
            Show more visits
          </Button>
        </Flex>
      )}
    </>
  );
};

export default connect(null, actions)(VisitsList);
