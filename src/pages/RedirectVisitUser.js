import React, { useState, useEffect } from "react";
import { reduxForm, Field } from "redux-form";
import { compose } from "redux";
import { connect } from "react-redux";
import * as actions from "redux/actions";
import { Link, withRouter } from "react-router-dom";

import {
  Box,
  Flex,
  Stack,
  Button,
  Heading,
  Divider,
  Container,
  Icon,
  CircularProgress,
  Image,
} from "@chakra-ui/react";
import { FaFacebook } from "react-icons/fa";

import { required, email } from "components/forms/validator";
import PrintToast from "components/feedback/printToast";
import EmailInput from "components/forms/EmailInput";
import PasswordInput from "components/forms/PasswordInput";
import { getUser } from "storage/Storage";
import SEO from "components/Seo";

const RedirectUserFromBot = (props) => {
  const redirectToTarget = () => {
    const location = props.match;
    const userProps = getUser();

    setIsUserLoggedIn(true);
    if (location.params.tagId || visitTag) {
      const tagId = location.params ? location.params.tagId : visitTag;

      props.getVisitWithTagIdUser(
        { tagID: tagId, userID: userProps.id },
        (visit) => {
          const visitName = visit.name.replace(" ", "-");

          if (
            visit.states_id === 2 ||
            visit.states_id === 8 ||
            visit.states_id === 16
          ) {
            props.history.push(`/visita-concluida/${visitName}=${visit.id}`);
          } else {
            props.history.push(`/editor/${visitName}=${visit.id}`);
          }
        },
        (_) => {
          props.history.push("/");
          PrintToast(
            "error",
            "Ocorreu um erro e não foi possível redirecioná-lo(a) para o editor da sua visita, tente novamente mais tarde."
          );
        }
      );
    } else {
      props.history.push("/");
      PrintToast(
        "error",
        "Ocorreu e um erro e não foi possível redirecioná-lo(a) para o editor da sua visita, tente novamente mais tarde."
      );
    }
  };

  const _linkWithFb = () => {
    const query = new URLSearchParams(props.location.search);
    const payloadToApiReq =
      query.get("to") === "editor" ? { editor: visitTag } : { tagID: visitTag };

    props.linkWithFacebook(
      payloadToApiReq,
      (url) => {
        window.open(url, "_blank") || window.location.replace(url);
      },
      () => {
        PrintToast(
          "error",
          `Um erro ocorreu durante o processo de login, por favor tente novamente mais tarde. Se o problema persistir contacte-nos via email.`
        );
      }
    );
  };

  const _onSigninFormSubmit = (formProps) => {
    props.signin(
      formProps,
      () => {
        PrintToast(
          "success",
          `Login bem sucedido - por favor aguarde enquanto o redirecionamos`
        );
        props.user(() => redirectToTarget());
      },
      (error) => {
        if (error !== "User or password incorrect") {
          PrintToast(
            "error",
            `Um erro ocorreu durante o processo de login, por favor tente novamente mais tarde. Se o problema persistir contacte-nos via email.`
          );
        } else {
          PrintToast(
            "error",
            "O email inserido ou palavra-passe inserida encontram-se incorretos"
          );
        }
      }
    );
  };

  const [isUserLoggedIn, setIsUserLoggedIn] = useState(false);
  const [visitTag, setVisitTag] = useState(false);

  useEffect(() => {
    if (props.userData) {
      props.user(() => {
        redirectToTarget();
      });
    } else {
      setVisitTag(props.match.params.tagId);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const { handleSubmit, signinLoad } = props;

  return (
    <>
      <Container h="100%" py={20} px={8}>
        <SEO title="Redirecionamento para Visita" />

        {isUserLoggedIn ? (
          <Flex
            flexDir="column"
            align="center"
            justify="center"
            h="100%"
            sx={{
              svg: {
                fill: "primary.500",
                height: 10,
                width: 10,
              },
            }}
          >
            <Flex
              flexDir="row"
              flexWrap="wrap"
              align="center"
              justify="center"
              textAlign="center"
              h="100%"
            >
              <CircularProgress
                thickness="5px"
                capIsRound={true}
                isIndeterminate
                trackColor="primary.100"
                color="primary.500"
              />
              <Heading as="h4" pl={[0, 4]} pt={[4, 0]}>
                Espere uns instantes enquanto o redirecionamos...
              </Heading>
            </Flex>
          </Flex>
        ) : (
          <Flex flexDir="column" justify="center" align="center">
            <Heading textAlign="center" pb={10} as="h2">
              Antes de explorar a sua visita precisamos que se autentique
            </Heading>
            <Button
              w={["85%", "60%"]}
              colorScheme="facebook"
              isDisabled={signinLoad}
              onClick={() => _linkWithFb()}
              leftIcon={<Icon boxSize={5} mr={2} as={FaFacebook} />}
            >
              Continuar com Facebook
            </Button>

            <Box
              display="flex"
              justifyContent="center"
              position="relative"
              w={["85%", "100%"]}
            >
              <Heading
                bg="white"
                as="h3"
                zIndex="1"
                position="absolute"
                px={6}
                mb={0}
                pb={0}
                top="31%"
              >
                Ou
              </Heading>
              <Divider borderColor="primary.300" my={12} />
            </Box>

            <Box
              w={["85%", "75%"]}
              as="form"
              display="flex"
              flexDir="column"
              justfiy="center"
              onSubmit={handleSubmit(_onSigninFormSubmit)}
            >
              <Stack spacing={6} mb={6}>
                <Field
                  name="email"
                  type="text"
                  validate={[required, email]}
                  component={EmailInput}
                />
                <Field
                  name="password"
                  type="password"
                  validate={[required]}
                  component={PasswordInput}
                />
              </Stack>

              <Button
                mb={10}
                mx="auto"
                type="submit"
                variant="primary"
                w={["100%", "75%"]}
                loadingText="A realizar login..."
                isDisabled={signinLoad}
                isLoading={signinLoad}
              >
                Login
              </Button>
            </Box>

            <Flex pt={8} w="100%" justify="center" align="center">
              <Link to="/collection">
                <Button
                  py={2}
                  px={6}
                  h="auto"
                  mb={[6, 0]}
                  variant="transparent"
                  leftIcon={
                    <Image
                      h={8}
                      mr={3}
                      alt="Back"
                      objectFit="contain"
                      src={require("assets/icons/Back.svg")}
                    />
                  }
                >
                  Voltar para a página inicial
                </Button>
              </Link>
            </Flex>
          </Flex>
        )}
      </Container>
    </>
  );
};

function mapStateToProps(state) {
  const initialValues = {
    remember: true,
  };

  return {
    signinLoad: state.signin.signin,
    userData: state.user.userSuccess,
    userFail: state.user.userFail,
    initialValues,
  };
}

export default compose(
  withRouter,
  connect(mapStateToProps, actions),
  reduxForm({ form: "signin" })
)(RedirectUserFromBot);
