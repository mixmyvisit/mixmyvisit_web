import React, { useState, useEffect, useRef } from "react";
import ReactDOM from "react-dom";
import { reduxForm, Field } from "redux-form";
import { compose } from "redux";
import { connect } from "react-redux";
import * as actions from "redux/actions";
import { Link, withRouter } from "react-router-dom";

import {
  Text,
  Box,
  Flex,
  Stack,
  Button,
  Heading,
  Divider,
  Container,
  Input,
  Icon,
  Image,
} from "@chakra-ui/react";
import { FaFacebook, FaPlus } from "react-icons/fa";

import SEO from "components/Seo";
import PrintToast from "components/feedback/printToast";
import { required, email } from "components/forms/validator";
import UploadStatusModal from "components/modals/UploadStatusModal";
import EmailInput from "components/forms/EmailInput";
import PasswordInput from "components/forms/PasswordInput";
import { getUser } from "storage/Storage";

const MessengerUploadContent = (props) => {
  const [isUserLoggedIn, setIsUserLoggedIn] = useState(false);
  const [visitTag, setVisitTag] = useState(false);
  const [isOpenUploadModal, setIsOpenUploadModal] = useState(false);
  const [didUploadFile, setDidUploadFile] = useState(false);

  const _linkWithFb = () => {
    props.linkWithFacebook(
      { videoUpload: visitTag },
      (url) => {
        window.open(url, "_blank") || window.location.replace(url);
      },
      () => {
        PrintToast(
          "error",
          `Um erro ocorreu durante o processo de login, por favor tente novamente mais tarde. Se o problema persistir contacte-nos via email.`
        );
      }
    );
  };

  const _onSigninFormSubmit = (formProps) => {
    props.signin(
      formProps,
      () => {
        PrintToast(
          "success",
          `Login bem sucedido - por favor aguarde enquanto o(a) redirecionamos`
        );
        props.user(() => {
          props.history.push(`/redirect/messenger/upload-conteudo/${visitTag}`);
        });
        setIsUserLoggedIn(true);
      },
      (error) => {
        if (error !== "User or password incorrect") {
          PrintToast(
            "error",
            `Um erro ocorreu durante o processo de login, por favor tente novamente mais tarde. Se o problema persistir contacte-nos via email.`
          );
        } else {
          PrintToast(
            "error",
            "O email inserido ou palavra-passe inserida encontram-se incorretos"
          );
        }
      }
    );
  };

  useEffect(() => {
    setVisitTag(props.match.params.tagId);

    if (props.userData) {
      props.user(() => {
        setIsUserLoggedIn(true);
      });
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const _uploadFileToVisit = (event) => {
    const userProps = getUser();

    if (props.match.params.tagId || visitTag) {
      const tagId = props.location.params
        ? props.location.params.tagId
        : visitTag;

      if (event.target.files[0]) {
        setIsOpenUploadModal(true);

        const file = event.target.files[0];
        const data = new FormData();

        data.append("file", file);
        data.append("tagID", tagId);
        data.append("userID", userProps.id);

        props.uploadContentsRedirectedFromMessenger(
          data,
          () => {
            setIsOpenUploadModal(false);
            setDidUploadFile(true);

            PrintToast(
              "success",
              "O seu ficheiro foi adicionado com sucesso à sua visita.\nPor favor aguarde, dentro de segundos será reencaminhado novamente para o Messenger."
            );

            setTimeout(() => {
              window.location.replace("http://m.me/MixMyVisit");
            }, 6000);
          },
          (err) => {
            setIsOpenUploadModal(false);
            let message;

            if (typeof err === "string") {
              const errorMessage = err;

              if (errorMessage === "Can't upload more files") {
                message =
                  "Já alcançou pelo menos 20 conteúdos na sua visita o que o impossibilita de adicionar mais conteúdos à mesma.";
              } else if (errorMessage === "User has no active visit") {
                message =
                  "A visita associada a esta pulseira não se encontra ativa impossibilitando o upload do ficheiro.";
              } else if (errorMessage === "File not valid") {
                message =
                  "Por favor insira um ficheiro dos seguintes formatos: .jpeg, .jpg, .png, .mp4.";
              } else if (errorMessage === "File not found") {
                message = `Pedimos desculpa mas ocorreu um erro no processo de upload do seu ficheiro.`;
              } else {
                message = `Pedimos desculpa mas ocorreu um erro no processo de upload do seu ficheiro.`;
              }
            } else {
              message = `Pedimos desculpa mas ocorreu um erro no processo de upload do seu ficheiro.`;
            }

            PrintToast("error", message);
          }
        );
      } else {
        PrintToast(
          "error",
          "Ocorreu um erro e não foi possível realizar o upload do seu ficheiro para a sua visita, tente novamente mais tarde."
        );
      }
    } else {
      PrintToast(
        "error",
        "Ocorreu um erro e não foi possível realizar o upload do seu ficheiro para a sua visita, tente novamente mais tarde."
      );
    }
  };

  const { handleSubmit, signinLoad } = props;
  const inputFileRef = useRef();

  return (
    <>
      <Container h="100%" py={20} px={8}>
        <SEO title="Upload de imagem/vídeo" />

        {isUserLoggedIn ? (
          <>
            <Flex flexDir="column" align="center" justify="center" h="100%">
              <Heading as="h2">Adição de imagem/vídeo à sua visita</Heading>
              <Text pb={2}>
                Carregue no botão de - "Upload imagem/vídeo" - e selecione o
                ficheiro (.jpeg, .jpg, .png, .mp4) pretendido no seu dispositivo
                para complementar a sua visita com esse mesmo vídeo.
              </Text>
              <Text pb={4}>
                Caso realize upload de uma imagem aconselhamos que escolha
                imagens com uma orientação landscape (horizontal) e que a sua
                resolução seja de pelo menos 1280x720 pixels.
              </Text>
              <Text pb={8}>
                Se fizer upload de um vídeo aconselhamos que utilize um com uma
                resolução de 16:9 de forma a obter um melhor resultado no vídeo
                final da visita.
              </Text>
              <Button
                mx="auto"
                display="flex"
                variant="primary"
                justifyContent="center"
                alignItems="center"
                onClick={() => {
                  ReactDOM.findDOMNode(inputFileRef.current).click();
                }}
                leftIcon={
                  <Icon mr={2} as={FaPlus} color="white" h={6} w="auto" />
                }
              >
                Upload de imagem/vídeo
              </Button>
              <Input
                name="fileToUpload"
                type="file"
                display="none"
                ref={inputFileRef}
                onChange={(ev) => _uploadFileToVisit(ev)}
                accept="image/*, video/mp4"
              />
            </Flex>
            <UploadStatusModal isModalOpen={isOpenUploadModal} />
          </>
        ) : didUploadFile ? (
          <Flex flexDir="column" justify="center" align="center">
            <Image
              w={24}
              h="auto"
              alt="Sucesso"
              src={require(`assets/icons/Check.svg`)}
            />
            <Heading textAlign="center" pb={10} as="h2">
              O ficheiro foi adicionado com sucesso à sua visita.
            </Heading>
            <Text>
              Caso tenha feito este processo pelo browser embutido do Facebook
              Messenger por favor aguarde visto que dentro de segundos será
              reencaminhado novamente para o Messenger. Se tiver realizado o
              upload num browser dedicado terá de voltar novamente à aplicação
              do Facebook Messenger manualmente.
            </Text>
          </Flex>
        ) : (
          <Flex flexDir="column" justify="center" align="center">
            <Heading textAlign="center" pb={10} as="h2">
              Antes de adicionar uma imagem/vídeo à sua visita precisamos que se
              autentique
            </Heading>
            <Button
              w={["85%", "60%"]}
              colorScheme="facebook"
              isDisabled={signinLoad}
              onClick={() => _linkWithFb()}
              leftIcon={<Icon boxSize={5} mr={2} as={FaFacebook} />}
            >
              Continuar com Facebook
            </Button>

            <Box
              display="flex"
              justifyContent="center"
              position="relative"
              w={["85%", "100%"]}
            >
              <Heading
                bg="white"
                as="h3"
                zIndex="1"
                position="absolute"
                px={6}
                mb={0}
                pb={0}
                top="31%"
              >
                Ou
              </Heading>
              <Divider borderColor="primary.300" my={12} />
            </Box>

            <Box
              w={["85%", "75%"]}
              as="form"
              display="flex"
              flexDir="column"
              justfiy="center"
              onSubmit={handleSubmit(_onSigninFormSubmit)}
            >
              <Stack spacing={6} mb={6}>
                <Field
                  name="email"
                  type="text"
                  validate={[required, email]}
                  component={EmailInput}
                />
                <Field
                  name="password"
                  type="password"
                  validate={[required]}
                  component={PasswordInput}
                />
              </Stack>

              <Button
                mb={10}
                mx="auto"
                type="submit"
                variant="primary"
                w={["100%", "75%"]}
                loadingText="A realizar login..."
                isDisabled={signinLoad}
                isLoading={signinLoad}
              >
                Login
              </Button>
            </Box>

            <Flex pt={8} w="100%" justify="center" align="center">
              <Link to="/">
                <Button
                  py={2}
                  px={6}
                  h="auto"
                  mb={[6, 0]}
                  variant="transparent"
                  leftIcon={
                    <Image
                      h={8}
                      mr={3}
                      alt="Back"
                      objectFit="contain"
                      src={require("assets/icons/Back.svg")}
                    />
                  }
                >
                  Voltar para a página inicial
                </Button>
              </Link>
            </Flex>
          </Flex>
        )}
      </Container>
    </>
  );
};

function mapStateToProps(state) {
  const initialValues = {
    remember: true,
  };

  return {
    signinLoad: state.signin.signin,
    userData: state.user.userSuccess,
    userFail: state.user.userFail,
    initialValues,
  };
}

export default compose(
  withRouter,
  connect(mapStateToProps, actions),
  reduxForm({ form: "signin" })
)(MessengerUploadContent);
