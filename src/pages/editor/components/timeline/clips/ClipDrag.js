import React from "react";
import { Transition } from "react-spring/renderprops";

const ClipDrag = props => {
	const layerStyles = {
		backgroundImage: `url(${props.thumbnail})`,
		backgroundRepeat: "no-repeat",
		backgroundPosition: "center",
		backgroundColor: "lightgrey",
		width: `${props.size || 100}px`,
		height: `${props.size || 100}px`
	};

	let img = new Image(); // HTML5 Constructor
	img.src = props.thumbnail;
	img.crossOrigin = "Anonymous";

	return (
		<Transition
			from={{ opacity: 0, transform: "scale(0)" }}
			enter={{ opacity: 1, transform: "scale(1)" }}
			leave={{ opacity: 0, transform: "scale(0)" }}
		>
			{styles => <div style={{ ...styles, ...layerStyles }} />}
		</Transition>
	);
};

export default ClipDrag;
