import React, { Component } from "react";
import ReactDOM from "react-dom";
import PropTypes from "prop-types";
import { compose } from "redux";
import { connect } from "react-redux";
import * as actions from "redux/actions";
import { withRouter } from "react-router-dom";
import { Spring } from "react-spring/renderprops";
import _ from "lodash";

import { Box, Image, Icon } from "@chakra-ui/react";
import { FaCommentAlt } from "react-icons/fa";
import {
  selfTarget,
  Drop,
  Drag,
  DragSource,
  DropTarget,
  TYPES,
} from "components/draggable";

let stopTimer = false;
let stoppedTimer = false;

class Clip extends Component {
  static propTypes = {
    connectDragSource: PropTypes.func.isRequired,
    connectDragPreview: PropTypes.func.isRequired,
    connectDropTarget: PropTypes.func.isRequired,
    isDragging: PropTypes.bool.isRequired,
    canDrop: PropTypes.bool.isRequired,
  };

  timer = undefined;
  state = {
    markIns: {},
    mouseDown: 0,
  };

  render() {
    const {
      connectDropTarget,
      connectDragSource,
      isDragging,
      id,
      img,
      clipKey,
      containerSize,
      pos,
      comment,
    } = this.props;

    const style = isDragging ? { opacity: 1 } : { opacity: 0.75 };

    return connectDragSource(
      connectDropTarget(
        <div
          onClick={() => {
            this.props.videoContext({
              action: "clip",
              id: id,
              pos: pos,
            });
            this.props._setSelectedClip(clipKey);
          }}
          onMouseDown={() => {
            stoppedTimer = false;
            this._mouseDownHandler();
          }}
          onTouchStart={() => {
            stoppedTimer = false;
            this._mouseDownHandler();
          }}
          onMouseUp={() => {
            clearTimeout(this.timer);
            this.setState({ mouseDown: 0 });
          }}
          onTouchEnd={() => {
            clearTimeout(this.timer);
            this.setState({ mouseDown: 0 });
          }}
        >
          <Spring from={isDragging} to={style}>
            {({ opacity }) => (
              <Box
                minH="20px"
                minW="20px"
                bgSize="cover"
                bgPosition="center center"
                bgRepeat="no-repeat"
                bgImage={`url(${encodeURI(img)})`}
                h={containerSize}
                w={containerSize}
                position="relative"
                opacity={opacity}
                transition="all 0.5s ease-in-out"
                _active={{
                  opacity: 1,
                }}
                _hover={{
                  opacity: 1,
                }}
              >
                {comment !== "" && (
                  <Icon
                    top="5%"
                    left="80%"
                    color="white"
                    as={FaCommentAlt}
                    position="absolute"
                  />
                )}
                <Image
                  h={8}
                  alt="Indicator"
                  objectFit="contain"
                  style={this._getStyle("indicator")}
                  src={require("assets/icons/Indicator.svg")}
                />
                <Box style={this._getStyle("bar")} />
              </Box>
            )}
          </Spring>
        </div>
      )
    );
  }

  componentDidUpdate() {
    if (stopTimer) {
      clearTimeout(this.timer);
      this.setState({ mouseDown: 0 });
      stopTimer = false;
      stoppedTimer = true;
    }
  }

  // this prints the line that will represent which clip is playing at which specific point
  _getStyle = (type) => {
    const { videoCtxData, playerTime, videoForm, pos, containerSize } =
      this.props;

    if (videoCtxData === undefined) {
      return null;
    } else {
      let showOverlay, left, right;

      let startClip = 0;
      _.each(videoForm, (clip) => {
        if (clip.pos < pos) {
          startClip += clip.trimEnd - clip.trimStart;
        }
      });

      if (
        playerTime >= startClip &&
        playerTime <= videoCtxData.trimEnd + startClip
      ) {
        showOverlay = true;
        // regra de 3 simples --> trimEnd = 100% = containerSize (77.5px || 50px); (playerTime - startClip) = x
        if (type === "indicator") {
          left =
            ((playerTime - startClip) * containerSize - 33.5) /
            videoCtxData.trimEnd;
        } else {
          left =
            ((playerTime - startClip) * containerSize) / videoCtxData.trimEnd;
        }

        right = containerSize - containerSize / 10 - left;
      } else {
        showOverlay = false;
      }

      return {
        display: !showOverlay ? "none" : "block",
        height: type === "indicator" ? "2.5rem" : "100%",
        backgroundColor: type === "indicator" ? "transparent" : "#14223b",
        opacity: "1",
        position: "absolute",
        top: type === "indicator" ? "-50px" : "-8px",
        width: type === "indicator" ? "12px" : "3px",
        left: left,
        right: right,
        transition: "all 0.15s ease-out",
      };
    }
  };

  _mouseDownHandler = () => {
    if (this.state.mouseDown === 3 || stopTimer) {
      clearTimeout(this.timer);
      let clipId = this.props.id;
      this.setState({ mouseDown: 0 });
      this.props.history.push(`${this.props.location.pathname}/${clipId}`);
    } else if (!stoppedTimer) {
      this.setState({ mouseDown: this.state.mouseDown + 1 });
      this.timer = setTimeout(() => this._mouseDownHandler(), 1000);
    }
  };
}

const dragSrc = {
  beginDrag(props, monitor, component) {
    stopTimer = true;
    let boundingRect = ReactDOM.findDOMNode(component).getBoundingClientRect();

    return {
      id: props.id,
      img: props.img,
      duration: props.duration,
      boundingRect: boundingRect,
    };
  },

  endDrag(props, monitor, component) {
    const dropResult = monitor.getDropResult();
    if (!_.isEmpty(dropResult)) {
      monitor.prevId = null;
      props.clipsCollapsed > 0
        ? window.location.reload()
        : props._setClipsId(component.props.id);
      if (dropResult.container === TYPES.DELETE_AREA) {
        props._removeClip(component.props.id); //to remove it uses the last element position after dragging around
      }
    }
  },
};

export default compose(
  withRouter,
  connect(null, actions),
  DropTarget([TYPES.CLIP, TYPES.CLIP_USED], selfTarget, Drop),
  DragSource(TYPES.CLIP_USED, dragSrc, Drag)
)(Clip);
