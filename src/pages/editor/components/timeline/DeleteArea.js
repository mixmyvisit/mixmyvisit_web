import React from "react";
import PropTypes from "prop-types";
import { compose } from "redux";
import { Flex, Image, Text } from "@chakra-ui/react";
import { Drop, DropTarget, TYPES } from "components/draggable";

const DeleteArea = ({ connectDropTarget, isOver, canDrop, size }) => {
  const isActive = canDrop && isOver;

  return connectDropTarget(
    <div style={{ height: size, w: size }}>
      <Flex
        p={6}
        h="100%"
        flexDir="column"
        justify="center"
        align="center"
        borderRadius="4px"
        border={`${isActive ? "2px" : "1px"} dashed`}
        borderColor="primary.500"
        opacity={isActive ? 1 : 0.5}
        color="primary.500"
        transition="all 0.5s ease-in-out"
      >
        <Image
          h={16}
          alt="Delete"
          objectFit="contain"
          src={require("assets/icons/Delete.svg")}
        />
        <Text>Apagar</Text>
      </Flex>
    </div>
  );
};

DeleteArea.propTypes = {
  connectDropTarget: PropTypes.func.isRequired,
  canDrop: PropTypes.bool.isRequired,
  isOver: PropTypes.bool.isRequired,
};

const deleteSource = {
  drop() {
    return {
      container: TYPES.DELETE_AREA,
    };
  },
};

export default compose(
  DropTarget(
    [TYPES.CLIP_USED, TYPES.STICKER_USED, TYPES.TITLE_USED],
    deleteSource,
    Drop
  )
)(DeleteArea);
