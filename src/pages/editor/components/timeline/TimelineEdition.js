import React, { Component } from "react";
import ReactDOM from "react-dom";

import { compose } from "redux";
import { connect } from "react-redux";
import * as actions from "redux/actions";

import { isMobile } from "react-device-detect";
import { Spring } from "react-spring/renderprops";
import { withRouter } from "react-router-dom";
import _ from "lodash";

import {
  Box,
  Flex,
  SimpleGrid,
  Button,
  Input,
  Image,
  Heading,
  Text,
  VStack,
} from "@chakra-ui/react";

import Clip from "./clips/Clip";
import DeleteArea from "./DeleteArea";
import UploadStatusModal from "components/modals/UploadStatusModal";
import ConfirmationRenderModal from "components/modals/ConfirmationRenderModal";
import Loading from "components/feedback/Loading";
import PrintToast from "components/feedback/printToast";
import { Drop, DropTarget, TYPES } from "components/draggable";
import { structureSaveClip } from "scripts/clipToDb";
import { getClip } from "storage/Storage";

import moment from "moment";

class TimelineEdition extends Component {
  state = {
    isLoading: true,
    clips: [],
    selectedKey: null,
    winWidth: 0,
    clipDimensions: 0,
    timelineHeight: 0,
    timelineWidth: 0,
    collapsedClips: 0,
    isEditingClip: false,
    isModalToRender: false,
    isOpenRenderModal: false,
    isLoadingRenderModal: false,
    isOpenUploadModal: false,
    alertShouldLeave: true,
  };

  timelineRef = React.createRef();
  inputFileRef = React.createRef();
  prevClipsState = [];
  movingClips = [];
  currentVisit = null;
  pushToPosition = false;

  divClipsHeight = 125;
  divClipsWidth = "100%";
  clipDimensions = {
    mobile: 100,
    desktop: 125,
  };

  render() {
    const { connectDropTarget, undoActions, redoActions, didEditVisit } =
      this.props;
    const { clips, winWidth, timelineHeight, isLoading, clipDimensions } =
      this.state;

    const doesVisitHaveVideo = clips.filter((item) => item.type === "video");

    //const selectedKey = this.state.selectedKey;
    let styles = {};

    if (isMobile) {
      styles = {
        minHeight: `${this.divClipsHeight + timelineHeight}px`,
        flexDirection: "column",
      };
    } else {
      styles = {
        minHeight: `${this.divClipsHeight}px`,
      };
    }

    console.log(this.props.videoInfo);

    return isLoading ? (
      <Loading />
    ) : (
      connectDropTarget(
        <div>
          <SimpleGrid
            pt={6}
            pb={[10, 16]}
            columns={[2, 3]}
            gridGap={[6, 8]}
            alignItems="center"
            w={["full", "auto"]}
          >
            <Button
              h="auto"
              alignItems="center"
              variant="transparent"
              flexDirection="column"
              isDisabled={undoActions.length !== 0 ? false : true}
              onClick={() => {
                let action = undoActions[undoActions.length - 1];
                this._undoActionHandler(action.action, action.media);
              }}
            >
              <Image
                h={16}
                alt="Undo"
                objectFit="contain"
                src={require("assets/icons/Undo.svg")}
              />
              <p>Desfazer</p>
            </Button>

            <VStack
              display="inline-flex"
              spacing={6}
              order={[3, "inherit"]}
              w="100%"
              gridColumnStart={[1, "auto"]}
              gridColumnEnd={[3, "auto"]}
              justifyContent="center"
              alignItems="center"
            >
              <Button
                variant="primary"
                isDisabled={!didEditVisit}
                w={[64, "100%"]}
                onClick={() =>
                  this.setState({
                    isOpenRenderModal: !this.state.isOpenRenderModal,
                    isModalToRender: false,
                  })
                }
              >
                Gravar alterações
              </Button>

              <Button
                variant="primary"
                bg="primary.500"
                w={[64, "100%"]}
                onClick={() => {
                  if (!doesVisitHaveVideo?.length >= 1) {
                    PrintToast(
                      "error",
                      "Não é possí­vel renderizar a sua visita pois esta não possuí­ qualquer vídeo na mesma. Por favor certifique-se de adicionar um vídeo."
                    );
                  } else {
                    this.setState({
                      isOpenRenderModal: !this.state.isOpenRenderModal,
                      isModalToRender: true,
                    });
                  }
                }}
              >
                Renderizar
              </Button>
            </VStack>

            <Button
              h="auto"
              alignItems="center"
              variant="transparent"
              flexDirection="column"
              isDisabled={redoActions.length !== 0 ? false : true}
              onClick={() => {
                let action = redoActions[redoActions.length - 1];
                this._redoActionHandler(action.action, action.media);
              }}
            >
              <Image
                h={16}
                alt="Redo"
                objectFit="contain"
                src={require("assets/icons/Redo.svg")}
              />
              <p>Refazer</p>
            </Button>
          </SimpleGrid>

          {(isMobile || winWidth < 600) && clips.length > 1 && (
            <Flex w="100%" justify="center" mb={4}>
              <DeleteArea
                doesVisitHaveVideo={doesVisitHaveVideo}
                size={clipDimensions}
              />
            </Flex>
          )}

          <Flex
            justify={["flex-start", "center"]}
            style={styles}
            ref={this.timelineRef}
          >
            {_.isEmpty(clips) ? (
              <Box textAlign="center">
                <Heading as="h4" pb={4}>
                  A timeline desta visita está vazia!
                </Heading>
                <Text pb={4}>
                  Adicione um ví­deo ou uma image para começar a editar via{" "}
                  <a
                    target="_blank"
                    rel="noopener noreferrer"
                    href="http://m.me/Mixmyvisit"
                  >
                    Bot no Messenger
                  </a>{" "}
                  ou a partir do botão em baixo.
                </Text>
                <Button
                  mx="auto"
                  h={clipDimensions}
                  w={clipDimensions}
                  display="flex"
                  justifyContent="center"
                  alignItems="center"
                  flexDirection="column"
                  variant="transparent"
                  onClick={() => {
                    ReactDOM.findDOMNode(this.inputFileRef.current).click();
                  }}
                >
                  <Image
                    h={16}
                    mx="auto"
                    objectFit="contain"
                    alt="Add Clip"
                    src={require("assets/icons/Plus.svg")}
                  />
                  <span>
                    Adicionar <p>vídeo/imagem</p>
                  </span>
                </Button>
                <Input
                  name="fileToUpload"
                  type="file"
                  display="none"
                  ref={this.inputFileRef}
                  onChange={(ev) => this._uploadFileToVisit(ev)}
                  accept="image/*, video/mp4"
                />
              </Box>
            ) : (
              <>
                {!isMobile && winWidth >= 600 && clips.length > 1 && (
                  <Box mr={2}>
                    <DeleteArea
                      doesVisitHaveVideo={doesVisitHaveVideo}
                      size={clipDimensions}
                    />
                  </Box>
                )}
                <Flex
                  h={this.divClipsHeight}
                  flexDir="column"
                  align={["center", "flex-start"]}
                  w={isMobile || winWidth < 600 ? "100%" : this.divClipsWidth}
                >
                  {this._renderTimeline()}
                </Flex>
              </>
            )}
          </Flex>
          <ConfirmationRenderModal
            isRender={this.state.isModalToRender}
            updateTypeItem="visit"
            isModalLoading={this.state.isLoadingRenderModal}
            isModalOpen={this.state.isOpenRenderModal}
            handleConfirmation={
              this.state.isModalToRender
                ? () => this._sendToRenderVisit()
                : () => this._updateVisit()
            }
            handleClose={() =>
              this.setState({
                isOpenRenderModal: !this.state.isOpenRenderModal,
              })
            }
          />
          <UploadStatusModal isModalOpen={this.state.isOpenUploadModal} />
        </div>
      )
    );
  }

  _renderTimeline() {
    // this method is destined to print all the thumbnails of the clips of the visit.
    // to do that its need a lot of conditions for the positioning of the thumbnails
    // mainly because responsive styles and the thumbnails are draggable

    const { video, playerTime } = this.props;
    const {
      clips,
      timelineHeight,
      timelineWidth,
      collapsedClips,
      clipDimensions,
    } = this.state;
    const selectedKey = this.state.selectedKey;
    let positionsArray = [];
    let clipPositions;
    let height = clipDimensions;
    let width = 0;

    return clips.map((clip, key) => {
      // build an array to determine the x and y position of each clip image and clone that array (because mutability)

      // Verify if height is larger than width
      if ((key + 1) * timelineHeight > timelineWidth) {
        if (key - 1 !== -1) {
          if (
            (key + 1 - clipPositions[key - 1].xPos) * timelineHeight >
            timelineWidth
          ) {
            positionsArray[key] = {
              xPos: key,
              yPos: (clipPositions[key - 1].yPos += 1),
            };
            height += timelineHeight;
            this.divClipsHeight = _.cloneDeep(height);
          } else {
            positionsArray[key] = {
              xPos: clipPositions[key - 1].xPos,
              yPos: clipPositions[key - 1].yPos,
            };
            this.divClipsHeight = _.cloneDeep(height);
          }
        } else {
          if (this.state.winWidth <= 600 || isMobile) {
            height += timelineHeight;
            this.divClipsHeight = _.cloneDeep(height);
          }
          positionsArray[key] = {
            xPos: 0,
            yPos: 0,
          };
        }
      } else {
        if (this.state.winWidth <= 600 || isMobile) {
          if (key !== 0) {
            const rowWidth = clipDimensions * clipPositions[key - 1].xPos;
            if (rowWidth > timelineWidth) {
              height += timelineHeight;
              this.divClipsHeight = _.cloneDeep(height);
            }
          }
        } else if (this.state.winWidth > 600 || !isMobile) {
          width += timelineHeight;
        }
        positionsArray[key] = {
          xPos: 0,
          yPos: 0,
        };
      }

      this.divClipsHeight = _.cloneDeep(height);
      this.divClipsWidth = _.cloneDeep(width);
      clipPositions = _.cloneDeep(positionsArray);

      let from = { y: clip.from };
      let to = { y: clip.to };
      let isSelected = key === selectedKey;

      return (
        <Box key={key} w={clipDimensions}>
          <Spring
            from={from}
            to={to}
            config={{
              friction: 15,
              tension: 300,
              clamp: true,
            }}
            onRest={() => {
              if (this.movingClips !== undefined) {
                let movingIndex = this.movingClips.findIndex(
                  (el) => el.id === clip.id
                );
                this.movingClips.splice(movingIndex, 1);
              }
            }}
          >
            {({ y }) => {
              if (key === clips.length - 1) {
                if (clips.length < 20) {
                  return (
                    <Box
                      mt={[4, 0]}
                      position="absolute"
                      transform={`translate3d(${
                        (y - clipPositions[clip.pos].xPos) * timelineHeight
                      }px, ${
                        clipPositions[clip.pos].yPos * timelineHeight
                      }px, 0)`}
                    >
                      <Button
                        h={clipDimensions}
                        w={clipDimensions}
                        display="flex"
                        justifyContent="center"
                        alignItems="center"
                        flexDirection="column"
                        variant="transparent"
                        onClick={() => {
                          ReactDOM.findDOMNode(
                            this.inputFileRef.current
                          ).click();
                        }}
                      >
                        <Image
                          h={16}
                          mx="auto"
                          objectFit="contain"
                          alt="Add Clip"
                          src={require("assets/icons/Plus.svg")}
                        />
                        <span>
                          Adicionar <p>vídeo/imagem</p>
                        </span>
                      </Button>
                      <Input
                        name="fileToUpload"
                        type="file"
                        display="none"
                        ref={this.inputFileRef}
                        onChange={(ev) => this._uploadFileToVisit(ev)}
                        accept="image/*, video/mp4"
                      />
                    </Box>
                  );
                } else {
                  return <></>;
                }
              } else {
                return (
                  <Box
                    position="absolute"
                    transform={`translate3d(${
                      (y - clipPositions[clip.pos].xPos) * timelineHeight
                    }px, ${
                      clipPositions[clip.pos].yPos * timelineHeight
                    }px, 0)`}
                  >
                    <Clip
                      id={clip.id}
                      clipKey={key}
                      pos={clip.pos}
                      img={clip.img}
                      isSelected={isSelected}
                      videoCtxData={
                        video[video.findIndex((el) => el.id === clip.id)]
                      }
                      comment={clip.comment}
                      containerSize={timelineHeight}
                      playerTime={playerTime}
                      videoForm={_.cloneDeep(this.state.clips)}
                      _moveClip={this._moveClip}
                      _resetPosition={this._resetPosition}
                      _pushClipToPosition={this._pushClipToPosition}
                      _removeClip={this._removeClip}
                      _setClipsId={this._setClipsId}
                      _setSelectedClip={this._setSelectedClip}
                      _updatedSelectedId={this._updatedSelectedId}
                      clipsCollapsed={collapsedClips}
                    />
                  </Box>
                );
              }
            }}
          </Spring>
        </Box>
      );
    });
  }

  componentDidMount() {
    window.addEventListener("resize", this._updateDimensions);

    if (!_.isEmpty(this.props.videoInfo.video)) {
      this._loadVideo(this.props.videoInfo.video);
    } else if (!_.isEmpty(this.props.video)) {
      this._loadVideo(this.props.video);
    } else {
      this.setState({ isLoading: false });
    }
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this._updateDimensions);
    this.props.undoAction([]);
    this.props.redoAction([]);
  }

  componentDidUpdate(prevProps, prevState) {
    let dragAction = this.props.dragAction;
    var timelineNode = ReactDOM.findDOMNode(this.timelineRef.current);

    if (timelineNode.clientWidth === 0) {
      setTimeout(() => {
        this.setState({
          timelineHeight: isMobile
            ? this.clipDimensions.mobile
            : this.clipDimensions.desktop,
          timelineWidth: isMobile
            ? timelineNode.clientWidth
            : timelineNode.clientWidth - timelineNode.clientHeight / 2,
          winWidth: window.innerWidth,
          clipDimensions: isMobile
            ? this.clipDimensions.mobile
            : this.clipDimensions.desktop,
        });
      }, 100);
    } else if (timelineNode.clientWidth !== 0 && !this.state.timelineWidth) {
      this.setState({
        timelineHeight: isMobile
          ? this.clipDimensions.mobile
          : this.clipDimensions.desktop,
        timelineWidth: isMobile
          ? timelineNode.clientWidth
          : timelineNode.clientWidth - timelineNode.clientHeight / 2,
        winWidth: window.innerWidth,
        clipDimensions: isMobile
          ? this.clipDimensions.mobile
          : this.clipDimensions.desktop,
      });
    }

    if (dragAction !== prevProps.dragAction) {
      if (dragAction.func === "pushClipToPosition") {
        this._pushClipToPosition(
          dragAction.parameters[0],
          dragAction.parameters[1],
          dragAction.parameters[2]
        );
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps !== this.props) {
      //reset position when elements are out of drop target timeline
      if (!nextProps.isOver && !nextProps.didProp && nextProps.canDrop) {
        this._resetPosition();
      }
    }
  }

  _setSelectedClip = (key) => {
    this.setState({
      selectedKey: key,
    });
  };

  _updateDimensions = () => {
    var node = ReactDOM.findDOMNode(this.timelineRef.current);
    if (node) {
      //console.log(this.clipDimensions.mobile > window.innerWidth)

      this.setState({
        timelineHeight: isMobile
          ? this.clipDimensions.mobile
          : this.clipDimensions.desktop,
        timelineWidth: isMobile
          ? node.clientWidth
          : node.clientWidth - node.clientHeight / 2,
        winWidth: window.innerWidth,
        clipDimensions: isMobile
          ? this.clipDimensions.mobile
          : this.clipDimensions.desktop,
      });
    }
  };

  _loadVideo = (video) => {
    let clips = _.cloneDeep(this.state.clips); //After this clips is magically populated
    const _loadClip = (id) => {
      let clip = video[id];
      if (clips.length === 0) {
        //add the empty drop
        clips.push({
          id: clips.length + 1,
          from: clips.length + 1,
          to: clips.length + 1,
          pos: clips.length + 1,
        });

        //add the clip before the empty drop
        clip.id = clips.length - 1;
        clip.from = clips.length - 1;
        clip.to = clips.length - 1;
        clip.pos = clips.length - 1;

        clips.splice(clips.length - 1, 0, clip);
      } else {
        //set the add empty area position
        clips[clips.length - 1].id = clips.length;
        clips[clips.length - 1].from = clips.length;
        clips[clips.length - 1].to = clips.length;
        clips[clips.length - 1].pos = clips.length;

        //add the clip before the empty drop
        clip.id = clips.length - 1;
        clip.from = clips.length - 1;
        clip.to = clips.length - 1;
        clip.pos = clips.length - 1;

        clips.splice(clips.length - 1, 0, clip);
      }

      this.setState({ clips }, () => {
        this.prevClipsState = _.cloneDeep(this.state.clips);
        //Triggers action to update videoContext in Store
        switch (clip.type) {
          case "video":
            this.props.videoContext({
              action: "add",
              type: "video",
              url: clip.url,
              id: clip.id,
              img: clip.img,
              thumbnail: clip.img,
              timestamp: clip.timestamp,
              duration: clip.duration,
              trimStart: clip.trimStart, //added after, but no effect on fullVideoCtx
              trimEnd: clip.trimEnd, //added after, but no effect on fullVideoCtx
              volume: clip.volume,
              muted: clip.muted,
              location: clip.location || null,
              pos: clip.pos,
              comment: clip.comment,
            });
            break;
          case "image":
            this.props.videoContext({
              action: "add",
              type: "image",
              url: clip.url,
              id: clip.id,
              img: clip.url,
              thumbnail: clip.url,
              timestamp: clip.timestamp,
              duration: clip.duration,
              trimStart: clip.trimStart, //added after, but no effect on fullVideoCtx
              trimEnd: clip.trimEnd, //added after, but no effect on fullVideoCtx
              volume: clip.volume,
              muted: clip.muted,
              location: clip.location || null,
              pos: clip.pos,
              comment: clip.comment,
            });
            break;
          default:
            break;
        }

        if (id < video.length - 1) {
          _loadClip(id + 1);
        }
      });
      this._setSelectedClip(clips.length - 2); //Here clips is already assembled, as well as vctx
      this.setState({ isLoading: false });
    };
    _loadClip(0);
  };

  _sendToRenderVisit = () => {
    this.setState({ isLoadingRenderModal: !this.state.isLoadingRenderModal });
    let clip = getClip();
    this.props.updateClip(
      structureSaveClip(clip),
      clip.id,
      () => {
        this.props.saveCliptoStorage(clip);
        this.props.updateForm("editor", "video", clip.video);

        this.props.updateClip(
          structureSaveClip(clip),
          `${clip.id}?notify=ready`,
          () => {
            this.props.handleSubmitVisit();
            this.setState({
              isOpenRenderModal: !this.state.isLoadingRenderModal,
              isLoadingRenderModal: !this.state.isLoadingRenderModal,
            });
            PrintToast(
              "success",
              "Visita enviada para o processo de renderização com sucesso! Irá receber uma notificação pelo nosso bot no messenger quando o processo tiver terminado!"
            );
          },
          () =>
            PrintToast(
              "error",
              `Ocorreu um erro quando tentou enviar a sua visita para o processo de renderização. Tente novamente mais tarde.`
            )
        );
      },
      () =>
        PrintToast(
          "error",
          `Ocorreu um erro quando tentou enviar a sua visita para o processo de renderização. Tente novamente mais tarde.`
        )
    );
  };

  _updateVisit = () => {
    this.setState({
      isLoadingRenderModal: !this.state.isLoadingRenderModal,
      isModalToRender: false,
    });
    let clip = getClip();
    this.props.updateClip(
      structureSaveClip(clip),
      clip.id,
      () => {
        this.props.saveCliptoStorage(clip);
        this.props.updateForm("editor", "video", clip.video);
        this.props.handleSubmitVisit();
        this.setState({
          isOpenRenderModal: !this.state.isLoadingRenderModal,
          isLoadingRenderModal: !this.state.isLoadingRenderModal,
        });
        PrintToast("success", "Visita atualizada com sucesso");
      },
      () =>
        PrintToast(
          "error",
          `Ocorreu um erro quando tentou atualizar a sua visita. Tente novamente mais tarde.`
        )
    );
  };

  _uploadFileToVisit = (event) => {
    let file = event.target.files[0];
    if (file) {
      const data = new FormData();
      data.append("file", file);
      data.append("id", this.props.videoInfo?.id);

      this.setState({ isOpenUploadModal: true });
      this.props.uploadClip(
        data,
        (response) => {
          switch (response.type) {
            case "video":
              let video = {
                url: response.url,
                img: response.thumbnail,
                thumbnail: response.thumbnail,
                duration: response.duration,
                trimStart: 0,
                trimEnd: response.duration,
                comment: "",
                timestamp: moment().format("YYYY-MM-DD HH:mm:ss"),
              };

              this._pushClip(video);
              this.setState({ isOpenUploadModal: false });

              break;
            case "image":
              let image = {
                url: response.url,
                img: response.url,
                duration: response.duration,
                trimStart: 0,
                trimEnd: response.duration,
                comment: "",
                muted: true,
                timestamp: moment().format("YYYY-MM-DD HH:mm:ss"),
              };

              this._pushImage(image);
              this.setState({ isOpenUploadModal: false });
              break;
            default:
              break;
          }
          PrintToast("success", `Upload do ficheiro bem sucedido!`);
        },
        () => {
          this.setState({ isOpenUploadModal: false });
          PrintToast(
            "error",
            `Pedimos desculpa mas ocorreu um erro no processo de upload do ficheiro.`
          );
        }
      );
    }
  };

  _pushClip = (clip) => {
    let undoArr = _.cloneDeep(this.props.undoActions);
    let clips = _.cloneDeep(this.state.clips);
    if (clips.length === 0) {
      //add the empty drop
      clips.push({
        id: clips.length + 1,
        from: clips.length + 1,
        to: clips.length + 1,
        pos: clips.length + 1,
      });

      //add the clip before the empty drop
      clip.id = clips.length - 1;
      clip.from = clips.length - 1;
      clip.to = clips.length - 1;
      clip.pos = clips.length - 1;
      clips.splice(clips.length - 1, 0, clip);
    } else {
      if (clips.length <= 26) {
        //set the add empty area position
        clips[clips.length - 1].id = clips.length;
        clips[clips.length - 1].from = clips.length;
        clips[clips.length - 1].to = clips.length;
        clips[clips.length - 1].pos = clips.length;

        //add the clip before the empty drop
        clip.id = clips.length - 1;
        clip.from = clips.length - 1;
        clip.to = clips.length - 1;
        clip.pos = clips.length - 1;
        clips.splice(clips.length - 1, 0, clip);
      }
    }
    this._setSelectedClip(clip.id);
    this.setState({ clips }, () => {
      this.prevClipsState = _.cloneDeep(this.state.clips);
      this.props.videoContext({
        action: "add",
        type: "video",
        url: clip.url,
        id: clip.id,
        img: clip.img,
        thumbnail: clip.img,
        timestamp: clip.timestamp,
        duration: clip.duration,
        trimStart: clip.trimStart, //added after, but no effect on fullVideoCtx
        trimEnd: clip.trimEnd, //added after, but no effect on fullVideoCtx
        volume: 1,
        muted: false,
        local: clip.local,
        pos: clip.pos,
        comment: clip.comment,
      });
      undoArr.push({
        action: "remove",
        media: {
          thumbnail: clip.img,
          duration: clip.duration,
          type: "video",
          img: clip.img,
          url: clip.url,
          trimStart: clip.trimStart,
          trimEnd: clip.trimEnd,
          volume: clip.volume,
          muted: clip.muted,
          id: clip.id,
          from: clip.from,
          to: clip.to,
          pos: clip.pos,
          comment: clip.comment,
          timestamp: clip.timestamp,
        },
      });
      this.props.undoAction(undoArr);
      this.props.redoAction([]);
    });
  };

  _pushImage = (image) => {
    let undoArr = _.cloneDeep(this.props.undoActions);
    let clips = _.cloneDeep(this.state.clips);
    if (clips.length === 0) {
      //add the empty drop
      clips.push({
        id: clips.length + 1,
        from: clips.length + 1,
        to: clips.length + 1,
        pos: clips.length + 1,
      });

      //add the clip before the empty drop
      image.id = clips.length - 1;
      image.from = clips.length - 1;
      image.to = clips.length - 1;
      image.pos = clips.length - 1;
      clips.splice(clips.length - 1, 0, image);
    } else {
      if (clips.length <= 26) {
        //set the add empty area position
        clips[clips.length - 1].id = clips.length;
        clips[clips.length - 1].from = clips.length;
        clips[clips.length - 1].to = clips.length;
        clips[clips.length - 1].pos = clips.length;

        //add the image before the empty drop
        image.id = clips.length - 1;
        image.from = clips.length - 1;
        image.to = clips.length - 1;
        image.pos = clips.length - 1;
        clips.splice(clips.length - 1, 0, image);
      }
    }

    this._setSelectedClip(image.id);
    this.setState({ clips }, () => {
      this.prevClipsState = _.cloneDeep(this.state.clips);
      this.props.videoContext({
        action: "add",
        type: "image",
        url: image.url,
        id: image.id,
        img: image.url,
        thumbnail: image.url,
        timestamp: image.timestamp,
        duration: image.duration,
        trimStart: image.trimStart,
        trimEnd: image.trimEnd,
        volume: 1,
        muted: false,
        local: image.local,
        pos: image.pos,
        comment: image.comment,
      });
      undoArr.push({
        action: "remove",
        media: {
          thumbnail: image.img,
          duration: image.duration,
          type: "image",
          img: image.img,
          url: image.url,
          trimStart: image.trimStart,
          trimEnd: image.trimEnd,
          volume: image.volume,
          muted: image.muted,
          id: image.id,
          from: image.from,
          to: image.to,
          pos: image.pos,
          comment: image.comment,
          timestamp: image.timestamp,
        },
      });
      this.props.undoAction(undoArr);
      this.props.redoAction([]);
    });
  };

  _removeClip = (id) => {
    let undoArr = _.cloneDeep(this.props.undoActions);
    let clips = _.cloneDeep(this.state.clips);
    let removeAt = clips.findIndex((el) => el.id === id);
    let clipToRemove = clips[removeAt];
    _.pullAt(clips, [removeAt]);
    let selectedClip = clips.length > 0 ? 0 : null;
    this._setSelectedClip(selectedClip);
    //setting the id, because this needs to be updated
    _.each(clips, (clip, key) => {
      if (clip.id > id) {
        clip.id -= 1;
        clip.from = clip.to;
        clip.to -= 1;
      } else {
        clip.from = clip.id;
        clip.to = clip.id;
      }
      clip.pos = clip.to;
    });

    if (clips.length === 1) {
      _.pullAt(clips, [clips.length - 1]);
    }

    this.setState({ clips: [] }, () => {
      this.setState({ clips }, () => {
        this.prevClipsState = _.cloneDeep(this.state.clips);
        this.props.videoContext({
          action: "remove",
          id: id,
        });
        undoArr.push({
          action: "add",
          media: {
            thumbnail: clipToRemove.img,
            duration: clipToRemove.duration,
            type: "video",
            img: clipToRemove.img,
            url: clipToRemove.url,
            trimStart: clipToRemove.trimStart,
            trimEnd: clipToRemove.trimEnd,
            volume: clipToRemove.volume,
            muted: clipToRemove.muted,
            id: clipToRemove.id,
            from: clipToRemove.from,
            to: clipToRemove.to,
            pos: clipToRemove.pos,
            comment: clipToRemove.comment,
            timestamp: clipToRemove.timestamp,
          },
        });
        this.props.undoAction(undoArr);
        this.props.redoAction([]);
      });
    });

    this._renderTimeline();
  };

  _moveClip = (dragId, hoverId) => {
    // console.log("dragId = ", dragId);
    // console.log("hoverId = ", hoverId);
    const clips = _.cloneDeep(this.state.clips);
    const dragIndex = clips.findIndex((el) => el.id === dragId);
    const hoverIndex = clips.findIndex((el) => el.id === hoverId);

    let setMoving = (id, direction) => {
      let movingIndex = _.findIndex(this.movingClips, {
        id: id,
      });
      if (movingIndex !== -1) {
        this.movingClips.splice(movingIndex, 1);
      }

      this.movingClips.push({ id: id });
    };

    let isMoving = (id, direction) => {
      let movingIndex = _.findIndex(this.movingClips, {
        id: id,
      });

      if (movingIndex === -1) {
        return false;
      } else {
        return true;
      }
    };

    _.each(clips, (clip, key) => {
      if (clip.to > clips[hoverIndex].to && clip.to < clips[dragIndex].to) {
        clip.from = clip.to;
        clip.to += 1;
      } else if (
        clip.to < clips[hoverIndex].to &&
        clip.to > clips[dragIndex].to
      ) {
        clip.from = clip.to;
        clip.to -= 1;
      }
      clip.to !== -1 ? (clip.pos = clip.to) : window.location.reload();
    });

    //the dragId needs to be sames as hoverId on order to match reactDND properties,
    //where the monitor is set as the hoverID
    if (clips[dragIndex].to > clips[hoverIndex].to) {
      if (!isMoving(hoverId)) {
        clips[dragIndex].from = clips[dragIndex].to;
        clips[dragIndex].to = clips[hoverIndex].to;

        clips[hoverIndex].from = clips[hoverIndex].to;
        clips[hoverIndex].to += 1;

        setMoving(hoverId);
      }
    } else {
      if (!isMoving(hoverId)) {
        clips[dragIndex].from = clips[dragIndex].to;
        clips[dragIndex].to = clips[hoverIndex].to;
        clips[hoverIndex].from = clips[hoverIndex].to;
        clips[hoverIndex].to -= 1;

        setMoving(hoverId);
      }
    }

    let collapsedClips = 0;

    _.each(clips, (clip, key) => {
      let filtered = _.filter(clips, (filterClip, key) => {
        return clip.pos === filterClip.pos;
      });

      if (filtered.length > 1) collapsedClips++;
    });

    this.setState({ clips, collapsedClips });
  };

  _resetPosition = () => {
    this.pushToPosition = false;
    this.setState({ clips: this.prevClipsState });
  };

  _setClipsId = (previousPositionalId) => {
    let undoArr = _.cloneDeep(this.props.undoActions);
    const clips = _.cloneDeep(this.state.clips);
    const originalOrder = _.cloneDeep(this.state.clips);
    let didItMove = 0;

    _.orderBy(clips, ["to"], ["asc"]);

    _.each(clips, (clip, key) => {
      // to check if the clip really moved, if the id is equal to the to, increment
      clip.id === clip.to && didItMove++;

      clip.id = clip.to;
      clip.from = clip.to;
    });

    this.setState({ clips }, () => {
      this.prevClipsState = _.cloneDeep(this.state.clips);

      this.props.videoContext({
        action: "move",
        unorderedClips: clips,
      });
      // if total didItMove count equals the lenght of clips array, don't add a undo action
      if (didItMove !== clips.length) {
        undoArr.push({
          action: "move",
          media: originalOrder,
        });
        this.props.undoAction(undoArr);
        this.props.redoAction([]);
      }
    });
  };

  _undoActionHandler = (action, media) => {
    let redoArr = _.cloneDeep(this.props.redoActions);
    let undoArr = _.filter(this.props.undoActions, (action, key) => {
      return key !== this.props.undoActions.length - 1;
    });
    let clips = _.cloneDeep(this.state.clips);
    //let clipKey = null;
    //let clipPos = null;

    switch (action) {
      case "add":
        clips.splice(media.id, 0, media);

        _.each(clips, (clip, key) => {
          if (key > media.id) {
            clip.id++;
            clip.to++;
            clip.pos++;
          }
        });

        this.setState({ clips }, () => {
          this.prevClipsState = _.cloneDeep(this.state.clips);
          this.props.videoContext({
            action: "add",
            type: media.type,
            url: media.url,
            id: media.id,
            img: media.img,
            thumbnail: media.img,
            timestamp: media.timestamp,
            duration: media.duration,
            trimStart: media.trimStart, //added after, but no effect on fullVideoCtx
            trimEnd: media.trimEnd, //added after, but no effect on fullVideoCtx
            volume: 1,
            muted: false,
            local: media.local,
            pos: media.pos,
            comment: media.comment,
          });
          redoArr.push({
            action: "remove",
            media: {
              thumbnail: media.img,
              duration: media.duration,
              type: media.type,
              img: media.img,
              url: media.url,
              trimStart: media.trimStart,
              trimEnd: media.trimEnd,
              volume: media.volume,
              muted: media.muted,
              id: media.id,
              from: media.from,
              to: media.to,
              pos: media.pos,
              comment: media.comment,
              timestamp: media.timestamp,
            },
          });
          this.props.redoAction(redoArr);
          this.props.undoAction(undoArr);
        });
        break;
      case "remove":
        let removeAt = clips.findIndex((el) => el.id === media.id);
        _.pullAt(clips, [removeAt]);

        _.each(clips, (clip, key) => {
          if (clip.id > media.id) {
            clip.id -= 1;
            clip.from = clip.to;
            clip.to -= 1;
          } else {
            clip.from = clip.id;
            clip.to = clip.id;
          }
          clip.pos = clip.to;
        });

        if (clips.length === 1) {
          _.pullAt(clips, [clips.length - 1]);
        }

        this.setState({ clips: [] }, () => {
          this.setState({ clips }, () => {
            this.prevClipsState = _.cloneDeep(this.state.clips);
            this.props.videoContext({
              action: "remove",
              id: media.id,
            });
            redoArr.push({
              action: "add",
              media: {
                thumbnail: media.img,
                duration: media.duration,
                type: media.type,
                img: media.img,
                url: media.url,
                trimStart: media.trimStart,
                trimEnd: media.trimEnd,
                volume: media.volume,
                muted: media.muted,
                id: media.id,
                from: media.from,
                to: media.to,
                pos: media.pos,
                comment: media.comment,
                timestamp: media.timestamp,
              },
            });
            this.props.redoAction(redoArr);
            this.props.undoAction(undoArr);
          });
        });
        break;
      case "move":
        let order = _.cloneDeep(media);
        let originalOrder = _.cloneDeep(clips);

        _.each(clips, (clip, key) => {
          clip.id = order[key].id;
          clip.from = order[key].id;
          clip.to = order[key].id;
          clip.pos = order[key].id;
        });

        this.setState({ clips }, () => {
          this.prevClipsState = _.cloneDeep(this.state.clips);
          this.props.videoContext({
            action: "move",
            unorderedClips: clips,
          });
          redoArr.push({
            action: "move",
            media: originalOrder,
          });
          this.props.redoAction(redoArr);
          this.props.undoAction(undoArr);
        });
        break;
      case "trim":
        let trimmedClip = _.cloneDeep(this.props.video[media.id]);
        clips[media.id].comment = media.comment;
        clips[media.id].trimStart = media.trimStart;
        clips[media.id].trimEnd = media.trimEnd;
        this.setState({ clips }, () => {
          this.prevClipsState = _.cloneDeep(this.state.clips);
          this.props.videoContext({
            action: "trim",
            media: {
              trimStart: media.trimStart,
              trimEnd: media.trimEnd,
              id: media.id,
              comment: media.comment,
            },
          });
          redoArr.push({
            action: "trim",
            media: trimmedClip,
          });
          this.props.redoAction(redoArr);
          this.props.undoAction(undoArr);
        });
        break;
      default:
        break;
    }
  };

  _redoActionHandler = (action, media) => {
    let undoArr = _.cloneDeep(this.props.undoActions);
    let redoArr = _.filter(this.props.redoActions, (action, key) => {
      return key !== this.props.redoActions.length - 1;
    });
    let clips = _.cloneDeep(this.state.clips);
    // let clipKey = null;
    // let clipPos = null;

    switch (action) {
      case "add":
        clips.splice(media.id, 0, media);

        _.each(clips, (clip, key) => {
          if (key > media.id) {
            clip.id++;
            clip.to++;
            clip.pos++;
          }
        });

        this.setState({ clips }, () => {
          this.prevClipsState = _.cloneDeep(this.state.clips);
          this.props.videoContext({
            action: "add",
            type: media.type,
            url: media.url,
            id: media.id,
            img: media.img,
            thumbnail: media.img,
            timestamp: media.timestamp,
            duration: media.duration,
            trimStart: media.trimStart, //added after, but no effect on fullVideoCtx
            trimEnd: media.trimEnd, //added after, but no effect on fullVideoCtx
            volume: 1,
            muted: false,
            local: media.local,
            pos: media.pos,
            comment: media.comment,
          });
          undoArr.push({
            action: "remove",
            media: {
              thumbnail: media.img,
              duration: media.duration,
              type: "video",
              img: media.img,
              url: media.url,
              trimStart: media.trimStart,
              trimEnd: media.trimEnd,
              volume: media.volume,
              muted: media.muted,
              id: media.id,
              from: media.from,
              to: media.to,
              pos: media.pos,
              comment: media.comment,
              timestamp: media.timestamp,
            },
          });
          this.props.undoAction(undoArr);
          this.props.redoAction(redoArr);
        });
        break;
      case "remove":
        let removeAt = clips.findIndex((el) => el.id === media.id);
        _.pullAt(clips, [removeAt]);

        _.each(clips, (clip, key) => {
          if (clip.id > media.id) {
            clip.id -= 1;
            clip.from = clip.to;
            clip.to -= 1;
          } else {
            clip.from = clip.id;
            clip.to = clip.id;
          }
          clip.pos = clip.to;
        });

        if (clips.length === 1) {
          _.pullAt(clips, [clips.length - 1]);
        }

        this.setState({ clips: [] }, () => {
          this.setState({ clips }, () => {
            this.prevClipsState = _.cloneDeep(this.state.clips);
            this.props.videoContext({
              action: "remove",
              id: media.id,
            });
            undoArr.push({
              action: "add",
              media: {
                thumbnail: media.img,
                duration: media.duration,
                type: "video",
                img: media.img,
                url: media.url,
                trimStart: media.trimStart,
                trimEnd: media.trimEnd,
                volume: media.volume,
                muted: media.muted,
                id: media.id,
                from: media.from,
                to: media.to,
                pos: media.pos,
                comment: media.comment,
                timestamp: media.timestamp,
              },
            });
            this.props.undoAction(undoArr);
            this.props.redoAction(redoArr);
          });
        });
        break;
      case "move":
        let order = _.cloneDeep(media);
        let originalOrder = _.cloneDeep(clips);

        _.each(clips, (clip, key) => {
          clip.id = order[key].id;
          clip.from = order[key].from;
          clip.to = order[key].from;
          clip.pos = order[key].from;
        });

        this.setState({ clips }, () => {
          this.prevClipsState = _.cloneDeep(this.state.clips);

          this.props.videoContext({
            action: "move",
            unorderedClips: clips,
          });
          undoArr.push({
            action: "move",
            media: originalOrder,
          });
          this.props.undoAction(undoArr);
          this.props.redoAction(redoArr);
        });
        break;
      case "trim":
        let trimmedClip = _.cloneDeep(this.props.video[media.id]);
        clips[media.id].comment = media.comment;
        clips[media.id].trimStart = media.trimStart;
        clips[media.id].trimEnd = media.trimEnd;
        this.setState({ clips }, () => {
          this.prevClipsState = _.cloneDeep(this.state.clips);
          this.props.videoContext({
            action: "trim",
            media: {
              trimStart: media.trimStart,
              trimEnd: media.trimEnd,
              id: media.id,
              comment: media.comment,
            },
          });
          undoArr.push({
            action: "trim",
            media: trimmedClip,
          });
          this.props.undoAction(undoArr);
          this.props.redoAction(redoArr);
        });
        break;
      default:
        break;
    }
  };
}

const timeline = {
  canDrop(props, monitor) {
    return props.video.length <= 25;
  },
  drop(props, monitor, component) {
    //const sourceObj = monitor.getItem();

    if (monitor.getItemType() === TYPES.CLIP) {
      if (component.pushToPosition) {
        component._pushClipToPosition(monitor.getItem(), null, "push");
      } else {
        component._pushClip(monitor.getItem());
      }
    }

    return {
      container: TYPES.TIMELINE,
    };
  },
  hover(props, monitor, component) {
    //const sourceObj = monitor.getItem();

    if (!monitor.isOver({ shallow: true })) {
      return false;
    } else {
      return true;
    }
  },
};

function mapStateToProps(state) {
  return {
    video: state.player.fullCtxData,
    playerTime: state.player.currentTime,
    dragAction: state.drag.data,
    undoActions: state.undo_redo.undoActions,
    redoActions: state.undo_redo.redoActions,
  };
}

export default compose(
  withRouter,
  connect(mapStateToProps, actions, null, { withRef: true }),
  DropTarget(TYPES.CLIP, timeline, Drop)
)(TimelineEdition);
