import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import * as actions from "redux/actions";
import _ from "lodash";

import { Box, Flex } from "@chakra-ui/react";
import VideoContext from "videocontext/dist/videocontext";
import PlayerPlayButton from "components/players/elements/PlayerPlayButton";
import PlayerLegendOverlay from "components/players/elements/PlayerLegendOverlay";
import PlayerControlsEdition from "components/players/elements/PlayerControlsEdition";

class PlayerVisitEditor extends Component {
  ctx = null;
  canvasRef = React.createRef();

  state = {
    currentTime: 0,
    totalTime: 0,
    isPlaying: false,
    volume: 1,
    commentFontSize: undefined,
  };

  video = [];
  activeClip = null;

  //This magical function triggers changes on video array at form->editor->video (a reduxForm inside State)
  //which triggers changes on the video array (also called videoctxdata etc) that is passed on props
  //which triggers componentDidUpdate in key components as Videobites (which also updates the form directly)
  _updateVideoReduxContext = () => {
    this.props.fullVideoContext(this.video);
  };

  render() {
    const { isPlaying, currentTime, totalTime, volume } = this.state;

    return (
      <Box boxShadow="default">
        <Flex position="relative" justify="center" align="center">
          <canvas
            style={{
              width: "100%",
              height: "56.25%",
              minWidth: "16px",
              minHeight: "9px",
            }}
            width="1280px"
            height="720px"
            id="playerCanvas"
            ref={this.canvasRef}
            onClick={this._pauseStart}
          />
          {this._displayTextOverlay(currentTime)}
          {!isPlaying && (
            <PlayerPlayButton
              onClick={() => {
                this._play();
              }}
            />
          )}
        </Flex>

        <PlayerControlsEdition
          onVolumeChange={(value) => this._onVolumeChange(value)}
          volume={volume}
          duration={totalTime}
          onPlay={this._play}
          isPlaying={isPlaying}
          onPause={this._pause}
          currentTime={currentTime}
          onSeek={this._onSeek}
        />
      </Box>
    );
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this._updateDimensions);

    this.setState(
      {
        currentTime: 0,
        isPlaying: false,
      },
      () => {
        this.props.currentTime(0);
      }
    );
    this.ctx.pause();
    this.ctx.currentTime = 0;
    this.ctx.unregisterCallback(this._onUpdate);
    this.ctx.unregisterCallback(this._onEnded);
    this.ctx.unregisterCallback(this._onStalled);
  }

  componentDidMount() {
    window.addEventListener("resize", this._updateDimensions);

    this.ctx = new VideoContext(document.getElementById("playerCanvas"));
    this._render();

    this.props.undoAction([]);
    this.props.redoAction([]);
  }

  componentDidUpdate() {
    !this.state.commentFontSize &&
      this.canvasRef.current &&
      setTimeout(() => {
        this.setState({
          commentFontSize: this.canvasRef.current.clientWidth / 65,
        });
      }, 100);
  }

  componentWillReceiveProps(nextProps) {
    const { ctxData } = nextProps;
    let clipKey = null;
    let clipPos = null;

    if (!_.isEmpty(ctxData)) {
      switch (ctxData.action) {
        case "add":
          //adds ctxData to video array (a.k.a fullCtxData), as sent from above to be populated
          this.video.splice(ctxData.id, 0, ctxData);

          _.each(this.video, (clip, key) => {
            if (key > ctxData.id) {
              clip.id++;
            }
          });

          clipKey = this.video.findIndex((el) => el.id === ctxData.id);

          this._resetVideoContext();
          this._setupClip(clipKey);
          this._render();
          break;
        case "remove":
          //remove the unwanted video
          let removeKey = this.video.findIndex((el) => el.id === ctxData.id);

          this.video.splice([removeKey], 1);

          this._resetVideoContext(); //This resets the video nodes from canvas, so they can be redraw from zero
          this._updateVideoReduxContext(); //This is necessary to update video array (state from forms)
          if (this.video.length !== 0) {
            //even when the video.length is 0 (deleting last video from timeline)
            //set a new video key
            if (ctxData.id === 0) {
              clipKey = 0;
            } else {
              clipKey = this.video.findIndex((el) => el.id === ctxData.id - 1);
            }

            //reorder the rest of the videos
            _.each(this.video, (clip, key) => {
              if (clip.id > ctxData.id) {
                clip.id -= 1;
              }
            });

            this._setupClip(clipKey);
            this._render();
          } else {
            this._resetVideoCanvas();
            //When deleting the LAST video from the timeline, clears canvas screen
            //because resetVideoContext does not do that after clearing nodes
          }

          break;
        case "move":
          _.each(this.video, (clip, key) => {
            clip.id = ctxData.unorderedClips[key].id;
            clip.pos = ctxData.unorderedClips[key].pos;
          });
          this._updateVideoReduxContext();
          this._resetVideoContext();
          this._render();
          break;
        case "clip":
          let videoArr = _.orderBy(_.cloneDeep(this.video), ["pos"], ["asc"]);
          clipKey = videoArr.findIndex((el) => el.id === ctxData.id);
          clipPos = videoArr.findIndex((el) => el.id === ctxData.pos);
          let startClip = 0;
          _.each(videoArr, (clip, key) => {
            if (clip.pos < clipPos) {
              startClip += clip.trimEnd - clip.trimStart;
            }
          });
          this._resetVideoContext();
          this.props.currentTime(startClip);
          this._setupClip(clipKey);
          this.setState({ currentTime: startClip });
          this._render();
          break;
        default:
          break;
      }
    }
  }

  _render = async () => {
    let video = _.cloneDeep(this.video);
    video = _.orderBy(video, ["pos"], ["asc"]);

    const ctx = this.ctx;

    ctx.reset();
    ctx.unregisterCallback(this._onUpdate);
    ctx.unregisterCallback(this._onEnded);
    ctx.unregisterCallback(this._onStalled);

    ctx.registerCallback("update", this._onUpdate);
    ctx.registerCallback("stalled", this._onStalled);
    ctx.registerCallback("ended", this._onEnded);

    let node = null;

    for (const item of video) {
      let duration = _.clone(ctx.duration);

      //if duration not set then null makes it have its duration, infinite if an image
      //if was trimed initial duration is maintained so trimEnd is used for calculation

      let stop = item.trimEnd - item.trimStart + duration || null;

      switch (item.type) {
        //when defined the startFrom the videos will automatically start from that timestamp
        case "video":
          node = ctx.video(item.url, item.trimStart, 0.1);
          node.connect(ctx.destination);
          node.start(duration);
          node.stop(stop);
          break;
        case "image":
          window.createImageBitmap = null;
          node = ctx.image(item.url, item.trimStart, 0.1);
          node.connect(ctx.destination);
          node.start(duration);
          node.stop(stop);
          break;
        default:
          break;
      }
    }

    ctx.currentTime = this.state.currentTime || 0;

    // if ctx.duration starts bugging --> test video.reduce((acummulator, value) => acummulator += (value.trimEnd - value.trimStart), 0)
    this.setState({
      totalTime: video.reduce(
        (acummulator, value) =>
          (acummulator += value.trimEnd - value.trimStart),
        0
      ),
    });
  };

  _resetVideoCanvas = () => {
    //Improvisation to clear canvas screen after deleting last video from timeline
    //because _resetVideoContext does not do that with ctx.reset() after clearing nodes
    let node = this.ctx.video("/a.mp4");
    node.connect(this.ctx.destination);
    node.start(0);
    node.stop(11);
    this.ctx.play();
  };

  _resetVideoContext = () => {
    this.ctx.reset();
    this.ctx.unregisterCallback(this._onUpdate);
    this.ctx.unregisterCallback(this._onEnded);
    this.ctx.unregisterCallback(this._onStalled);
    this.props.currentTime(0);
    this.setState({ currentTime: 0, totalTime: 0 });
  };

  _setupClip = (active) => {
    this.activeClip = active;
    this._updateVideoReduxContext();
  };

  _displayTextOverlay = (time) => {
    let startClip = 0,
      showComment = false,
      comment = "",
      location = "";

    let video = _.cloneDeep(this.video);
    video = _.orderBy(video, ["pos"], ["asc"]);

    for (let i = 0; i < video.length; i++) {
      if (
        time >= startClip &&
        time <= startClip + (video[i].trimEnd - video[i].trimStart)
      ) {
        comment = video[i].comment;
        location = video[i].location;
        showComment = true;
        break;
      } else {
        startClip += video[i].trimEnd - video[i].trimStart;
      }
    }

    // return the overlay div with the comment corresponding to the video time if there is a comment
    if (showComment && comment !== "") {
      if (location === "start") {
        const startComment = comment.split("?");

        return (
          <>
            <PlayerLegendOverlay
              position={"45%"}
              fontSize={this.state.commentFontSize * 2.5}
            >
              {startComment[0]}
            </PlayerLegendOverlay>
            <PlayerLegendOverlay
              position={["5%", "7%"]}
              fontSize={this.state.commentFontSize}
            >
              {startComment[1]}
            </PlayerLegendOverlay>
          </>
        );
      } else {
        return (
          <PlayerLegendOverlay
            position={["5%", "7%"]}
            fontSize={this.state.commentFontSize}
          >
            {comment}
          </PlayerLegendOverlay>
        );
      }
    } else {
      return null;
    }
  };

  _onUpdate = (value) => {
    if (value !== this.state.currentTime) {
      this.setState(
        {
          currentTime: value,
          isPlaying: this._isPlaying(),
        },
        () => {
          this.props.currentTime(value);
        }
      );
    }
  };

  _onStalled = (value) => {
    this.setState({
      isPlaying: false,
    });
  };

  _onEnded = () => {
    this.setState(
      {
        currentTime: 0,
        isPlaying: false,
      },
      () => {
        this.ctx.currentTime = 0;
        this.props.currentTime(0);
      }
    );
  };

  _onSeek = (time) => {
    this.ctx.currentTime = time;
    this.props.currentTime(time);
  };

  _pauseStart = () => {
    if (!_.isEmpty(this.ctx)) {
      if (this._isPlaying()) {
        this.ctx.pause();
        this.setState({
          isPlaying: false,
        });
      } else {
        this.ctx.play();
        this.setState({
          isPlaying: true,
        });
      }
    }
  };

  _pause = () => {
    if (!_.isEmpty(this.ctx)) {
      this.ctx.pause();
    }
  };

  _play = () => {
    if (!_.isEmpty(this.ctx)) {
      this.ctx.play();
    }
  };

  _isPlaying = () => {
    return this.ctx.state === 0;
  };

  _onVolumeChange = (value) => {
    this.ctx.volume = value;
    this.setState({ volume: value });
  };

  timeFormat = (time) => {
    let hrs = ~~(time / 3600);
    let mins = ~~((time % 3600) / 60);
    let secs = ~~time % 60;
    let ret = "";

    if (hrs > 0) {
      ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
    }
    ret += "" + mins + ":" + (secs < 10 ? "0" : "");
    ret += "" + secs;
    return ret;
  };

  _updateDimensions = () => {
    this.setState({ commentFontSize: this.canvasRef.current.clientWidth / 65 });
  };
}

function mapStateToProps(state) {
  return {
    ctxData: state.player.ctxData,
  };
}

export default compose(connect(mapStateToProps, actions))(PlayerVisitEditor);
