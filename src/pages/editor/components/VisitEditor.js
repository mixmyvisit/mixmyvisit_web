import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "redux/actions";

import { Link } from "react-router-dom";
import moment from "moment";
import localization from "moment/locale/pt";

import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalFooter,
  ModalBody,
  UnorderedList,
  List,
  ListItem,
  Button,
  IconButton,
  Heading,
  Text,
  Box,
  Flex,
  Image,
  Divider,
} from "@chakra-ui/react";

import InputEdit from "components/forms/InputEdit";
import Loader from "components/feedback/Loader";
import PrintToast from "components/feedback/printToast";
import { structureSaveClip } from "scripts/clipToDb";
import { getClip } from "storage/Storage";

class VideoEditor extends Component {
  state = {
    isOpenInfo: false,
    isLoading: false,
  };

  render() {
    const { videoInfo, visitCreation, player, storagedVisit } = this.props;
    const { title, video, submitted_at } = videoInfo;

    return (
      <Box>
        <Flex
          flexDir="row"
          wrap="wrap"
          align="center"
          pb={[8, 12]}
          justify={["space-between", "center"]}
        >
          <Flex
            align="center"
            as={Link}
            to={this.props.location?.state?.prevPath || "/"}
          >
            <Button
              py={2}
              px={6}
              h="auto"
              mb={[6, 0]}
              left={[null, "5%"]}
              variant="transparent"
              position={["relative", "absolute"]}
              leftIcon={
                <Image
                  h={8}
                  mr={3}
                  alt="Back"
                  objectFit="contain"
                  src={require("assets/icons/Back.svg")}
                />
              }
            >
              Voltar
            </Button>
          </Flex>

          <Flex
            flexDir="column"
            justify="center"
            order="3"
            textAlign="center"
            w={["100%", "auto"]}
          >
            {this.state.isLoading ? (
              <Box my={4}>
                <Loader />
              </Box>
            ) : (
              <InputEdit
                form="visitTitle"
                name="visitTitle"
                boldTitle={true}
                initialValue={storagedVisit.title}
                onSubmit={(value) => this._onChangeTitle(value)}
              />
            )}
            <Divider
              my={2}
              mx="auto"
              w={["50%", "100%"]}
              borderColor="primary.300"
            />
            <p>
              {moment(visitCreation)
                .locale("pt", localization)
                .format("D MMMM YYYY")}
            </p>
          </Flex>

          <IconButton
            px={2}
            mb={[6, 0]}
            order="2"
            right={[null, "5%"]}
            variant="transparent"
            position={["relative", "absolute"]}
            onClick={() =>
              this.setState({ isOpenInfo: !this.state.isOpenInfo })
            }
            icon={
              <Image
                h={12}
                alt="Info"
                src={require("assets/icons/Info.svg")}
                objectFit="contain"
              />
            }
          />

          <Modal
            onClose={() =>
              this.setState({ isOpenInfo: !this.state.isOpenInfo })
            }
            motionPreset="scale"
            isOpen={this.state.isOpenInfo}
            scrollBehavior="inside"
            isCentered
            size="xl"
          >
            <ModalOverlay>
              <ModalContent p={6}>
                <ModalBody>
                  <Heading as="h3">Informação da Visita</Heading>
                  <List>
                    <ListItem>Título: {title}</ListItem>
                    <ListItem>
                      Data de criação:{" "}
                      {moment(visitCreation)
                        .locale("pt", localization)
                        .format("D MMMM YYYY")}
                    </ListItem>
                    <ListItem>
                      Data da última submissão:{" "}
                      {moment(submitted_at)
                        .locale("pt", localization)
                        .format("D MMMM YYYY")}
                    </ListItem>
                    <ListItem>
                      Número de videos/imagens: {video.length}
                    </ListItem>
                    <ListItem>
                      Duração total: {_getTotalDuration(video)}
                    </ListItem>
                  </List>

                  <Divider my={6} />

                  <Heading as="h3">Ajuda</Heading>
                  <Text pb={2}>
                    Nesta página pode modificar a sua visita consoante os seus
                    gostos, podendo mudar a ordem de <i>clips</i>, adicionar
                    novas imagens e vídeos ou remover e cortar esses mesmos
                    ficheiros.
                  </Text>
                  <UnorderedList spacing={3}>
                    <ListItem>
                      Para mudar a ordem de um item carregue na sua thumbnail e
                      largue a mesma no local desejado.
                    </ListItem>
                    <ListItem>
                      Para adicionar um novo item pressione no respetivo botão
                      de adição de um novo item, representado pelo ícone de
                      mais.
                    </ListItem>
                    <ListItem>
                      Para modificar um item, como a sua duração e se possuí um
                      comentário, carregue na sua thumbnail durante 5 segundos
                      ou mais e será redirecionado para a página de edição desse
                      mesmo item.
                    </ListItem>
                  </UnorderedList>
                  <ModalFooter>
                    <Button
                      variant="primary"
                      mr={3}
                      onClick={() =>
                        this.setState({
                          isOpenInfo: !this.state.isOpenInfo,
                        })
                      }
                    >
                      Fechar
                    </Button>
                  </ModalFooter>
                </ModalBody>
              </ModalContent>
            </ModalOverlay>
          </Modal>
        </Flex>

        {player}
      </Box>
    );
  }

  _onChangeTitle = (value) => {
    this.setState({ isLoading: true });
    let clip = getClip();
    clip.title = value;
    this.props.updateClip(
      structureSaveClip(clip),
      clip.id,
      () => {
        this.props.saveCliptoStorage(clip);
        this.setState({ isLoading: false });
        PrintToast(
          "success",
          `O título da sua visita foi alterado com sucesso!`
        );
      },
      (error) => {
        this.setState({ isLoading: false });
        PrintToast(
          "error",
          `Pedimos desculpa mas ocorreu um erro na alteração do título da sua visita`
        );
      }
    );
  };
}

const _getTotalDuration = (arr) => {
  let dur = 0;
  if (arr) {
    for (let i = 0; i < arr.length; i++) {
      let clipDuration = arr[i].trimEnd - arr[i].trimStart;
      dur += clipDuration;
    }
  }

  return timeFormat(dur);
};

const timeFormat = (time) => {
  let hrs = ~~(time / 3600);
  let mins = ~~((time % 3600) / 60);
  let secs = ~~time % 60;
  let ret = "";

  if (hrs > 0) {
    ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
  }
  ret += "" + mins + ":" + (secs < 10 ? "0" : "");
  ret += "" + secs;
  return ret;
};

function mapStateToProps(state) {
  return {
    storagedVisit: state.storageClip.clip,
  };
}

export default connect(mapStateToProps, actions)(VideoEditor);
