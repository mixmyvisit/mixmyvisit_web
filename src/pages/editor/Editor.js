import React, { Component } from "react";
import { reduxForm } from "redux-form";
import * as actions from "redux/actions";
import { compose } from "redux";
import { connect } from "react-redux";

import { DragDropContext } from "react-dnd";
import HTML5Backend from "react-dnd-html5-backend";
import TouchBackend from "react-dnd-touch-backend";
import { isMobile } from "react-device-detect";
import _ from "lodash";

import { getClip } from "storage/Storage";
import requireAuth from "components/auth/requireAuth";
import requireEditorForm from "scripts/HOC/requireEditorForm";
import Loading from "components/feedback/Loading";
import Error from "components/feedback/Error";
import ConfirmationLeaveModal from "components/modals/ConfirmationLeaveModal";

import VisitEditor from "./components/VisitEditor";
import PlayerVisitEditor from "./components/PlayerVisitEditor";
import TimelineEdition from "./components/timeline/TimelineEdition";
import SEO from "components/Seo";

import {
  Button,
  CircularProgress,
  Flex,
  Heading,
  Icon,
  Text,
} from "@chakra-ui/react";
import { FiRotateCw } from "react-icons/fi";

class Editor extends Component {
  state = {
    isLoading: true,
    error: false,
    didFinishRender: false,
    createdAt: null,
    originalVideo: null,
    didEdit: false,
    isOpenRenderModal: false,
    isLoadingRenderModal: false,
  };

  render() {
    const {
      isLoading,
      error,
      didEdit,
      createdAt,
      visitEditorForm,
      visitState,
      visitRenderedDate,
    } = this.state;

    return isLoading ? (
      <Loading />
    ) : error ? (
      <Error />
    ) : visitState === 2 || visitState === 8 || visitState === 16 ? (
      <>
        <SEO title="Editor" />
        <Flex justify="center" align="center" flexDir="column">
          <CircularProgress
            thickness="5px"
            capIsRound={true}
            isIndeterminate
            trackColor="primary.100"
            color="primary.500"
            size={20}
          />
          <Heading as="h1" textAlign="center" pt={6}>
            A sua visita, {visitEditorForm.title}, encontra-se a ser processada
            no nosso <i>render</i>.
          </Heading>
          <Text mb={4}>
            Quando o processo de renderização da sua visita terminar será
            notificado via <b>email</b> e pelo <b>Facebook Messenger</b>.
          </Text>
          <Text mb={4}>
            Este processo de renderização pode demorar alguns minutos pelo que a
            página irá atualizar-se de 2 em 2 minutos automaticamente ou então
            pode carregar no botão de "recarregar" para verificar se o processo
            terminou.
          </Text>
          <Button
            h="auto"
            variant="transparent"
            leftIcon={<Icon as={FiRotateCw} h={7} w={7} mr={3} />}
            onClick={() => this._reloadPage()}
          >
            Recarregar página
          </Button>
        </Flex>
      </>
    ) : (
      <>
        <SEO title="Editor" />

        <VisitEditor
          videoInfo={visitEditorForm}
          visitCreation={createdAt}
          player={
            <PlayerVisitEditor handleRenderFunc={this._handleFinishRendering} />
          }
        />
        <TimelineEdition
          didEditVisit={didEdit}
          didRenderOnce={visitRenderedDate}
          videoInfo={visitEditorForm}
          handleSubmitVisit={this._handleSubmitVisit}
        />
        <ConfirmationLeaveModal
          when={didEdit}
          navigate={(path) => this.props.history.push(path)}
          shouldBlockNavigation={didEdit}
        />
      </>
    );
  }

  componentDidMount() {
    // verify if there is content in editorForm, if not its needed to do a call to the API to get the form info
    // this happens when the user refresh the page on the browser
    const visitId = this.props.match.params.clip.split("=");
    this.props.getClip(
      parseInt(visitId[1]),
      () => {
        const editorForm = JSON.parse(this.props.clip.editor_form);
        this.props.saveCliptoStorage(editorForm);
        const getVisit = getClip();

        this.setState({
          isLoading: false,
          visitState: this.props.clip.states_id,
          createdAt: this.props.clip.created_at,
          originalVideo: getVisit,
          visitEditorForm: editorForm,
          visitRenderedDate: this.props.renderer_started_at,
          // downloadUrl: this.props.clip.download_url,
        });
      },
      () => this.setState({ isLoading: false, error: true })
    );

    const { visitState } = this.state;
    if (visitState === 8 || visitState === 14 || visitState === 2) {
      setTimeout(() => {
        this._reloadPage();
      }, 120000);
    }
  }

  componentDidUpdate(prevProps) {
    if (!_.isEqual(prevProps.video, this.props.video)) {
      const { video } = this.props;
      video.sort((a, b) => a.id - b.id);
      const clip = getClip();
      const updateClipInStorage = {
        ...clip,
        video: [...video],
      };
      this.props.saveCliptoStorage(updateClipInStorage);
    }

    const videoToCompare = getClip();
    const isVideoDifferent = !_.isEqual(
      this.state.originalVideo?.video,
      videoToCompare.video
    );

    // console.log("COMPARE");
    // console.log(this.state.originalVideo?.video);
    // console.log(videoToCompare.video);

    if (this.state.didEdit !== isVideoDifferent) {
      this.setState({ didEdit: isVideoDifferent });
    }
  }

  componentWillUnmount() {
    this.props.saveCliptoStorage(this.state.originalVideo);
  }

  _reloadPage = () => {
    window.location.reload();
  };

  _handleLoading = () => {
    this.setState({ isLoading: !this.state.isLoading });
  };

  _handleSubmitVisit = () => {
    this.setState({ originalVideo: this.props.storagedClip, didEdit: false });
  };

  _handleFinishRendering = () => {
    this.setState({ didFinishRender: true });
  };
}

//This video below is initially passed up empty and its going to be filled in the child TimelineEdition component
function mapStateToProps(state) {
  return {
    video: state.player.fullCtxData,
    storagedClip: state.storageClip.clip,
    clip: state.clip.getClipSuccess,
  };
}

let backend = isMobile ? TouchBackend : HTML5Backend;

export default compose(
  DragDropContext(backend),
  requireAuth,
  requireEditorForm,
  connect(mapStateToProps, actions),
  reduxForm({
    form: "editor",
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
  })
)(Editor);
