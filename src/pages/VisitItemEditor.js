import React, { Component, createRef } from "react";
import { compose } from "redux";
import { reduxForm } from "redux-form";
import { connect } from "react-redux";
import * as actions from "redux/actions";

import ReactPlayer from "react-player";
import { Link, withRouter } from "react-router-dom";
import _ from "lodash";

import {
  Box,
  Flex,
  SimpleGrid,
  Button,
  IconButton,
  Image as ChakraImage,
  Heading,
  Textarea,
  Text,
  Divider,
  CircularProgress,
} from "@chakra-ui/react";
import { TimePicker, TimePrecision } from "@blueprintjs/datetime";

import { getClip } from "storage/Storage";
import { structureSaveClip } from "scripts/clipToDb";
import requireAuth from "components/auth/requireAuth";
import requireEditorForm from "scripts/HOC/requireEditorForm";

import Loading from "components/feedback/Loading";
import Error from "components/feedback/Error";
import PrintToast from "components/feedback/printToast";
import PlayerControlsEdition from "components/players/elements/PlayerControlsEdition";
import TrimmableEdition from "components/players/elements/TrimmableEdition";
import SliderEdition from "components/players/elements/SliderEdition";
import ConfirmationRenderModal from "components/modals/ConfirmationRenderModal";
import ConfirmationLeaveModal from "components/modals/ConfirmationLeaveModal";
import PlayerLegendOverlay from "components/players/elements/PlayerLegendOverlay";
import PlayerPlayButton from "components/players/elements/PlayerPlayButton";
import SEO from "components/Seo";

class VisitItemEditor extends Component {
  visitItem = createRef();

  state = {
    error: null,
    isLoading: true,
    isBuffering: false,
    didEdit: false,
    volume: 1,
    isOpenRenderModal: false,
    isLoadingRenderModal: false,
    commentFontSize: null,
    actionsLimit: 15,
    currentVisitItem: {},
    nextVisitItem: {},
    previousVisitItem: {},
    originalCurrentVisitItem: {},
  };

  render() {
    const {
      isLoading,
      error,
      isPlaying,
      isBuffering,
      currentTime,
      volume,
      totalTime,
      visitName,
      currentVisitItem,
      nextVisitItem,
      previousVisitItem,
      didEdit,
      isOpenRenderModal,
    } = this.state;
    const { comment, duration, location, type, url, trimStart, trimEnd, id } =
      currentVisitItem;
    const { undoActions, redoActions } = this.props;

    return (
      <>
        {isLoading ? (
          <Loading />
        ) : error ? (
          <Error />
        ) : (
          <>
            <SEO title="Editor de Clip" />
            <Flex
              flexDir="row"
              wrap="wrap"
              align="center"
              justify={["space-between", "center"]}
              pb={[8, 12]}
            >
              <Flex
                as={Link}
                align="center"
                to={`/editor/${this.props.match.params.clip}`}
              >
                <Button
                  h="auto"
                  py={2}
                  px={6}
                  mb={[6, 0]}
                  variant="transparent"
                  left={[null, "5%"]}
                  position={["relative", "absolute"]}
                  leftIcon={
                    <ChakraImage
                      alt="Back"
                      src={require("assets/icons/Back.svg")}
                      objectFit="contain"
                      h={8}
                      mr={3}
                    />
                  }
                >
                  Voltar à visita
                </Button>
              </Flex>

              <Flex
                flexDir="column"
                justify="center"
                order="3"
                textAlign="center"
                w={["100%", "auto"]}
              >
                <Heading pb={0} as="h2">
                  {visitName}
                </Heading>
                <Divider
                  my={2}
                  mx="auto"
                  w={["50%", "100%"]}
                  borderColor="primary.300"
                />
                <Heading pb={0} as="h3">
                  {type === "image" ? "imagem" : "vídeo"} - {_.add(id, 1)}
                </Heading>
              </Flex>

              <IconButton
                order="2"
                px={2}
                mb={[6, 0]}
                variant="transparent"
                position={["relative", "absolute"]}
                right={[null, "5%"]}
                onClick={() =>
                  this.setState({ isOpenInfo: !this.state.isOpenInfo })
                }
                icon={
                  <ChakraImage
                    alt="Info"
                    src={require("assets/icons/Info.svg")}
                    objectFit="contain"
                    h={12}
                  />
                }
              />
            </Flex>

            {type === "video" ? (
              <Box boxShadow="default" mb={8}>
                <Flex justify="center" align="center" position="relative">
                  <ReactPlayer
                    width="100%"
                    height="100%"
                    ref={this.visitItem}
                    url={url}
                    controls={false}
                    playing={isPlaying}
                    volume={volume}
                    onStart={this._play}
                    onPlay={this._play}
                    onPause={this._pause}
                    onProgress={this._onProgress}
                    onDuration={this._onDuration}
                    onBuffer={() => this.setState({ isBuffering: true })}
                    onBufferEnd={() => this.setState({ isBuffering: false })}
                    onClick={isPlaying ? this._play : this._pause}
                  />
                  {isBuffering && isPlaying && (
                    <Box
                      zIndex="3"
                      minWidth="2.5rem"
                      position="absolute"
                      bg="rgba(255, 255, 255, 0.5)"
                      py="1.5rem"
                      px={0}
                      h={["4.5rem", "5rem", "7rem"]}
                      w={["4.5rem", "5rem", "7rem"]}
                      borderRadius="9999px"
                      display="inline-flex"
                      justifyContent="center"
                      alignItems="center"
                      boxShadow="0 1px 1px rgb(0 0 0 / 8%), 0 2px 2px rgb(0 0 0 / 8%), 0 4px 4px rgb(0 0 0 / 8%), 0 6px 6px rgb(0 0 0 / 8%), 0 8px 10px rgb(0 0 0 / 8%)"
                    >
                      <CircularProgress
                        isIndeterminate
                        thickness="10px"
                        color="primary.500"
                        trackColor="primary.100"
                      />
                    </Box>
                  )}
                  {!isPlaying && (
                    <PlayerPlayButton onClick={() => this._play()} />
                  )}
                  {comment && this._displayCommentOverlay(comment)}
                </Flex>
                <PlayerControlsEdition
                  onVolumeChange={(value) => this._onVolumeChange(value)}
                  volume={volume}
                  duration={totalTime}
                  onPlay={this._play}
                  isPlaying={isPlaying}
                  onPause={this._pause}
                  currentTime={currentTime}
                  onSeek={this._onSeek}
                />
              </Box>
            ) : (
              <Box mb={8} boxShadow="default">
                <Flex position="relative" justify="center" align="center">
                  <canvas
                    style={{
                      width: "100%",
                      height: "56.25%",
                      minWidth: "16px",
                      minHeight: "9px",
                    }}
                    width="1280px"
                    height="720px"
                    id="imageCanvas"
                    ref={this.visitItem}
                  />
                  {comment && this._displayCommentOverlay(comment)}
                </Flex>
              </Box>
            )}

            <SimpleGrid
              alignItems="center"
              columns={[2, 3]}
              gridGap={[6, 8]}
              pb={8}
              w={["full", "auto"]}
            >
              <Button
                h="auto"
                variant="transparent"
                display="flex"
                flexDirection="column"
                alignItems="center"
                isDisabled={undoActions.length !== 0 ? false : true}
                onClick={() => this._undoActionHandler()}
              >
                <ChakraImage
                  h={16}
                  objectFit="contain"
                  alt="Undo"
                  src={require("assets/icons/Undo.svg")}
                />
                <p>Desfazer</p>
              </Button>

              <Button
                order={[3, 2]}
                mx={["auto", null]}
                variant="primary"
                w={[40, "100%"]}
                isDisabled={!didEdit}
                gridColumnStart={[1, "auto"]}
                gridColumnEnd={[3, "auto"]}
                onClick={() =>
                  didEdit &&
                  this.setState({
                    isOpenRenderModal: !this.state.isOpenRenderModal,
                  })
                }
              >
                Atualizar {type === "image" ? "imagem" : "vídeo"}
              </Button>

              <Button
                h="auto"
                order={[2, 3]}
                variant="transparent"
                display="flex"
                flexDirection="column"
                alignItems="center"
                isDisabled={redoActions.length !== 0 ? false : true}
                onClick={() => this._redoActionHandler()}
              >
                <ChakraImage
                  h={16}
                  objectFit="contain"
                  alt="Redo"
                  src={require("assets/icons/Redo.svg")}
                />
                <p>Refazer</p>
              </Button>
            </SimpleGrid>

            <Box pb={[8, 12]}>
              <Heading as="h3">
                Duração {type === "image" ? "da imagem" : "do vídeo"}
              </Heading>
              <Flex
                w="100%"
                pb={4}
                pt={2}
                justify="space-between"
                align="center"
              >
                {type === "video" ? (
                  <TrimmableEdition
                    onChange={(value) => this._onSeek(value)}
                    currentTime={(currentTime * 100) / duration || 0}
                    trimStart={(trimStart * 100) / duration || 0}
                    trimEnd={(trimEnd * 100) / duration || 0}
                    duration={duration}
                    ratio={100}
                    onTrim={(value) => this._onTrim(value)}
                    allowCross={false}
                    onTrimEnd={(value) => this._onTrimEnd(value)}
                    min={0}
                    max={100}
                  />
                ) : (
                  <SliderEdition
                    duration={duration}
                    trimStart={(trimStart * 100) / duration || 0}
                    trimEnd={(trimEnd * 100) / duration || 0}
                    ratio={100}
                    onTrim={(value) => {
                      let { duration } = this.state.currentVisitItem;
                      let trimStart = (value.trimStart * duration) / 100;
                      let trimEnd = (value.trimEnd * duration) / 100;

                      this.setState({
                        currentVisitItem: {
                          ...currentVisitItem,
                          trimStart: trimStart,
                          trimEnd: trimEnd,
                        },
                      });
                    }}
                    onTrimEnd={(value) => this._onTrimEnd(value)}
                    min={0}
                    max={100}
                  />
                )}
              </Flex>

              <Flex
                flexWrap="wrap"
                justify={["center", "space-between"]}
                align="center"
                w="100%"
              >
                <Flex w={["100%", "auto"]} flexDir="column" align="flex-start">
                  <ChakraImage
                    mb={2}
                    h={10}
                    objectFit="contain"
                    src={require("assets/icons/PlayEdit.svg")}
                    alt="Clip Start"
                  />

                  <Box
                    as={TimePicker}
                    sx={{
                      svg: {
                        display: "inline",
                        verticalAlign: "baseline",
                        fill: "primary.400",
                      },
                    }}
                    maxTime={new Date(2011, 0, 1, 0, 0, 20, 0)}
                    minTime={new Date(2011, 0, 1, 0, 0, 0, 0)}
                    showArrowButtons={true}
                    precision={TimePrecision.SECOND}
                    value={new Date(2011, 0, 1, 0, 0, trimStart, 0)}
                    onChange={(time) => {
                      const hours = time.getHours();
                      const minutes = time.getMinutes();
                      const seconds = time.getSeconds();
                      let returnTime = 0;

                      if (hours !== 0) {
                        returnTime += ~~(hours / 3600);
                      } else if (minutes !== 0) {
                        returnTime += ~~((minutes % 3600) / 60);
                      } else {
                        returnTime += seconds;
                      }

                      this.visitItem.current.seekTo(returnTime);
                      this.setState({
                        currentVisitItem: {
                          ...currentVisitItem,
                          trimStart: returnTime,
                        },
                      });
                      this._onTrimEnd({ trimStart: returnTime, trimEnd });
                    }}
                  />
                </Flex>

                <Flex w={["100%", "auto"]} flexDir="column" align="flex-end">
                  <ChakraImage
                    mb={2}
                    h={10}
                    objectFit="contain"
                    src={require("assets/icons/TimeEdit.svg")}
                    alt="Clip End"
                  />

                  <Box
                    as={TimePicker}
                    sx={{
                      svg: {
                        display: "inline",
                        verticalAlign: "baseline",
                        fill: "primary.400",
                      },
                    }}
                    maxTime={new Date(2011, 0, 1, 0, 0, 20, 0)}
                    minTime={new Date(2011, 0, 1, 0, 0, 0, 0)}
                    showArrowButtons={true}
                    precision={TimePrecision.SECOND}
                    value={new Date(2011, 0, 1, 0, 0, trimEnd, 0)}
                    onChange={(time) => {
                      const hours = time.getHours();
                      const minutes = time.getMinutes();
                      const seconds = time.getSeconds();
                      let returnTime = 0;

                      if (hours !== 0) {
                        returnTime += ~~(hours / 3600);
                      } else if (minutes !== 0) {
                        returnTime += ~~((minutes % 3600) / 60);
                      } else {
                        returnTime += seconds;
                      }

                      this.visitItem.current.seekTo(returnTime);
                      this.setState({
                        currentVisitItem: {
                          ...currentVisitItem,
                          trimEnd: returnTime,
                        },
                      });
                      this._onTrimEnd({ trimStart, trimEnd: returnTime });
                    }}
                  />
                </Flex>
              </Flex>
            </Box>

            <Flex pb={[10, 16]} flexDir="column" justify="start">
              <Heading as="h3">Comentário</Heading>
              {location !== "start" ? (
                <Textarea
                  h={40}
                  w="100%"
                  id="comment"
                  name="comment"
                  bg="grey.400"
                  boxShadow="default"
                  borderRadius="4px"
                  value={comment || ""}
                  type="textarea"
                  placeholder={`Comentário para ${
                    type === "image" ? "a imagem" : "o vídeo"
                  }`}
                  onChange={(ev) => {
                    this.setState(
                      {
                        currentVisitItem: {
                          ...currentVisitItem,
                          comment: ev.target.value,
                        },
                      },
                      () => {
                        this.props.videoContext({
                          ...this.state.currentVisitItem,
                        });
                      }
                    );
                  }}
                />
              ) : (
                <Text>
                  O comentário deste vídeo inicial é gerado automaticamente
                  aquando o processo de <i>render</i> do vídeo da visita sendo a
                  alteração do conteúdo do mesmo não permitida
                </Text>
              )}
            </Flex>

            <Flex
              flexDir="row"
              flexWrap="wrap"
              align="center"
              justify={
                previousVisitItem && nextVisitItem
                  ? "space-between"
                  : previousVisitItem
                  ? "flex-start"
                  : "flex-end"
              }
            >
              {previousVisitItem && (
                <Link
                  to={`/editor/${this.props.match.params.clip}/${_.subtract(
                    this.props.match.params.specificClip,
                    1
                  )}`}
                >
                  <Button
                    h="auto"
                    py={2}
                    px={6}
                    variant="transparent"
                    leftIcon={
                      <ChakraImage
                        h={8}
                        mr={3}
                        alt="Back"
                        objectFit="contain"
                        src={require("assets/icons/Back.svg")}
                      />
                    }
                  >
                    <span>
                      Ir para{" "}
                      {previousVisitItem.type === "image"
                        ? "a imagem"
                        : "o vídeo"}{" "}
                      anterior da visita
                    </span>
                  </Button>
                </Link>
              )}

              {nextVisitItem && (
                <Link
                  to={`/editor/${this.props.match.params.clip}/${_.add(
                    this.props.match.params.specificClip,
                    1
                  )}`}
                >
                  <Button
                    h="auto"
                    py={2}
                    px={6}
                    variant="transparent"
                    rightIcon={
                      <ChakraImage
                        h={8}
                        ml={3}
                        alt="Back"
                        objectFit="contain"
                        transform="rotate(-180deg)"
                        src={require("assets/icons/Back.svg")}
                      />
                    }
                  >
                    <span>
                      Ir para{" "}
                      {nextVisitItem.type === "image"
                        ? "a próxima imagem"
                        : "o próximo vídeo"}{" "}
                      da visita
                    </span>
                  </Button>
                </Link>
              )}
            </Flex>

            <ConfirmationLeaveModal
              when={didEdit}
              navigate={(path) => this.props.history.push(path)}
              shouldBlockNavigation={didEdit}
            />

            <ConfirmationRenderModal
              updateTypeItem={type}
              isModalLoading={this.state.isLoadingRenderModal}
              isModalOpen={this.state.isOpenRenderModal}
              handleClose={() =>
                this.setState({
                  isOpenRenderModal: !this.state.isOpenRenderModal,
                })
              }
              handleConfirmation={() => {
                this.setState({
                  isLoadingRenderModal: !this.state.isLoadingRenderModal,
                });
                const updateVisit = getClip();

                updateVisit.video[this.state.currentVisitItem.id] = {
                  ...updateVisit.video[this.state.currentVisitItem.id],
                  trimEnd: trimEnd,
                  trimStart: trimStart,
                  comment: comment,
                };

                this.props.updateClip(
                  structureSaveClip(updateVisit),
                  updateVisit.id,
                  () => {
                    this.props.updateForm("editor", "video", updateVisit.video);
                    this.setState({
                      didEdit: false,
                      isOpenRenderModal: !isOpenRenderModal,
                      originalCurrentVisitItem: updateVisit,
                    });
                    this.props.saveCliptoStorage(updateVisit);
                    PrintToast("success", "Item atualizado com sucesso!");
                    setTimeout(() => {
                      this.setState({ isLoadingRenderModal: false });
                    }, 200);
                  },
                  (error) => {
                    this.setState({
                      isOpenRenderModal: !isOpenRenderModal,
                      didEdit: false,
                    });
                    PrintToast(
                      "error",
                      `Ocorreu um erro a atualizar o seu item. Tente novamente mais tarde`
                    );
                    setTimeout(() => {
                      this.setState({
                        isLoadingRenderModal: false,
                        didEdit: true,
                      });
                    }, 200);
                  }
                );
              }}
            />
          </>
        )}
      </>
    );
  }

  componentDidMount() {
    window.addEventListener("resize", this._updateDimensions);
    this.props.undoAction([]);
    this.props.redoAction([]);
    const visitId = this.props.match.params.clip.split("=");
    const currentVisit = getClip();
    const itemId = parseInt(this.props.match.params.specificClip);

    if (!currentVisit) {
      this.props.getClip(
        visitId[2],
        () => {
          const editorForm = JSON.parse(this.props.clip.editor_form);
          const visitTitle = this.props.clip.title;
          this._setInitialStateVisitItem(editorForm, itemId, visitTitle);
        },
        (error) => {
          PrintToast(
            "error",
            `Ocorreu um erro ao obter a data deste item. Tente novamente mais tarde`
          );
          this.setState({ isLoading: false, error: error });
        }
      );
    } else {
      const visitTitle = currentVisit.title;
      this._setInitialStateVisitItem(currentVisit, itemId, visitTitle);
    }
  }

  componentDidUpdate(prevProps) {
    const { isLoading, error, currentVisitItem, didEdit } = this.state;

    if (
      !_.isEqual(prevProps.visitItem, this.props.visitItem) &&
      !_.isEmpty(prevProps.visitItem) &&
      !_.isEmpty(this.props.visitItem)
    ) {
      const { updateForm, form, visitItem, undoActions } = this.props;
      const { currentVisitItem, actionsLimit } = this.state;
      delete currentVisitItem["action"];

      if (!_.isEmpty(prevProps.visitItem) && !_.isEmpty(visitItem)) {
        if (prevProps.undoActions === this.props.undoActions) {
          const sendUndoItem = {
            trimStart: prevProps.visitItem.trimStart,
            trimEnd: prevProps.visitItem.trimEnd,
            comment: prevProps.visitItem.comment,
          };

          undoActions.length === actionsLimit && undoActions.pop();
          undoActions.push(sendUndoItem);
          this.props.undoAction(undoActions);
        }
      }

      let clip = getClip();
      let videoForm = clip.video;
      videoForm[currentVisitItem.id] = { ...this.state.currentVisitItem };
      updateForm(form, "video", videoForm);
    }

    // load canvas to display player for image
    if (!isLoading && !error && currentVisitItem.type === "image") {
      const canvas = document.getElementById("imageCanvas");

      let image = new Image();
      image.src = this.state.currentVisitItem.url;
      image.onload = function () {
        const canvasContext = canvas.getContext("2d");

        canvasContext.drawImage(
          image,
          0,
          0,
          image.width,
          image.height,
          0,
          0,
          canvas.width,
          canvas.height
        );
      };
    }

    // logic to determine if the alert 'did you changed this video?' appears
    const { id } = currentVisitItem;

    const videoToCompare = getClip();

    const isVideoDifferent = !_.isEqual(
      videoToCompare.video[id],
      currentVisitItem
    );

    if (didEdit !== isVideoDifferent) {
      this.setState({
        didEdit: isVideoDifferent,
      });
    }

    // set the fontSize for the comment overlay with a timeout to make ref exist
    const itemType = this.state.currentVisitItem.type;
    !this.state.commentFontSize &&
      this.visitItem.current &&
      setTimeout(() => {
        this.setState({
          commentFontSize:
            itemType === "video"
              ? this.visitItem.current.wrapper.clientWidth / 65
              : this.visitItem.current.clientWidth / 65,
        });
      }, 100);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this._updateDimensions);
    if (this.state.didEdit) {
      this.props.saveCliptoStorage(this.state.originalCurrentVisitItem);
      this.props.undoAction([]);
      this.props.redoAction([]);
    }
  }

  _displayCommentOverlay = (comment) => {
    if (this.state.currentVisitItem.location === "start") {
      const startComment = comment.split("?");

      return (
        <>
          <PlayerLegendOverlay
            position={"45%"}
            fontSize={this.state.commentFontSize * 2.5}
          >
            {startComment[0]}
          </PlayerLegendOverlay>
          <PlayerLegendOverlay
            position={["5%", "7%"]}
            fontSize={this.state.commentFontSize}
          >
            {startComment[1]}
          </PlayerLegendOverlay>
        </>
      );
    } else {
      return (
        <PlayerLegendOverlay
          position={["5%", "7%"]}
          fontSize={this.state.commentFontSize}
        >
          {comment}
        </PlayerLegendOverlay>
      );
    }
  };

  _setInitialStateVisitItem = (visit, visitId, visitTitle) => {
    const itemInfo = visit.video[visitId];
    const nextVisitItem = visit.video[_.add(visitId, 1)];
    const previousVisitItem = visit.video[_.subtract(visitId, 1)];

    this.props.saveCliptoStorage(visit);
    this._loadItemToContext(itemInfo, visitId);

    this.setState({
      isLoading: false,
      visitName: visitTitle,
      nextVisitItem: nextVisitItem,
      previousVisitItem: previousVisitItem,
      originalCurrentVisitItem: visit,
      currentVisitItem: {
        ...itemInfo,
        id: visitId,
      },
    });
  };

  _loadItemToContext = (item, id) => {
    this.props.videoContext({
      type: "video",
      url: item.url,
      id: id,
      img: item.type === "video" ? item.img : item.url,
      thumbnail: item.type === "video" ? item.img : item.url,
      duration: item.duration,
      trimStart: item.trimStart,
      trimEnd: item.trimEnd,
      volume: item.volume,
      muted: item.muted,
      comment: item.comment,
    });
  };

  _updateDimensions = () => {
    const itemType = this.state.currentVisitItem.type;

    this.visitItem.current &&
      this.setState({
        commentFontSize:
          itemType === "video"
            ? this.visitItem.current.wrapper.clientWidth / 65
            : this.visitItem.current.clientWidth / 65,
      });
  };

  _onProgress = (progress) => {
    if (progress.playedSeconds >= this.state.currentVisitItem.trimEnd) {
      this.setState({ currentTime: 0, isPlaying: false }, () => {
        this.visitItem.current.seekTo(0);
      });
    } else {
      this.setState({ currentTime: progress.playedSeconds });
    }
  };

  _onDuration = (durationSeconds) => {
    this.setState({ totalTime: durationSeconds });
  };

  _onStalled = () => {
    this.setState({ isPlaying: false });
  };

  _pause = () => this.setState({ isPlaying: false });

  _play = () => this.setState({ isPlaying: true });

  _onSeek = (time) => {
    this.setState({ currentTime: time }, () => {
      this.visitItem.current.seekTo(time);
    });
  };

  _onVolumeChange = (value) => this.setState({ volume: value });

  _onTrim = (value) => {
    const { duration } = this.state.currentVisitItem;
    const calcTrimStart = (value.trimStart * duration) / 100;
    const calcTrimEnd = (value.trimEnd * duration) / 100;

    if (this.state.trimStart !== calcTrimStart) {
      this.visitItem.current.seekTo(calcTrimStart);
    } else {
      this.visitItem.current.seekTo(calcTrimEnd);
    }

    this.setState({
      currentVisitItem: {
        ...this.state.currentVisitItem,
        trimStart: calcTrimStart,
        trimEnd: calcTrimEnd,
      },
    });
  };

  _onTrimEnd = (value) => {
    const { id, comment } = this.state.currentVisitItem;

    this.props.videoContext({
      ...this.props.visitItem,
      id: id,
      trimStart: value.trimStart,
      trimEnd: value.trimEnd,
      comment: comment,
    });
  };

  _undoActionHandler = () => {
    const undoItem = this.props.undoActions[this.props.undoActions.length - 1];
    const undoArr = _.filter(
      this.props.undoActions,
      (_, key) => key !== this.props.undoActions.length - 1
    );
    const redoArr = _.cloneDeep(this.props.redoActions);

    const sendRedoItem = {
      trimStart: this.state.currentVisitItem.trimStart,
      trimEnd: this.state.currentVisitItem.trimEnd,
      comment: this.state.currentVisitItem.comment,
    };

    redoArr.length === this.state.actionsLimit - 1 && redoArr.pop();
    redoArr.push({ ...sendRedoItem });

    this.props.redoAction(redoArr);
    this.props.undoAction(undoArr);

    this.setState({
      currentVisitItem: {
        ...this.state.currentVisitItem,
        ...undoItem,
      },
    });

    this.props.videoContext({
      ...this.state.currentVisitItem,
      ...undoItem,
    });
  };

  _redoActionHandler = () => {
    const redoItem = this.props.redoActions[this.props.redoActions.length - 1];
    const redoArr = _.filter(
      this.props.redoActions,
      (_, key) => key !== this.props.redoActions.length - 1
    );
    const undoArr = _.cloneDeep(this.props.undoActions);

    const sendUndoItem = {
      trimStart: this.state.currentVisitItem.trimStart,
      trimEnd: this.state.currentVisitItem.trimEnd,
      comment: this.state.currentVisitItem.comment,
    };

    undoArr.length === this.state.actionsLimit - 1 && undoArr.pop();
    undoArr.push({ sendUndoItem });

    this.props.redoAction(redoArr);
    this.props.undoAction(undoArr);

    this.setState({
      currentVisitItem: {
        ...this.state.currentVisitItem,
        ...redoItem,
      },
    });

    this.props.videoContext({
      ...this.state.currentVisitItem,
      ...redoItem,
    });
  };

  timeFormat = (time) => {
    const hrs = ~~(time / 3600);
    const mins = ~~((time % 3600) / 60);
    const secs = ~~time % 60;
    const ret = `${hrs < 10 ? `0${hrs}` : hrs}:${
      mins < 10 ? `0${mins}` : mins
    }:${secs < 10 ? `0${secs}` : secs}`;

    return ret;
  };
}

function mapStateToProps(state) {
  return {
    visitItem: state.player.ctxData,
    clip: state.clip.getClipSuccess,
    userClips: state.clipsUser.getUserClipsSuccess,
    storagedClip: state.storageClip.clip,
    undoActions: state.undo_redo.undoActions,
    redoActions: state.undo_redo.redoActions,
  };
}

export default compose(
  withRouter,
  requireAuth,
  requireEditorForm,
  connect(mapStateToProps, actions),
  reduxForm({
    form: "editor",
    destroyOnUnmount: false,
    forceUnregisterOnUnmount: true, // <------ unregister fields on unmount
  })
)(VisitItemEditor);
