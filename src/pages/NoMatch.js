import React from "react";
import { Link } from "react-router-dom";
import { Flex, Button, Icon, Heading, Text } from "@chakra-ui/react";
import { FiAlertTriangle } from "react-icons/fi";
import SEO from "components/Seo";

const NoMatch = () => {
  return (
    <>
      <SEO title="404 Página não encontrada" />
      <Flex
        as="article"
        h="100%"
        w="100%"
        align="center"
        justify="center"
        flexDir="column"
      >
        <Icon as={FiAlertTriangle} boxSize={100} />
        <Heading as="h1" py={4}>
          404 - Página não encontrada
        </Heading>
        <Text textAlign="center" pb={8}>
          Aparentemente encontra-se numa página que não existe
        </Text>
        <Link to="/">
          <Button variant="solid">Voltar à página inicial</Button>
        </Link>
      </Flex>
    </>
  );
};

export default NoMatch;
