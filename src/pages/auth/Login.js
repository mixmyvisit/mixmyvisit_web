import React, { Component } from "react";
import { reduxForm, Field } from "redux-form";
import { compose } from "redux";
import { connect } from "react-redux";
import * as actions from "redux/actions";
import { Link, withRouter } from "react-router-dom";

import { required, email } from "components/forms/validator";

import {
  Box,
  Flex,
  Stack,
  Button,
  Image,
  Heading,
  Divider,
  Text,
  Container,
  Icon,
} from "@chakra-ui/react";
import { FaFacebook } from "react-icons/fa";

import SEO from "components/Seo";
import Loading from "components/feedback/Loading";
import EmailInput from "components/forms/EmailInput";
import PasswordInput from "components/forms/PasswordInput";
import PrintToast from "components/feedback/printToast";

class Login extends Component {
  state = {
    isLoading: false,
    disableFormButton: false,
    isFbButtonLoading: false,
  };

  render() {
    const { handleSubmit, signinLoad } = this.props;
    const { disableFormButton, isFbButtonLoading } = this.state;

    return this.state.isLoading ? (
      <Loading />
    ) : (
      <Container py={20}>
        <SEO title="Login" />

        <Link to="/">
          <Button
            h="auto"
            px={6}
            py={2}
            mb={[6, 0]}
            left={[null, "5%"]}
            variant="transparent"
            position={["relative", "absolute"]}
            leftIcon={
              <Image
                mr={3}
                alt="Back"
                src={require("assets/icons/Back.svg")}
                objectFit="contain"
                h={8}
              />
            }
          >
            Voltar
          </Button>
        </Link>

        <Flex pt={12} flexDir="column" justify="center" align="center">
          <Heading as="h1">Login</Heading>
          <Button
            w={["85%", "60%"]}
            colorScheme="facebook"
            isDisabled={signinLoad || isFbButtonLoading}
            isLoading={isFbButtonLoading}
            onClick={() => this._linkWithFb()}
            leftIcon={<Icon boxSize={5} mr={2} as={FaFacebook} />}
          >
            Continuar com Facebook
          </Button>

          <Box
            display="flex"
            justifyContent="center"
            position="relative"
            w={["85%", "100%"]}
          >
            <Heading
              bg="white"
              as="h3"
              zIndex="1"
              position="absolute"
              px={6}
              mb={0}
              pb={0}
              top="31%"
            >
              Ou
            </Heading>
            <Divider borderColor="primary.300" my={12} />
          </Box>

          <Box
            w={["85%", "75%"]}
            as="form"
            display="flex"
            flexDir="column"
            justfiy="center"
            onSubmit={handleSubmit(this._onSigninFormSubmit)}
          >
            <Stack spacing={6} mb={6}>
              <Field
                name="email"
                type="text"
                validate={[required, email]}
                component={EmailInput}
                onChange={this._dismissAlert}
              />
              <Field
                name="password"
                type="password"
                validate={[required]}
                component={PasswordInput}
                onChange={this._dismissAlert}
              />
            </Stack>

            <Button
              mb={10}
              mx="auto"
              type="submit"
              variant="primary"
              w={["100%", "75%"]}
              loadingText="A realizar login..."
              isDisabled={signinLoad || disableFormButton}
              isLoading={signinLoad}
            >
              Login
            </Button>

            <Text pb={4} textAlign="center" fontSize="sm">
              Ainda não possui uma conta? Crie uma neste momento seguindo as
              instruções no{" "}
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://m.me/Mixmyvisit?ref=0x04c45522626080"
              >
                nosso bot no Facebook Messenger
              </a>
            </Text>

            <Text textAlign="center" fontSize="sm">
              Esqueceu-se da sua palavra-passe?{" "}
              <Link to="/password-reset/email">Carregue aqui</Link>
            </Text>
          </Box>
        </Flex>
      </Container>
    );
  }

  componentDidMount() {
    const location = this.props.location;

    if (this.props.userData) {
      this.props.user(() => {
        this.props.history.push("/");
      });
    } else if (location.hash) {
      this.setState({
        isLoading: true,
        disableFormButton: true,
        isFbButtonLoading: true,
      });

      const providerID = location.hash.replace("#", "");
      this._signinWithFb(providerID);
    }
  }

  _signinWithFb = (providerID) => {
    this.props.signInWithFacebook(
      providerID,
      (redirect) => {
        PrintToast(
          "success",
          `Login bem sucedido - por favor aguarde enquanto o redirecionamos`
        );

        this.setState({ isLoading: false });

        if (redirect) {
          this.props.user(() => {
            this.props.history.push(redirect);
          });
        } else {
          this.props.user(() => {
            this.props.history.push("/");
          });
        }
      },
      () => {
        this.setState({ isLoading: false });
        PrintToast(
          "error",
          `Um erro ocorreu durante o processo de login, por favor tente novamente mais tarde. Se o problema persistir contacte-nos via email.`
        );
        this.props.history.push("/login");
      }
    );
  };

  _linkWithFb = () => {
    this.props.linkWithFacebook(
      null,
      (url) => {
        this.setState({ isFbButtonLoading: true, disableFormButton: true });
        window.open(url, "_blank") || window.location.replace(url);
      },
      () => {
        PrintToast(
          "error",
          `Um erro ocorreu durante o processo de login, por favor tente novamente mais tarde. Se o problema persistir contacte-nos via email.`
        );
      }
    );
  };

  _onSigninFormSubmit = (formProps) => {
    this.props.signin(
      formProps,
      () => {
        PrintToast(
          "success",
          `Login bem sucedido - por favor aguarde enquanto o redirecionamos`
        );
        this.props.user(() => {
          this.props.history.push("/");
        });
      },
      (error) => {
        if (error !== "User or password incorrect") {
          PrintToast(
            "error",
            `Um erro ocorreu durante o processo de login, por favor tente novamente mais tarde. Se o problema persistir contacte-nos via email.`
          );
        } else {
          PrintToast(
            "error",
            "O email inserido ou palavra-passe inserida encontram-se incorretos"
          );
        }
      }
    );
  };
}

function mapStateToProps(state) {
  const initialValues = {
    remember: true,
  };

  return {
    signinLoad: state.signin.signin,
    userData: state.user.userSuccess,
    userFail: state.user.userFail,
    initialValues,
  };
}

export default compose(
  withRouter,
  connect(mapStateToProps, actions),
  reduxForm({ form: "signin" })
)(Login);
