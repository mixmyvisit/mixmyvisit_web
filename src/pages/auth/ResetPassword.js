import React, { useEffect, useState } from "react";
import { Link, withRouter } from "react-router-dom";
import { reduxForm, Field } from "redux-form";
import { compose } from "redux";
import { connect } from "react-redux";
import * as actions from "redux/actions";
import { required } from "components/forms/validator";
import SEO from "components/Seo";

import {
  Box,
  Button,
  Container,
  Flex,
  Stack,
  Heading,
  Image,
  Text,
} from "@chakra-ui/react";

import PasswordInput from "components/forms/PasswordInput";
import EmailInput from "components/forms/EmailInput";
import PrintToast from "components/feedback/printToast";

const ResetPassword = (props) => {
  const { handleSubmit } = props;
  const [submitEmail, setSubmitEmail] = useState(null);
  const [token, setToken] = useState(null);

  useEffect(() => {
    const locationParams = props.location.pathname.split("/");
    if (locationParams[2] === "email") {
      setSubmitEmail(true);
    } else {
      setSubmitEmail(false);
      setToken(locationParams[2]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const _updatePassword = (formProps) => {
    if (formProps.password !== formProps["password-again"]) {
      PrintToast(
        "error",
        `Por favor insira a mesma palavra-passe em ambos os campos do formulário.`
      );
    } else {
      props.updatePassword(
        { reset_token: token, password: formProps.password },
        () => {
          PrintToast(
            "success",
            `Palavra-passe atualizada com sucesso, pode agora entrar na sua conta via método login com o seu email e palavra-passe.`
          );
          props.history.push("/login");
        },
        () => {
          PrintToast(
            "error",
            `Ocorreu um erro durante o processo de redifinir a sua palavra-passe, por favor tente novamente mais tarde. Se o problema persistir contacte-nos via email.`
          );
        }
      );
    }
  };

  const _sendResetPasswordEmail = (email) => {
    props.sendResetPasswordEmail(
      email,
      () => {
        PrintToast(
          "success",
          `Email enviado com sucesso, verifique a caixa de entrada do seu email por um mail de mixmv.ua@gmail.com e siga o link nesse mesmo mail.`
        );
      },
      () => {
        PrintToast(
          "error",
          `Ocorreu um erro durante o processo de enviar o email para o reset da sua password, por favor tente novamente mais tarde. Se o problema persistir contacte-nos via email.`
        );
      }
    );
  };

  return (
    <Container px={0} py={20}>
      <SEO title="Redifinir palavra-passe" />

      <Link to="/">
        <Button
          h="auto"
          px={6}
          py={2}
          mb={[6, 0]}
          left={[null, "5%"]}
          variant="transparent"
          position={["relative", "absolute"]}
          leftIcon={
            <Image
              mr={3}
              alt="Back"
              src={require("assets/icons/Back.svg")}
              objectFit="contain"
              h={8}
            />
          }
        >
          Voltar
        </Button>
      </Link>

      <Flex pt={12} flexDir="column" justify="center" align="center">
        <Heading as="h1">Redifinir palavra-passe</Heading>

        {submitEmail ? (
          <Box
            w={["85%", "75%"]}
            as="form"
            display="flex"
            flexDir="column"
            justfiy="center"
            onSubmit={handleSubmit(_sendResetPasswordEmail)}
          >
            <Stack spacing={6} mb={6}>
              <Text>
                Para redifinir a sua palavra-passe de forma segura terá de
                fornecer o email associado à sua conta. Posteriomente irá
                receber um mail com um link específico para submeter a sua nova
                palavra-passe.
              </Text>
              <Field
                name="email"
                type="email"
                validate={[required]}
                component={EmailInput}
              />
            </Stack>

            <Button
              mt={4}
              mb={10}
              mx="auto"
              type="submit"
              variant="primary"
              w={["100%", "75%"]}
              /*             loadingText="A realizar login..."
            isDisabled={signinLoad}
            isLoading={signinLoad} */
            >
              Submeter
            </Button>
          </Box>
        ) : (
          <Box
            w={["85%", "75%"]}
            as="form"
            display="flex"
            flexDir="column"
            justfiy="center"
            onSubmit={handleSubmit(_updatePassword)}
          >
            <Stack spacing={6} mb={6}>
              <Field
                name="password"
                type="password"
                validate={[required]}
                component={PasswordInput}
              />
              <Field
                name="password-again"
                type="password"
                validate={[required]}
                component={PasswordInput}
              />
            </Stack>

            <Button
              mt={4}
              mb={10}
              mx="auto"
              type="submit"
              variant="primary"
              w={["100%", "75%"]}
              /*             loadingText="A realizar login..."
            isDisabled={signinLoad}
            isLoading={signinLoad} */
            >
              Submeter
            </Button>
          </Box>
        )}
      </Flex>
    </Container>
  );
};

function mapStateToProps(state) {
  const initialValues = {
    remember: true,
  };

  return {
    signinLoad: state.signin.signin,
    userData: state.user.userSuccess,
    userFail: state.user.userFail,
    initialValues,
  };
}

export default compose(
  withRouter,
  connect(mapStateToProps, actions),
  reduxForm({ form: "signin" })
)(ResetPassword);
