import React from "react";
import {
  Box,
  Divider,
  Heading,
  Text,
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
} from "@chakra-ui/react";
import SEO from "components/Seo";

const About = () => {
  return (
    <>
      <SEO title="Sobre" />
      <Box as="article">
        <Heading as="h1">Sobre nós</Heading>
        <Text>
          A solução MixMyVisit permite a criação de vídeos automáticos das
          visitas efetuadas a espaços culturais (ex. museus, parques,
          exposições, entre outros) com base nos percursos e locais visitados
          pelos utilizadores. Combinando tecnologia NFC com soluções de criação
          de vídeos automáticos, o sistema permite a cada visitante receber um
          vídeo personalizado e adaptado à sua visita. Pode, ainda, partilhar as
          suas fotos e vídeos que serão automaticamente integrados no vídeo da
          sua visita.
        </Text>
        <Divider my={8} />
        <Heading as="h2">FAQ</Heading>
        <Accordion allowToggle>
          <AccordionItem>
            <AccordionButton _expanded={{ bg: "primary.500", color: "white" }}>
              <Box flex="1" textAlign="left">
                Pergunta 1
              </Box>
              <AccordionIcon />
            </AccordionButton>
            <AccordionPanel pb={4}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat.
            </AccordionPanel>
          </AccordionItem>

          <AccordionItem>
            <AccordionButton _expanded={{ bg: "primary.500", color: "white" }}>
              <Box flex="1" textAlign="left">
                Pergunta 2
              </Box>
              <AccordionIcon />
            </AccordionButton>
            <AccordionPanel pb={4}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat.
            </AccordionPanel>
          </AccordionItem>
        </Accordion>
      </Box>
    </>
  );
};

export default About;
