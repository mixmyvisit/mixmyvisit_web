import { getToken, getRefreshToken } from "../storage/Cookies";
import * as actions from "redux/actions";
import * as requests from "redux/requests";
import _ from "lodash";

/*
The Interceptor is a "action" function that is to be returned inside a dispatch call. As the action is now a function,
redux-thunk middleware will intercept the dispatch and run the Interceptor function provided inside,
dispatching at the end the return of the Interceptor function (which is a new action); The return of the Interceptor function
is a regular action object with the payload.request field set, which will cause this new action dispatched by thunk
to be intercepted by another middleware, the redux-axios-middleware.

Redux-axios-middleware will run an async ajax request as defined inside the parameters inside the action it intercepted.
After running the request internally with axios, it may dispatch a regular action automatically directly to the reducers,
with type PREFIX_SUCCESS and PREFIX_FAIL (in case of success or failure) and the response of the request as parameters of this request.
If onSuccess, onError and onComplete are defined below, they will override the axios-middleware default behaviour
pointed above (wont dispatch the prefix actions by itself). The response is also sent as parameters inside this functions.

The AJAX request will be made to the default client/API (if none specified), or one of the clients specified in the clients list.
This is also set as parameters inside the action within the payload/request field.

It may be convenient to dispatch new actions (generally directly to the reducers as regular action objects) inside the Interceptor,
after a request is returned to effectively set the state of the application (which may also be done using the default behaviour above
with the prefixes).

Generally the Interceptor is dispatched inside an action creator (a function that generates an action) and may sometimes
specify the callbacks that are meant to be run inside the Interceptor, in case of success, error or whatever. These
callbacks are usually linked inside the Interceptor to the callbacks run by the redux-axios-middleware.
This action creators are made available as props inside the React components, and maybe called anywhere.
The final result (like fetched data) is put inside the redux state through the final pure action dispatched to reducers.

It's important to point out that the state of the application is also updated by dispatches to the redux-form (another module),
that it's in no way related to this Interceptor and Axios-Middleware, which are only when async AJAX API calls are
necessary. Redux-form is used when changing the internal redux state used by react components, and has it's onw API and logic.
It's not a middleware, it's just a specific reducer connected to the store and a specific decorators/wrapper to provide react components
access to the form field, values and functions to update the for inside the state.
*/

function Interceptor(
  request = {
    action: null,
    data: null,
    parameters: null,
    identifiers: null,
  },
  successCallback = null,
  errorCallback = null,
  completeCallback = null
) {
  let action = _.cloneDeep(requests[request.action]);
  //add id's to requests
  if (request.identifiers) {
    let newUrl = action.payload.request.url.split(":id");
    action.payload.request.url = action.payload.request.url.split(":id");

    action.payload.request.url.forEach((value, key) => {
      if (action.payload.request.url.length > key + 1) {
        newUrl.splice(key + 1, 0, request.identifiers[key]);
      }
    });

    action.payload.request.url = newUrl.join("");
  }

  //add urlParameters
  if (request.parameters) {
    action.payload.request.url += `?${request.parameters}`;
  }

  if (request.data) {
    action.payload.request.data = request.data;
  }
  //This sets the client that will be used for the request
  if (action.payload.client === "default" || !action.payload.client) {
    action.payload.request.headers = {
      "X-Requested-With": "XMLHttpRequest",
      "Content-Type": "application/json",
      Authorization: `Bearer ${getToken()}`,
    };
  }

  //The onSuccess, onError and onComplete functions below overrides (when set in options) the default behaviour of redux-axios-middleware
  //of dispatching the same functions inside de module, witch dispatches _SUCCESS OR _FAIL actions AFTER THE REGULAR ACTION,
  action.payload.options = {
    onSuccess({ getState, dispatch, response }) {
      //calls A FUNCTION TO PROBABLY DISPATCH _SUCCESS MANUALLY
      //AND UPDATE STATE IN THE PROCESS WITH PAYLOAD FROM SUCCESS RESPONSE
      if (successCallback) {
        successCallback(`${action.type}_SUCCESS`, response);
      }
    },
    onError({ getState, dispatch, error, status }) {
      //In case the response to the request is an http error code
      //console.log(error, error?.response, !error, status);
      //console.log(error.response);

      if (!error.response) {
        //general network error
        errorCallback(error);
      } else if (request.action !== "signin") {
        //check if the action is not signin to avoid conflicts of 401 unauthorized with the following conditions

        if (
          error.response.status === 401 &&
          error.response.statusText === "Unauthorized" &&
          request.action !== "refreshToken" &&
          request.action === "signout"
        ) {
          dispatch(
            actions.refreshSignOut({ refresh_token: getRefreshToken() })
          );
          // if response is unauthorized and is signout then try to reset token
        } else if (
          error.response.status === 401 &&
          error.response.statusText === "Unauthorized" &&
          request.action !== "refreshToken"
        ) {
          // if response is unauthorized then try to reset token
          dispatch(actions.refreshToken({ refresh_token: getRefreshToken() }));
        } else if (errorCallback) {
          errorCallback(
            `${action.type}_FAIL`,
            error.response.data.message || error.response.status || "error"
          );
        }
      } else if (errorCallback) {
        errorCallback(
          `${action.type}_FAIL`,
          error.response.message || error.response.status || "error"
        );
      }
    },
    onComplete({ getState, dispatch, error }) {
      // dispatches loading end and complete callback if false
      dispatch({
        type: action.type,
        payload: false,
      });

      if (completeCallback) {
        completeCallback(action.type);
      }
    },
  };
  return action; //Returns action to be dispatched by redux-thunk and intercepted by redux-axios-middleware
}

export default Interceptor;
