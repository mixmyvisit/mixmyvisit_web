export const signout = {
  type: "SIGNOUT",
  payload: {
    request: {
      method: "post",
      url: "/sign/out",
    },
  },
};

export const signin = {
  type: "SIGNIN",
  payload: {
    request: {
      method: "post",
      url: "/sign/in",
    },
  },
};

export const signinfb = {
  type: "SIGNINFB",
  payload: {
    request: {
      method: "get",
      url: "/sign/in/finish/facebook",
    },
  },
};

export const linkinfb = {
  type: "LINKINFB",
  payload: {
    request: {
      method: "post",
      url: "/sign/in/facebook",
    },
  },
};

export const signup = {
  type: "SIGNUP",
  payload: {
    request: {
      method: "post",
      url: "/sign/up",
    },
  },
};

export const resetPasswordEmail = {
  type: "RESETPASSWORDEMAIL",
  payload: {
    request: {
      method: "post",
      url: "/sign/email",
    },
  },
};

export const updatePassword = {
  type: "UPDATEPASSWORD",
  payload: {
    request: {
      method: "post",
      url: "/sign/reset",
    },
  },
};

export const refreshToken = {
  type: "SIGNIN",
  payload: {
    request: {
      method: "post",
      url: "/sign/refresh",
    },
  },
};

export const refreshSignOut = {
  type: "SIGNOUT",
  payload: {
    request: {
      method: "post",
      url: "/sign/refresh",
    },
  },
};
