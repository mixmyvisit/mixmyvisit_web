export const createClip = {
  type: "CREATE_CLIP",
  payload: {
    request: {
      method: "post",
      url: "/mixmvs",
    },
  },
};

export const updateClip = {
  type: "UPDATE_CLIP",
  payload: {
    request: {
      method: "put",
      url: "/mixmvs/:id",
    },
  },
};

export const getClips = {
  type: "GET_CLIPS",
  payload: {
    request: {
      method: "get",
      url: "/mixmvs",
    },
  },
};

export const getClip = {
  type: "GET_CLIP",
  payload: {
    request: {
      method: "get",
      url: "/mixmvs/:id",
    },
  },
};

export const getVisitWithTagIdUser = {
  type: "GET_VISIT_TAG_ID_USER",
  payload: {
    request: {
      method: "post",
      url: "/mixmvs/tagid/user",
    },
  },
};

export const getVisitWithTagIdNonUser = {
  type: "GET_VISIT_TAG_ID_NON_USER",
  payload: {
    request: {
      method: "post",
      url: "/mixmvs/tagid/non-user",
    },
  },
};

export const getUserClips = {
  type: "GET_USER_CLIPS",
  payload: {
    request: {
      method: "get",
      url: "/users/:id/mixmvs",
    },
  },
};

export const getClipsSearch = {
  type: "GET_CLIPS_SEARCH",
  payload: {
    request: {
      method: "get",
      url: "/mixmvs",
    },
  },
};

export const getVisitNonUserSendToRender = {
  type: "GET_CLIPS_SEARCH",
  payload: {
    request: {
      method: "post",
      url: "/mixmvs",
    },
  },
};

export const getEditorSendToRender = {
  type: "GET_CLIPS_SEARCH",
  payload: {
    request: {
      method: "post",
      url: "/mixmvs",
    },
  },
};
