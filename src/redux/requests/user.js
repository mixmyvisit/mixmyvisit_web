export const user = {
  type: "USER",
  payload: {
    request: {
      method: "get",
      url: "/user",
    },
  },
};
