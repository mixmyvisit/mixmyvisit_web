export const uploadClip = {
  type: "UPLOAD_CLIP",
  payload: {
    request: {
      method: "post",
      url: "/upload",
    },
  },
};

export const uploadContentsRedirectedFromMessenger = {
  type: "UPLOAD_CONTENTS_REDIRECT_FROM_MESSENGER",
  payload: {
    request: {
      method: "post",
      url: "/upload/messenger-contents",
    },
  },
};
