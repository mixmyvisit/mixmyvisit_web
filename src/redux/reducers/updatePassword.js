import {
  UPDATEPASSWORD,
  UPDATEPASSWORD_SUCCESS,
  UPDATEPASSWORD_FAIL,
} from "redux/type";

const INITIAL_STATE = {
  updatePassword: false,
  updatePasswordSuccess: "",
  updatePasswordFail: "",
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case UPDATEPASSWORD:
      return {
        ...state,
        updatePassword:
          typeof action.payload === "boolean" ? action.payload : true,
      };
    case UPDATEPASSWORD_SUCCESS:
      return {
        ...state,
        updatePasswordSuccess: action.payload,
      };
    case UPDATEPASSWORD_FAIL:
      return {
        ...state,
        updatePasswordFail: action.payload,
      };
    default:
      return state;
  }
}
