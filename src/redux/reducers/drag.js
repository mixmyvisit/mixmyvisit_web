import { DRAG_ACTION } from "redux/type";
import _ from "lodash";
const INITIAL_STATE = {
  data: null
};
export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case DRAG_ACTION:
      return {
        ...state,
        data: _.cloneDeep(action.payload)
      };
    default:
      return state;
  }
}
