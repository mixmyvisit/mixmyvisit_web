import { GET_CLIPS_SEARCH, GET_CLIPS_SEARCH_SUCCESS, GET_CLIPS_SEARCH_FAIL } from "redux/type";

const INITIAL_STATE = {
    getClipsSearch: [],
    getClipsSearchFail: "",
    getClipsSearchSuccess: []
};

export default function(state = INITIAL_STATE, action) {
    switch (action.type) {
        case GET_CLIPS_SEARCH:
            return {
                ...state,
                getClipsSearch: typeof action.payload === "boolean" ? action.payload : true
            };
        case GET_CLIPS_SEARCH_SUCCESS:
            return {
                ...state,
                getClipsSearchSuccess: action.payload
            };
        case GET_CLIPS_SEARCH_FAIL:
            return {
                ...state,
                getClipsSearchFail: action.payload
            };
        default:
            return state;
    }
}