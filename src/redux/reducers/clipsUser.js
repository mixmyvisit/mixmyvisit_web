import {
  GET_USER_CLIPS,
  GET_USER_CLIPS_SUCCESS,
  GET_USER_CLIPS_FAIL,
} from "redux/type";

const INITIAL_STATE = {
  getUserClips: [],
  getUserClipsFail: "",
  getUserClipsSuccess: [],
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_USER_CLIPS:
      return {
        ...state,
        getUserClips:
          typeof action.payload === "boolean" ? action.payload : true,
      };
    case GET_USER_CLIPS_SUCCESS:
      return {
        ...state,
        getUserClipsSuccess: action.payload,
      };
    case GET_USER_CLIPS_FAIL:
      return {
        ...state,
        getUserClipsFail: action.payload,
      };
    default:
      return state;
  }
}
