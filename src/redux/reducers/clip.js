import { GET_CLIP, GET_CLIP_SUCCESS, GET_CLIP_FAIL } from "redux/type";

const INITIAL_STATE = {
  getClip: [],
  getClipFail: "",
  getClipSuccess: []
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_CLIP:
      return {
        ...state,
        getClip: typeof action.payload === "boolean" ? action.payload : true
      };
    case GET_CLIP_SUCCESS:
      return {
        ...state,
        getClipSuccess: action.payload
      };
    case GET_CLIP_FAIL:
      return {
        ...state,
        getClipFail: action.payload
      };
    default:
      return state;
  }
}
