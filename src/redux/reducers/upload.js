import { UPLOAD, UPLOAD_SUCCESS, UPLOAD_FAIL } from "redux/type";

const INITIAL_STATE = {
  getUpload: [],
  getUploadFail: "",
  getUploadSuccess: []
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case UPLOAD:
      return {
        ...state,
        getUpload: typeof action.payload === "boolean" ? action.payload : true
      };
    case UPLOAD_SUCCESS:
      return {
        ...state,
        getUploadSuccess: action.payload
      };
    case UPLOAD_FAIL:
      return {
        ...state,
        getUploadFail: action.payload
      };
    default:
      return state;
  }
}