import { CREATE_CLIP, CREATE_CLIP_SUCCESS, CREATE_CLIP_FAIL } from "redux/type";

const INITIAL_STATE = {
  createClip: [],
  createClipFail: "",
  createClipSuccess: []
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case CREATE_CLIP:
      return {
        ...state,
        createClip: typeof action.payload === "boolean" ? action.payload : true
      };
    case CREATE_CLIP_SUCCESS:
      return {
        ...state,
        createClipSuccess: action.payload
      };
    case CREATE_CLIP_FAIL:
      return {
        ...state,
        createClipFail: action.payload
      };
    default:
      return state;
  }
}
