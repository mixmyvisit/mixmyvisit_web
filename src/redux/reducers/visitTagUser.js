import {
  GET_VISIT_TAG_ID_USER,
  GET_VISIT_TAG_ID_USER_SUCCESS,
  GET_VISIT_TAG_ID_USER_FAIL,
} from "redux/type";

const INITIAL_STATE = {
  getVisitTagUser: [],
  getVisitTagUserFail: "",
  getVisitTagUserSuccess: [],
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_VISIT_TAG_ID_USER:
      return {
        ...state,
        getVisitTagUser:
          typeof action.payload === "boolean" ? action.payload : true,
      };
    case GET_VISIT_TAG_ID_USER_SUCCESS:
      return {
        ...state,
        getVisitTagUserSuccess: action.payload,
      };
    case GET_VISIT_TAG_ID_USER_FAIL:
      return {
        ...state,
        getVisitTagUserFail: action.payload,
      };
    default:
      return state;
  }
}
