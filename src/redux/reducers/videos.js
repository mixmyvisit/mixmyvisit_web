import { ASSETS, ASSETS_SUCCESS, ASSETS_FAIL } from "redux/type";

const INITIAL_STATE = {
  videos: [],
  videosFail: "",
  videosSuccess: []
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ASSETS:
      return {
        ...state,
        videos: typeof action.payload === "boolean" ? action.payload : true
      };
    case ASSETS_SUCCESS:
      return {
        ...state,
        videosSuccess: action.payload.data.data
      };
    case ASSETS_FAIL:
      return {
        ...state,
        videosFail: action.payload
      };
    default:
      return state;
  }
}
