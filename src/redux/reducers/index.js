import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";
import signin from "./signin";
import signinfb from "./signinfb";
import signup from "./signup";
import signout from "./signout";
import sendPasswordResetEmail from "./sendPasswordResetEmail";
import updatePassword from "./updatePassword";
import videos from "./videos";
import player from "./player";
import user from "./user";
import drag from "./drag";
import createClip from "./createClip";
import updateClip from "./updateClip";
import storageClip from "./storageClip";
import clips from "./clips";
import clipsUser from "./clipsUser";
import clipsSearch from "./clipsSearch";
import clip from "./clip";
import visitTagUser from "./visitTagUser";
import visitTagNonUser from "./visitTagNonUser";
import upload from "./upload";
import undo_redo from "./undo_redo";

export default combineReducers({
  signin,
  signinfb,
  signup,
  signout,
  sendPasswordResetEmail,
  updatePassword,
  videos,
  player,
  drag,
  user,
  createClip,
  updateClip,
  storageClip,
  clip,
  clips,
  clipsUser,
  clipsSearch,
  visitTagUser,
  visitTagNonUser,
  upload,
  undo_redo,
  form: formReducer,
});
