import { STORAGE_CLIP } from "redux/type";

const INITIAL_STATE = {
  clip: []
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case STORAGE_CLIP:
      return {
        ...state,
        clip: action.payload
      };
    default:
      return state;
  }
}
