import { GET_CLIPS, GET_CLIPS_SUCCESS, GET_CLIPS_FAIL } from "redux/type";

const INITIAL_STATE = {
  getClips: [],
  getClipsFail: "",
  getClipsSuccess: []
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_CLIPS:
      return {
        ...state,
        getClips: typeof action.payload === "boolean" ? action.payload : true
      };
    case GET_CLIPS_SUCCESS:
      return {
        ...state,
        getClipsSuccess: action.payload
      };
    case GET_CLIPS_FAIL:
      return {
        ...state,
        getClipsFail: action.payload
      };
    default:
      return state;
  }
}
