import {
  GET_VISIT_TAG_ID_NON_USER,
  GET_VISIT_TAG_ID_NON_USER_SUCCESS,
  GET_VISIT_TAG_ID_NON_USER_FAIL,
} from "redux/type";

const INITIAL_STATE = {
  getVisitTagNonUser: [],
  getVisitTagNonUserFail: "",
  getVisitTagNonUserSuccess: [],
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case GET_VISIT_TAG_ID_NON_USER:
      return {
        ...state,
        getVisitTagNonUser:
          typeof action.payload === "boolean" ? action.payload : true,
      };
    case GET_VISIT_TAG_ID_NON_USER_SUCCESS:
      return {
        ...state,
        getVisitTagNonUserSuccess: action.payload,
      };
    case GET_VISIT_TAG_ID_NON_USER_FAIL:
      return {
        ...state,
        getVisitTagNonUserFail: action.payload,
      };
    default:
      return state;
  }
}
