import { VIDEO_CONTEXT, FULL_VIDEO_CONTEXT, CURRENT_TIME } from "redux/type";
import _ from "lodash";
const INITIAL_STATE = {
  ctxData: [],
  fullCtxData: [],
  currentTime: 0
};
export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case VIDEO_CONTEXT:
      return {
        ...state,
        ctxData: action.payload
      };
    case FULL_VIDEO_CONTEXT:
      return {
        ...state,
        fullCtxData: _.cloneDeep(action.payload)
      };
    case CURRENT_TIME:
      return {
        ...state,
        currentTime: _.cloneDeep(action.payload)
      };
    default:
      return state;
  }
}
