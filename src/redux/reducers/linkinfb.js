import { LINKINFB, LINKINFB_SUCCESS, LINKINFB_FAIL } from "redux/type";

const INITIAL_STATE = {
  linkin: false,
  linkinSuccess: "",
  linkinFail: "",
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case LINKINFB:
      return {
        ...state,
        linkin: typeof action.payload === "boolean" ? action.payload : true,
      };
    case LINKINFB_SUCCESS:
      return {
        ...state,
        linkinSuccess: action.payload,
      };
    case LINKINFB_FAIL:
      return {
        ...state,
        linkinFail: action.payload,
      };
    default:
      return state;
  }
}
