import { SIGNOUT, SIGNOUT_SUCCESS, SIGNOUT_FAIL } from "redux/type";

const INITIAL_STATE = {
	signout: false,
	signoutSuccess: "",
	signoutFail: ""
};

export default function(state = INITIAL_STATE, action) {
	switch (action.type) {
		case SIGNOUT:
			return {
				...state,
				signout:
					typeof action.payload === "boolean" ? action.payload : true
			};
		case SIGNOUT_SUCCESS:
			return {
				...state,
				signoutSuccess: action.payload
			};
		case SIGNOUT_FAIL:
			return {
				...state,
				signoutFail: action.payload
			};
		default:
			return state;
	}
}
