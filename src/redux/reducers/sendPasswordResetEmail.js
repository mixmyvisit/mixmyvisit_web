import {
  RESETPASSWORDEMAIL,
  RESETPASSWORDEMAIL_SUCCESS,
  RESETPASSWORDEMAIL_FAIL,
} from "redux/type";

const INITIAL_STATE = {
  resetPasswordEmail: false,
  resetPasswordEmailSuccess: "",
  resetPasswordEmailFail: "",
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case RESETPASSWORDEMAIL:
      return {
        ...state,
        resetPasswordEmail:
          typeof action.payload === "boolean" ? action.payload : true,
      };
    case RESETPASSWORDEMAIL_SUCCESS:
      return {
        ...state,
        resetPasswordEmailSuccess: action.payload,
      };
    case RESETPASSWORDEMAIL_FAIL:
      return {
        ...state,
        resetPasswordEmailFail: action.payload,
      };
    default:
      return state;
  }
}
