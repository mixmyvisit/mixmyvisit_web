import { UPDATE_CLIP, UPDATE_CLIP_SUCCESS, UPDATE_CLIP_FAIL } from "redux/type";

const INITIAL_STATE = {
  updateClip: [],
  updateClipFail: "",
  updateClipSuccess: []
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case UPDATE_CLIP:
      return {
        ...state,
        updateClip: typeof action.payload === "boolean" ? action.payload : true
      };
    case UPDATE_CLIP_SUCCESS:
      return {
        ...state,
        updateClipSuccess: action.payload
      };
    case UPDATE_CLIP_FAIL:
      return {
        ...state,
        updateClipFail: action.payload
      };
    default:
      return state;
  }
}
