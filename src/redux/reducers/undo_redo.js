import { UNDO_ACTION, REDO_ACTION } from "redux/type";
import _ from "lodash";

const INITIAL_STATE = {
  undoActions: [],
  redoActions: []
};

export default function(state = INITIAL_STATE, action) {
  switch (action.type) {
    case UNDO_ACTION:
      return {
        ...state,
        undoActions: _.cloneDeep(action.payload)
      };
    case REDO_ACTION:
      return {
        ...state,
        redoActions: _.cloneDeep(action.payload)
      };
    default:
      return state;
  }
}