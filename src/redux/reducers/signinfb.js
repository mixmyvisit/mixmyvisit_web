import { SIGNINFB, SIGNINFB_SUCCESS, SIGNINFB_FAIL } from "redux/type";

const INITIAL_STATE = {
  signin: false,
  signinSuccess: "",
  signinFail: "",
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case SIGNINFB:
      return {
        ...state,
        signin: typeof action.payload === "boolean" ? action.payload : true,
      };
    case SIGNINFB_SUCCESS:
      return {
        ...state,
        signinSuccess: action.payload,
      };
    case SIGNINFB_FAIL:
      return {
        ...state,
        signinFail: action.payload,
      };
    default:
      return state;
  }
}
