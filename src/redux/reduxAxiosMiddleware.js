import axios from "axios";
import { multiClientMiddleware } from "redux-axios-middleware";

// https://mixmyvisit.web.ua.pt/api
// http://localhost:8000/api

const clients = {
  default: {
    client: axios.create({
      baseURL: "https://mixmyvisit.web.ua.pt/api",
      responseType: "json",
    }),
  },
};

export default multiClientMiddleware(clients);
