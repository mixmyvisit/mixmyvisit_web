import { UNDO_ACTION, REDO_ACTION } from "redux/type";

export const undoAction = action => {
  return {
    type: UNDO_ACTION,
    payload: action
  };
};

export const redoAction = action => {
  return {
    type: REDO_ACTION,
    payload: action
  };
};