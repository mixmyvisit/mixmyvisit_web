import { DRAG_ACTION } from "redux/type";

export const pushClipToPosition = (clip, hoverId, action) => {
  return {
    type: DRAG_ACTION,
    payload: {
      func: "pushClipToPosition",
      parameters: [clip, hoverId, action]
    }
  };
};
