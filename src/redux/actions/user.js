import { setToken, removeToken } from "../../storage/Cookies";
import { setUser, removeUser } from "../../storage/Storage";
import Interceptor from "redux/Interceptor";

export const signout = (callback) => (dispatch) => {
  return dispatch(
    Interceptor(
      { action: "signout" },
      (type, response) => {
        removeToken();
        removeUser();
        //dispatches SIGNOUT_SUCCESS
        dispatch({
          type,
          payload: response,
        });

        callback(); //calls redirect
        window.location.reload();
      },
      (type, error) => {}
    )
  );
};

export const signInWithFacebook =
  (providerID, callbackSuccess, callbackError) => (dispatch) => {
    return dispatch(
      Interceptor(
        { action: "signinfb", parameters: `providerID=${providerID}` },
        (type, response) => {
          dispatch({ type, payload: response });
          setToken(
            response.data.access_token,
            response.data.expires_in,
            response.data.refresh_token,
            864000
          );

          if (response.data.redirect) {
            callbackSuccess(response.data.redirect);
          } else {
            callbackSuccess();
          }
        },
        (type, error) => {
          dispatch({
            type,
            payload: error,
          });
          callbackError(error);
        }
      )
    );
  };

export const linkWithFacebook =
  (data, callbackSuccess, callbackError) => (dispatch) => {
    return dispatch(
      Interceptor(
        { action: "linkinfb", data },
        (type, response) => {
          callbackSuccess(response.data.url);
        },
        (type, error) => {
          dispatch({
            type,
            payload: error,
          });
          callbackError(error);
        }
      )
    );
  };

export const signin = (data, callbackSuccess, callbackError) => (dispatch) => {
  return dispatch(
    Interceptor(
      { action: "signin", data },
      (type, response) => {
        dispatch({ type, payload: response });
        setToken(
          response.data.access_token,
          response.data.expires_in,
          response.data.refresh_token,
          864000
        );
        callbackSuccess();
      },
      (type, error) => {
        dispatch({
          type,
          payload: error,
        });
        callbackError(error);
      }
    )
  );
};

export const sendResetPasswordEmail =
  (data, callbackSuccess, callbackError) => (dispatch) => {
    return dispatch(
      Interceptor(
        { action: "resetPasswordEmail", data },
        (type, response) => {
          dispatch({
            type,
            payload: response.data.data,
          });
          callbackSuccess();
        },
        (type, error) => {
          dispatch({
            type,
            payload: error,
          });
          callbackError();
        }
      )
    );
  };

export const updatePassword =
  (data, callbackSuccess, callbackError) => (dispatch) => {
    return dispatch(
      Interceptor(
        { action: "updatePassword", data },
        (type, response) => {
          dispatch({
            type,
            payload: response.data.data,
          });
          callbackSuccess();
        },
        (type, error) => {
          dispatch({
            type,
            payload: error,
          });
          callbackError();
        }
      )
    );
  };

export const refreshToken =
  (data, callbackSuccess, callbackError) => (dispatch) => {
    return dispatch(
      Interceptor(
        { action: "refreshToken", data },
        (type, response) => {
          dispatch({
            type,
            payload: response,
          });
          setToken(
            response.data.access_token,
            response.data.expires_in,
            response.data.refresh_token,
            864000
          );
          window.location.reload();
        },
        (type, error) => {
          removeToken();
          removeUser();
          window.location.reload();
        }
      )
    );
  };

export const refreshSignOut = (data) => (dispatch) => {
  return dispatch(
    Interceptor(
      { action: "refreshSignOut", data },
      (type, response) => {
        removeToken();
        removeUser();
        dispatch({
          type,
          payload: response,
        });
        window.location.reload();
      },
      (type, error) => {}
    )
  );
};

export const user = (callbackSuccess) => (dispatch) => {
  return dispatch(
    Interceptor(
      { action: "user" },
      (type, response) => {
        dispatch({
          type,
          payload: response.data.data,
        });
        setUser(response.data.data);
        callbackSuccess();
      },
      (type, error) => {
        dispatch({
          type,
          payload: error,
        });
      }
    )
  );
};
