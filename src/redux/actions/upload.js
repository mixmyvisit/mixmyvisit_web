import Interceptor from "redux/Interceptor";

export const uploadClip =
  (data, callback, errorCallback) => async (dispatch) => {
    return dispatch(
      Interceptor(
        { action: "uploadClip", data },
        (type, response) => {
          if (callback) {
            callback(response.data);
          }
          dispatch({
            type,
            payload: response,
          });
        },
        (_, error) => {
          errorCallback(error.message);
        }
      )
    );
  };

export const uploadContentsRedirectedFromMessenger =
  (data, callback, errorCallback) => async (dispatch) => {
    return dispatch(
      Interceptor(
        { action: "uploadContentsRedirectedFromMessenger", data },
        (type, response) => {
          if (callback) {
            callback(response.data);
          }
          dispatch({
            type,
            payload: response,
          });
        },
        (_, error) => {
          errorCallback(error);
        }
      )
    );
  };
