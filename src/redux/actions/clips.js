import Interceptor from "redux/Interceptor";
import { STORAGE_CLIP } from "redux/type";
import { setClip } from "storage/Storage";

export const saveCliptoStorage = (data) => {
  setClip(data);
  return {
    type: STORAGE_CLIP,
    payload: data,
  };
};

export const updateClip =
  (data, id, callback, callbackError) => async (dispatch) => {
    return dispatch(
      Interceptor(
        { action: "updateClip", data: data, identifiers: [id] },
        (type, response) => {
          if (callback) {
            callback(response);
          }
          dispatch({
            type,
            payload: response,
          });
        },
        (type, error) => {
          if (callbackError) callbackError(error);
          dispatch({
            type,
            payload: error.message,
          });
        }
      )
    );
  };

export const getClips =
  (query, callback, page = 1) =>
  async (dispatch) => {
    return dispatch(
      Interceptor(
        {
          action: "getClips",
          parameters:
            `state=published&sort=-created_at` +
            (query ? `&search=${query}` : ""),
        },
        (type, response) => {
          dispatch({
            type,
            payload: response.data.data,
          });
          if (callback) callback();
        },
        (type, error) => {}
      )
    );
  };

export const getClip =
  (id, callbackSuccess, callbackError) => async (dispatch) => {
    return dispatch(
      Interceptor(
        {
          action: "getClip",
          identifiers: [id],
        },
        async (type, response) => {
          await dispatch({
            type,
            payload: response.data.data,
          });
          callbackSuccess && callbackSuccess();
        },
        (type, error) => {
          console.log(error);
          callbackError && callbackError(error);
        }
      )
    );
  };

export const getVisitWithTagIdUser =
  (data, callbackSuccess, callbackError) => async (dispatch) => {
    return dispatch(
      Interceptor(
        {
          action: "getVisitWithTagIdUser",
          data,
        },
        (type, response) => {
          dispatch({
            type,
            payload: response.data.data,
          });
          callbackSuccess && callbackSuccess(response.data.data);
        },
        (type, error) => {
          callbackError && callbackError(error);
        }
      )
    );
  };

export const getVisitWithTagIdNonUser =
  (data, callbackSuccess, callbackError) => async (dispatch) => {
    return dispatch(
      Interceptor(
        {
          action: "getVisitWithTagIdNonUser",
          data,
        },
        (type, response) => {
          dispatch({
            type,
            payload: response.data.data,
          });
          callbackSuccess && callbackSuccess(response.data.data);
        },
        (type, error) => {
          callbackError && callbackError(error);
        }
      )
    );
  };

export const editorSendToRender =
  (tagId, callbackSuccess, callbackError) => async (dispatch) => {
    return dispatch(
      Interceptor(
        {
          action: "getEditorSendToRender",
          identifiers: [tagId],
        },
        (type, response) => {
          dispatch({
            type,
            payload: response.data.data,
          });
          callbackSuccess && callbackSuccess();
        },
        (type, error) => {
          callbackError && callbackError(error);
        }
      )
    );
  };

export const visitNonUserSendToRender =
  (tagId, callbackSuccess, callbackError) => async (dispatch) => {
    return dispatch(
      Interceptor(
        {
          action: "getVisitNonUserSendToRender",
          identifiers: [tagId],
        },
        (type, response) => {
          dispatch({
            type,
            payload: response.data.data,
          });
          callbackSuccess && callbackSuccess();
        },
        (type, error) => {
          callbackError && callbackError(error);
        }
      )
    );
  };

export const getUserClips =
  (id, callback, callbackError) => async (dispatch) => {
    return dispatch(
      Interceptor(
        {
          action: "getUserClips",
          identifiers: [id],
          parameters: `sort=-created_at`,
        },
        (type, response) => {
          dispatch({
            type,
            payload: response.data.data,
          });
          if (callback) callback();
        },
        (type, error) => {
          dispatch({
            type,
            payload: error,
          });
          if (callbackError) callbackError(error);
        }
      )
    );
  };
