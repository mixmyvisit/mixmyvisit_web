import { VIDEO_CONTEXT, FULL_VIDEO_CONTEXT, CURRENT_TIME } from "redux/type";
export const videoContext = (ctx) => {
  return {
    type: VIDEO_CONTEXT,
    payload: ctx,
  };
};

export const fullVideoContext = (ctx) => {
  return {
    type: FULL_VIDEO_CONTEXT,
    payload: ctx,
  };
};

export const currentTime = (time) => {
  return {
    type: CURRENT_TIME,
    payload: time,
  };
};
