import { change, submit as _submit } from "redux-form";

export const updateForm = (form, field, value) => dispatch => {
  dispatch(change(form, field, value));
};

export const submit = form => async dispatch => {
  dispatch(_submit(form));
};
