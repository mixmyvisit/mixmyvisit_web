import ReactDOM from "react-dom";

export { DragSource, DropTarget, DragLayer } from "react-dnd";

export const selfTarget = {
  hover(props, monitor, component) {
    const dragIndex = monitor.getItem().id;
    const hoverIndex = props.id;

    // Don't replace items with themselves
    if (dragIndex === hoverIndex) {
      return;
    }

    // Determine rectangle on screen
    const hoverBoundingRect = ReactDOM.findDOMNode(
      component
    ).getBoundingClientRect();

    // Get horizontal middle
    const hoverMiddleX = (hoverBoundingRect.left - hoverBoundingRect.right) / 2;

    // Determine mouse position
    const clientOffset = monitor.getClientOffset();

    // Get pixels to the right
    const hoverClientX = clientOffset.x - hoverBoundingRect.right;

    // Only perform the move when the mouse has crossed half of the items width
    // When dragging left, only move when the cursor is below 50%
    // When dragging right, only move when the cursor is above 50%

    // Dragging left
    if (hoverClientX < hoverMiddleX - hoverBoundingRect.width / 3) {
      return;
    }

    // Dragging right
    if (hoverClientX > hoverMiddleX + hoverBoundingRect.width / 3) {
      return;
    }

    if (monitor.getItemType() === "CLIP_USED") {
      // Time to actually perform the action
      props._moveClip(dragIndex, hoverIndex);
    } else {
      props._pushClipToPosition(dragIndex, hoverIndex, "hover");
    }
  },

  drop(props, monitor) {
    return {
      container: TYPES.CLIP,
      id: props.id
    };
  }
};

export const Drop = (connect, monitor) => {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop(),
    didDrop: monitor.didDrop()
  };
};

export const Drag = (connect, monitor) => {
  return {
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    monitorItem: monitor.getItem(),
    isDragging: monitor.isDragging(),
    mousePosition: monitor.getInitialClientOffset()
  };
};

export const Layer = monitor => {
  return {
    item: monitor.getItem(),
    itemType: monitor.getItemType(),
    isDragging: monitor.isDragging(),
    currentOffset: monitor.getSourceClientOffset()
  };
};

export const TYPES = {
  CLIP: "CLIP",
  CLIP_USED: "CLIP_USED",
  VIDEOBITE: "VIDEOBITE",
  TIMELINE: "TIMELINE",
  AUDIO: "AUDIO",
  PLAYER: "PLAYER",
  TITLE: "TITLE",
  TITLE_USED: "TITLE_USED",
  STICKER: "STICKER",
  STICKER_USED: "STICKER_USED",
  DELETE_AREA: "DELETE_AREA"
};
