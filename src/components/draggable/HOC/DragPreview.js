import React, { Component } from "react";
import ReactDOM from "react-dom";
import CustomDragLayer from "./../CustomDragLayer";

export default attachComponent => ChildComponent => {
	return class ComposedComponent extends Component {
		state = { x: 0, y: 0 };
		LayerComponent = null;
		child = React.createRef();
		draggableLayerPreview = React.createRef();

		componentWillMount() {
			let { connectDragPreview } = this.props;

			//loads a transparent drag backgorund image before renderening the layer to use
			let canvas = document.createElement("canvas");
			var ctx = canvas.getContext("2d");
			let img = new Image();
			this.img = img;
			img.src = canvas.toDataURL();
			img.onload = () => {
				connectDragPreview(img);
			};
			this.LayerComponent = attachComponent(this.props);
		}

		render() {
			const { isDragging, mousePosition } = this.props;
			const { x, y } = this.state;
            this.LayerComponent = attachComponent(this.props);
			return (
				<span>
					<div
						style={{
							position: "fixed",
							pointerEvents: "none",
							zIndex: 100,
							left: x,
							top: y,
						}}
					>
						<CustomDragLayer ref={this.draggableLayerPreview}>
							{isDragging && this.LayerComponent}
						</CustomDragLayer>
					</div>

					<ChildComponent ref={this.child} {...this.props} />
				</span>
			);
		}

		componentDidUpdate(prevProps) {
			const { isDragging, mousePosition } = this.props;



			if (isDragging != prevProps.isDragging) {
				if (isDragging) {
					let x =
						mousePosition != undefined
							? mousePosition.x -
							  this._mousePosition().domX -
							  this._draggableLayerSize().width / 2
							: 0;
					let y =
						mousePosition != undefined
							? mousePosition.y -
							  this._mousePosition().domY -
							  this._draggableLayerSize().height / 2
							: 0;

					this.setState({ x, y });
				}
			}
		}

		_mousePosition = () => {
			const { mousePosition } = this.props;
			let domPos = ReactDOM.findDOMNode(
				this.child.current
			).getBoundingClientRect();

			return { domX: domPos.x, domY: domPos.y };
		};

		_draggableLayerSize = () => {
			return ReactDOM.findDOMNode(
				this.draggableLayerPreview.current
			).getBoundingClientRect();
		};
	};
};
