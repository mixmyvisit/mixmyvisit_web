import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import * as actions from "redux/actions";
import { NavLink, Link, withRouter } from "react-router-dom";
import { useWindowSize } from "react-use";

import NavMenu from "./NavMenu";
import NavDrawer from "./NavDrawer";
import { Box, Flex, Button, IconButton, Image, Icon } from "@chakra-ui/react";
import { FaHome, FaUsers, FaInfo, FaFileVideo } from "react-icons/fa";

const Header = (props) => {
  const { userData, location } = props;
  const { width } = useWindowSize();

  const showLogo =
    userData ||
    location.pathname === "/login" ||
    location.pathname.includes("/password-reset/")
      ? true
      : false;
  const isOnFrontpageAndNotLoggedIn = location.pathname === "/" && !userData;

  return (
    <Flex
      as="header"
      h="7rem"
      top="0"
      right="0"
      left="0"
      zIndex="999"
      align="center"
      px={[4, 12, 20]}
      position={isOnFrontpageAndNotLoggedIn ? "absolute" : "static"}
      bg={isOnFrontpageAndNotLoggedIn ? "transparent" : "grey.400"}
    >
      <Flex
        as="nav"
        w="100%"
        justify={isOnFrontpageAndNotLoggedIn ? "flex-end" : "space-between"}
      >
        <Flex flexDir="row" align="center">
          {width < 1024 && userData && <NavDrawer user={userData} />}
          {showLogo && (
            <Box h="100%" d="flex" align="center" as={Link} to="/">
              <IconButton
                h="auto"
                variant="transparent"
                sx={{ padding: 0 }}
                icon={
                  <Image
                    w={[32]}
                    objectFit="contain"
                    alt="MixMyVisit Logo"
                    src="/logo/svg/mmv-logo-with-letters.svg"
                  />
                }
              />
            </Box>
          )}
        </Flex>
        {location.pathname !== "/login" && (
          <Flex flexDir="row" align="center" justify="center">
            {!userData ? (
              <NavLink to="/login">
                <Button
                  py={8}
                  px={10}
                  borderRadius="25px"
                  variant="solid-white"
                >
                  Login
                </Button>
              </NavLink>
            ) : (
              <>
                {width >= 1024 && userData && (
                  <>
                    <Link to="/">
                      <Button
                        mr={2}
                        h="auto"
                        leftIcon={
                          <Icon
                            color="primary.500"
                            display="flex"
                            alignItems="center"
                            as={FaHome}
                          />
                        }
                        variant="transparent"
                      >
                        Home
                      </Button>
                    </Link>

                    <Link to="/colecao">
                      <Button
                        mr={2}
                        h="auto"
                        variant="transparent"
                        leftIcon={
                          <Icon
                            as={FaFileVideo}
                            color="primary.500"
                            display="flex"
                            alignItems="center"
                          />
                        }
                      >
                        Coleção
                      </Button>
                    </Link>
                    <Link to="/sobre">
                      <Button
                        mr={2}
                        h="auto"
                        leftIcon={
                          <Icon
                            color="primary.500"
                            display="flex"
                            alignItems="center"
                            as={FaInfo}
                          />
                        }
                        variant="transparent"
                      >
                        Sobre
                      </Button>
                    </Link>
                    <Link to="/equipa">
                      <Button
                        mr={2}
                        h="auto"
                        leftIcon={
                          <Icon
                            color="primary.500"
                            display="flex"
                            alignItems="center"
                            as={FaUsers}
                          />
                        }
                        variant="transparent"
                      >
                        Equipa
                      </Button>
                    </Link>
                  </>
                )}
                <NavMenu />
              </>
            )}
          </Flex>
        )}
      </Flex>
    </Flex>
  );
};

function mapStateToProps(state) {
  return {
    userData: state.user.userSuccess,
  };
}

export default compose(connect(mapStateToProps, actions), withRouter)(Header);
