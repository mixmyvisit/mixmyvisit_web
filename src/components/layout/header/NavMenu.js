import React, { useState, useContext } from "react";
import * as actions from "redux/actions";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  Menu,
  MenuButton,
  MenuList,
  Button,
  Icon,
  Avatar,
  Box,
  Flex,
  Modal,
  ModalContent,
  ModalOverlay,
  Heading,
  Collapse,
} from "@chakra-ui/react";
import { FaCog, FaFacebookMessenger, FaSignOutAlt } from "react-icons/fa";
import { FiChevronDown, FiChevronUp } from "react-icons/fi";
import { GoTextSize } from "react-icons/go";
import { ThemeContext } from "theme/ThemeContext";
import _ from "lodash";

import Loader from "components/feedback/Loader";
import PrintToast from "components/feedback/printToast";

const NavMenu = (props) => {
  const [isOpen, setOpen] = useState(false);
  const [showSettings, setShowSettings] = useState(false);
  const handleSettingsToggle = () => setShowSettings(!showSettings);
  const { fontSizes, themeConfig, changeThemeFontSize } =
    useContext(ThemeContext);

  const { fontSize } = themeConfig;
  const fontsSizesArray = Object.values(fontSizes);
  const activeFontSizeIndex = fontsSizesArray.findIndex(
    (item) => item === fontSize
  );

  const _signOut = () => {
    setOpen(true);
    props.signout(
      () => {
        setOpen(false);
        props.history.push("/");
      },
      (error) => {
        setOpen(false);
        PrintToast(
          "error",
          `An error occurred while loggin out - ${error.message}. Try again later`
        );
      }
    );
  };

  return (
    <>
      <Menu>
        <Button
          px={4}
          py={2}
          h="auto"
          as={MenuButton}
          borderRadius="md"
          variant="transparent"
          transition="all 0.5s ease-in-out"
        >
          <Avatar boxShadow="default" alt="User" />
        </Button>
        <MenuList bg="white" p="1.5rem">
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="http://m.me/Mixmyvisit"
          >
            <Button
              w="100%"
              variant="transparent"
              display="flex"
              justifyContent="flex-start"
              leftIcon={<Icon mr={2} as={FaFacebookMessenger} />}
            >
              Messenger Bot
            </Button>
          </a>
          <Button
            onClick={handleSettingsToggle}
            leftIcon={<Icon mr={2} as={FaCog} />}
            justifyContent="flex-start"
            variant="transparent"
          >
            <span>Definições</span>
            <Icon ml={3} as={!showSettings ? FiChevronDown : FiChevronUp} />
          </Button>

          <Collapse in={showSettings} style={{ overflow: "visible" }}>
            <Flex mt={2} ml={1} flexDir="column">
              <Button
                w="auto"
                leftIcon={<Icon mr={2} as={GoTextSize} />}
                justifyContent="flex-start"
                variant="transparent"
                fontSize="sm"
                onClick={() =>
                  changeThemeFontSize(
                    activeFontSizeIndex === fontsSizesArray.length - 1
                      ? fontsSizesArray[0]
                      : fontsSizesArray[_.add(activeFontSizeIndex, 1)]
                  )
                }
              >
                Fonte -{" "}
                {fontSize === "md"
                  ? "Médio"
                  : fontSize === "lg"
                  ? "Grande"
                  : "Pequeno"}
              </Button>
            </Flex>
          </Collapse>
          <Button
            w="100%"
            variant="transparent"
            display="flex"
            align="center"
            justifyContent="flex-start"
            onClick={_signOut}
            leftIcon={<Icon as={FaSignOutAlt} mr={2} />}
          >
            Logout
          </Button>
        </MenuList>
      </Menu>

      <Modal
        motionPreset="scale"
        onClose={() => setOpen(false)}
        closeOnEsc={false}
        closeOnOverlayClick={false}
        isOpen={isOpen}
        isCentered
      >
        <ModalOverlay>
          <ModalContent>
            <Flex
              p={8}
              textAlign="center"
              flexDir="column"
              justify="center"
              items="center"
            >
              <Box mb={4}>
                <Loader />
              </Box>
              <Heading pb={2} m={0} as="h3">
                Por favor aguarde enquanto realizamos o logout
              </Heading>
              <p>Não recarregue a página</p>
            </Flex>
          </ModalContent>
        </ModalOverlay>
      </Modal>
    </>
  );
};

export default compose(connect(null, actions), withRouter)(NavMenu);
