import React, { useRef, useState, useContext } from "react";
import { Link } from "react-router-dom";
import {
  Drawer,
  DrawerBody,
  DrawerOverlay,
  DrawerContent,
  useDisclosure,
  IconButton,
  Collapse,
  Button,
  Box,
  Icon,
  DrawerCloseButton,
  useColorMode,
} from "@chakra-ui/react";
import {
  FaBars,
  FaHome,
  FaUsers,
  FaInfo,
  FaCog,
  FaSun,
  FaMoon,
  FaFileVideo,
} from "react-icons/fa";
import { FiChevronDown, FiChevronUp } from "react-icons/fi";
import { GoTextSize } from "react-icons/go";
import { useWindowSize } from "react-use";
import { ThemeContext } from "theme/ThemeContext";
import _ from "lodash";

const NavDrawer = (props) => {
  const { width } = useWindowSize();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [showSettings, setShowSettings] = useState(false);
  const handleSettingsToggle = () => setShowSettings(!showSettings);
  const btnRef = useRef();

  const { colorMode, toggleColorMode } = useColorMode();
  const { fontSizes, themeConfig, changeThemeFontSize } = useContext(
    ThemeContext
  );

  const { fontSize } = themeConfig;
  const fontsSizesArray = Object.values(fontSizes);
  const activeFontSizeIndex = fontsSizesArray.findIndex(
    (item) => item === fontSize
  );

  return (
    <>
      <IconButton
        ref={btnRef}
        py={2}
        px={2}
        mr={4}
        onClick={onOpen}
        alignItems="center"
        variant="transparent"
        icon={<Icon color="primary.500" boxSize={30} as={FaBars} />}
      />
      <Drawer
        size="xs"
        bg="#FFF"
        isOpen={isOpen}
        placement="left"
        onClose={onClose}
        finalFocusRef={btnRef}
        closeOnOverlayClick={true}
      >
        <DrawerOverlay>
          <DrawerContent>
            {width < 350 && <DrawerCloseButton />}
            <DrawerBody
              pb={6}
              pt={[10, 6]}
              display="flex"
              flexDir="column"
              alignItems="center"
              justifyContent="start"
            >
              <Box as={Link} w="100%" to="/">
                <Button
                  py={0}
                  w="100%"
                  leftIcon={<Icon mr={2} as={FaHome} />}
                  justifyContent="flex-start"
                  variant="transparent"
                >
                  <span>Home</span>
                </Button>
              </Box>
              <Box as={Link} w="100%" to="/colecao">
                <Button
                  py={0}
                  w="100%"
                  justifyContent="flex-start"
                  variant="transparent"
                  leftIcon={<Icon mr={2} as={FaFileVideo} />}
                >
                  <span>Coleção</span>
                </Button>
              </Box>
              <Box as={Link} w="100%" to="/sobre">
                <Button
                  py={0}
                  leftIcon={<Icon mr={2} as={FaInfo} />}
                  justifyContent="flex-start"
                  variant="transparent"
                  w="100%"
                >
                  <span>Sobre</span>
                </Button>
              </Box>
              <Box as={Link} w="100%" to="/equipa">
                <Button
                  py={0}
                  leftIcon={<Icon mr={2} as={FaUsers} />}
                  justifyContent="flex-start"
                  variant="transparent"
                  w="100%"
                >
                  <span>Equipa</span>
                </Button>
              </Box>
              <Box w="100%">
                <Button
                  py={0}
                  onClick={handleSettingsToggle}
                  leftIcon={<Icon mr={2} as={FaCog} />}
                  justifyContent="flex-start"
                  variant="transparent"
                  w="100%"
                >
                  <span>Definições</span>
                  <Icon
                    ml="auto"
                    as={!showSettings ? FiChevronDown : FiChevronUp}
                  />
                </Button>

                <Collapse in={showSettings} style={{ overflow: "visible" }}>
                  <Box mt={2} ml={2}>
                    <Button
                      leftIcon={
                        <Icon
                          mr={2}
                          as={colorMode === "light" ? FaSun : FaMoon}
                        />
                      }
                      justifyContent="flex-start"
                      variant="transparent"
                      w="100%"
                      onClick={toggleColorMode}
                    >
                      Tema - {colorMode === "light" ? "Claro" : "Escuro"}
                    </Button>
                    <Button
                      leftIcon={<Icon mr={2} as={GoTextSize} />}
                      justifyContent="flex-start"
                      variant="transparent"
                      w="100%"
                      onClick={() =>
                        changeThemeFontSize(
                          activeFontSizeIndex === fontsSizesArray.length - 1
                            ? fontsSizesArray[0]
                            : fontsSizesArray[_.add(activeFontSizeIndex, 1)]
                        )
                      }
                    >
                      Tamanho da Fonte -{" "}
                      {fontSize === "md"
                        ? "Médio"
                        : fontSize === "lg"
                        ? "Grande"
                        : "Pequeno"}
                    </Button>
                  </Box>
                </Collapse>
              </Box>
            </DrawerBody>
          </DrawerContent>
        </DrawerOverlay>
      </Drawer>
    </>
  );
};

export default NavDrawer;
