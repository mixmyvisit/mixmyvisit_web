import React from "react";
import { Box, Flex, Text } from "@chakra-ui/react";

const Footer = () => {
  return (
    <Box p={6} as="footer" bg="grey.400">
      <Flex
        mx="auto"
        flexDir="row"
        flexWrap="wrap"
        align="center"
        justify="center"
        sx={{
          img: {
            height: "50px",
            width: "100px",
            objectFit: "contain",
            objectPosition: "center",
          },
        }}
      >
        <Box mx={[3, 6]} my={[4, 0]}>
          <a
            href="http://socialitv.web.ua.pt/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <img alt="Social iTV" src="/imgs/partners/logo-socialitv.png" />
          </a>
        </Box>
        <Box mx={[3, 6]} my={[4, 0]}>
          <a
            href="https://www.ua.pt/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <img
              alt="Universidade de Aveiro"
              src="/imgs/partners/logo-ua.png"
            />
          </a>
        </Box>
        <Box mx={[3, 6]} my={[4, 0]}>
          <a
            href="https://www.telecom.pt/en-us#"
            target="_blank"
            rel="noopener noreferrer"
          >
            <img alt="Altice" src="/imgs/partners/logo-altice.png" />
          </a>
        </Box>
      </Flex>
      <Text textAlign="center" fontSize="11px" pt={6}>
        MixMyVisit Copyright © {new Date().getFullYear()}
      </Text>
    </Box>
  );
};

export default Footer;
