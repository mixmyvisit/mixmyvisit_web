import React, { Component } from "react";
import { connect } from "react-redux";

export default (ChildComponent) => {
  class ComposedComponent extends Component {
    // Our component will render
    componentWillMount() {
      this._shouldNavigateAway();
    }

    // Our component just got rendered
    componentDidMount() {
      this._shouldNavigateAway();
    }

    _shouldNavigateAway() {
      if (!this.props.userData) {
        this.props.history.push("/");
      }
    }

    render() {
      return <ChildComponent {...this.props} />;
    }
  }

  function mapStateToProps(state) {
    return { userData: state.user.userSuccess };
  }

  return connect(mapStateToProps)(ComposedComponent);
};
