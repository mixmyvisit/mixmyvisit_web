import React from "react";
import PropTypes from "prop-types";
import { Helmet } from "react-helmet";

const SEO = ({ description, title }) => {
  return (
    <Helmet
      htmlAttributes={{ lang: "pt" }}
      title={title}
      titleTemplate={`${title} - MixMyVisit`}
      meta={[
        {
          name: `description`,
          content: description,
        },
        {
          property: `og:title`,
          content: title,
        },
        {
          property: `og:description`,
          content: description,
        },
        {
          property: `og:type`,
          content: `website`,
        },
      ]}
    />
  );
};

SEO.defaultProps = {
  lang: `pt`,
  description: `A solução MixMyVisit permite a criação de vídeos automáticos das
  visitas efetuadas a espaços culturais (ex. museus, parques,
  exposições, entre outros) com base nos percursos e locais
  visitados pelos utilizadores.`,
};

SEO.propTypes = {
  description: PropTypes.string,
  lang: PropTypes.string,
  title: PropTypes.string.isRequired,
};

export default SEO;
