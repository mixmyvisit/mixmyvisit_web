import React from "react";
import { IconButton } from "@chakra-ui/react";
import { FiDownload } from "react-icons/fi";

const EditButton = ({ destinyLink }) => {
  return (
    <a target="_blank" rel="noopener noreferrer" href={destinyLink}>
      <IconButton
        py={0}
        px={0}
        bg="white"
        isRound={true}
        position="absolute"
        boxShadow="default"
        transition="all 0.5s ease-in-out"
        boxSize="50px"
        top="5%"
        right="3%"
        _hover={{
          bg: "#bfbfbf",
        }}
        _active={{
          bg: "#bfbfbf",
        }}
      >
        <FiDownload size="38px" />
      </IconButton>
    </a>
  );
};

export default EditButton;
