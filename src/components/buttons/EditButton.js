import React from "react";
import { Link, useLocation } from "react-router-dom";
import { IconButton, Image } from "@chakra-ui/react";

const EditButton = ({ onClick, destinyLink }) => {
  const location = useLocation();
  return (
    <Link
      to={{
        pathname: destinyLink,
        state: { prevPath: location.pathname },
      }}
      onClick={() => (onClick ? onClick() : null)}
    >
      <IconButton
        py={0}
        px={0}
        bg="white"
        isRound={true}
        position="absolute"
        boxShadow="default"
        transition="all 0.5s ease-in-out"
        boxSize="50px"
        top="5%"
        right="3%"
        _hover={{
          bg: "#bfbfbf",
        }}
        _active={{
          bg: "#bfbfbf",
        }}
      >
        <Image
          htmlHeight="38px"
          htmlWidth="38px"
          alt="Edit"
          src={require("assets/icons/Edit.svg")}
        />
      </IconButton>
    </Link>
  );
};

export default EditButton;
