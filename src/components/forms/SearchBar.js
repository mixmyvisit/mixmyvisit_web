import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "redux/actions";
import { Input, Icon } from "@chakra-ui/react";
import { FaSearch } from "react-icons/fa";

class SearchBar extends Component {
  state = {
    query: "",
  };

  render() {
    let { query } = this.state;
    let { placeholder } = this.props;

    return (
      <div>
        <Input
          type="search"
          name="search"
          placeholder={placeholder}
          autoComplete="off"
          onKeyDown={this._search}
          onChange={this._onChange}
          value={query}
        />
        <div onClick={this._searchClick}>
          <Icon as={FaSearch} />
        </div>
      </div>
    );
  }

  _search = (e) => {
    if (e.keyCode === 13) {
      this.props.onSearch(this.state.query);
    }
  };

  _searchClick = () => {
    this.props.onSearch(this.state.query);
  };

  _onChange = (e) => {
    this.setState({
      query: e.target.value,
    });
  };
}

export default connect(null, actions)(SearchBar);
