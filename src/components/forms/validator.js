import validator from "validator";

export const required = (value) =>
  value ? undefined : "Este campo é obrigatório";

export const maxLength = (max, value) =>
  value && value.length > max
    ? `O valor inserido não pode exceder o máximo de ${max} caracteres.`
    : undefined;

export const number = (value) =>
  value && isNaN(Number(value)) ? "Tem de ser um número" : undefined;

export const minValue = (min, value) =>
  value && value.length < min
    ? `O valor inserido necessita de pelo menos ${min} caracteres.`
    : undefined;

export const email = (value) =>
  value && !validator.isEmail(value) && "Endereço de email inválido";

export const confirmPassword = (value, allValues) =>
  value !== allValues.password ? "Palavras-passes não são iguais" : undefined;
