import React, { Component } from "react";

import {
  Flex,
  IconButton,
  Icon,
  Image,
  Heading,
  Text,
  Input,
  InputGroup,
  InputLeftElement,
  FormControl,
  FormErrorMessage,
} from "@chakra-ui/react";
import { FiCheck, FiX } from "react-icons/fi";
import { MdTitle } from "react-icons/md";

import { maxLength, minValue } from "./validator";

class InputEdit extends Component {
  state = {
    inputValue: this.props.initialValue,
    editMode: false,
  };

  render() {
    const { editMode, inputValue } = this.state;
    const { name, initialValue, boldTitle } = this.props;

    const feedbackInvalid =
      maxLength(15, inputValue) === undefined &&
      minValue(8, inputValue) === undefined
        ? false
        : true;
    const feedbackInvalidMessage =
      maxLength(15, inputValue) || minValue(8, inputValue);

    return editMode ? (
      <Flex
        as="form"
        align="center"
        justify="center"
        onSubmit={() => {
          this.setState({ editMode: false });
          this.props.onSubmit(this.state.inputValue);
        }}
      >
        <FormControl isInvalid={feedbackInvalid}>
          <InputGroup size="md">
            <InputLeftElement
              p={0}
              pointerEvents="none"
              children={<MdTitle px={2} />}
            />
            <Input
              name={name}
              type="text"
              value={inputValue}
              isRequired={true}
              isInvalid={maxLength(inputValue)}
              placeholder="Insira um novo título"
              onChange={(ev) => this.setState({ inputValue: ev.target.value })}
            />
          </InputGroup>
          {feedbackInvalid && inputValue.length !== 0 && (
            <FormErrorMessage>{feedbackInvalidMessage}</FormErrorMessage>
          )}
        </FormControl>
        <IconButton
          px={0}
          ml={2}
          type="submit"
          variant="transparent"
          isRound={true}
          isDisabled={feedbackInvalid}
          icon={<Icon as={FiCheck} />}
        />
        <IconButton
          px={0}
          ml={2}
          variant="transparent"
          isRound={true}
          onClick={() =>
            this.setState({
              editMode: false,
              inputValue: this.props.initialValue,
            })
          }
          icon={<Icon as={FiX} />}
        />
      </Flex>
    ) : (
      <Flex justify="center" items="center" h="100%">
        <Flex justify="center" align="center">
          {boldTitle ? (
            <Heading pb={0} mr={2} as="h2">
              {initialValue}
            </Heading>
          ) : (
            <Text mr={2}>{initialValue}</Text>
          )}
        </Flex>
        <IconButton
          px={0}
          isRound={true}
          variant="transparent"
          onClick={() =>
            this.setState({
              editMode: true,
            })
          }
        >
          <Image
            alt="Edit title"
            src={require("assets/icons/Edit.svg")}
            h={8}
            objectFit="contain"
          />
        </IconButton>
      </Flex>
    );
  }
}

export default InputEdit;
