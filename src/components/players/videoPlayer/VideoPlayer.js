import React, { Component } from "react";
import ReactPlayer from "react-player";
import { Box, Flex } from "@chakra-ui/react";

import PlayerControlsEdition from "components/players/elements/PlayerControlsEdition";
// import PlayerLegendOverlay from "../elements/PlayerLegendOverlay";
import PlayerPlayButton from "../elements/PlayerPlayButton";

class VideoPlayer extends Component {
  visitVideoRef = React.createRef();

  state = {
    currentTime: 0,
    totalTime: 0,
    volume: 1,
    isPlaying: false,
  };

  render() {
    const { currentTime, totalTime, isPlaying, volume } = this.state;

    return (
      <Box w="100%" boxShadow="default" mb={8}>
        <Flex justify="center" align="center" position="relative">
          <ReactPlayer
            width="100%"
            height={this.props.height}
            ref={this.videoRef}
            url={this.props.source}
            controls={false}
            playing={isPlaying}
            volume={volume}
            onStart={this._play}
            onEnded={this._onEnded}
            onPlay={this._play}
            onPause={this._pause}
            onProgress={this._onProgress}
            onDuration={this._onDuration}
          />
          {!isPlaying && <PlayerPlayButton onClick={() => this._play()} />}
        </Flex>

        <PlayerControlsEdition
          onVolumeChange={(value) => this._onVolumeChange(value)}
          volume={volume}
          duration={totalTime}
          onPlay={this._play}
          isPlaying={isPlaying}
          onPause={this._pause}
          currentTime={currentTime}
          onSeek={this._onSeek}
        />
      </Box>
    );
  }

  _onProgress = (progress) => {
    this.setState({ currentTime: progress.playedSeconds });
  };

  _onDuration = (durationSeconds) => {
    this.setState({ totalTime: durationSeconds });
  };

  _onStalled = () => {
    this.setState({ isPlaying: false });
  };

  _onEnded = () => {
    if (this.props.onEnded) {
      this.props.onEnded();
    }
  };

  _pause = () => {
    this.setState({ isPlaying: false });
  };

  _play = () => {
    this.setState({ isPlaying: true });
  };

  _onSeek = (time) => {
    this.setState({ currentTime: time }, () => {
      this.videoRef.current.seekTo(time);
    });
  };

  _onVolumeChange = (value) => {
    this.setState({ volume: value });
  };
}

export default VideoPlayer;
