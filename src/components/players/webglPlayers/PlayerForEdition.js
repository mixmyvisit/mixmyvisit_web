import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import * as actions from "redux/actions";

import _ from "lodash";

import VideoContext from "videocontext/dist/videocontext";
import PlayerControlsEdition from "components/players/elements/PlayerControlsEdition";

class PlayerForEdition extends Component {
  ctx = null;
  audioNodes = [];

  state = {
    currentTime: 0,
    totalTime: 0,
    volume: 1,
    isPlaying: false
  };

  render() {
    const ctx = this.ctx;
    const { ctxData, showTrim, setCanvasRef } = this.props;
    const { currentTime, totalTime, isPlaying, volume } = this.state;

    return (
      <div className="player">
        <canvas
          ref={setCanvasRef}
          width="1280px"
          height="720px"
          id="playerCanvas"
          onClick={this._pauseStart}
        />
        <PlayerControlsEdition
          onVolumeChange={value => this._onVolumeChange(value)}
          volume={volume}
          duration={totalTime}
          onPlay={this._play}
          isPlaying={isPlaying}
          onPause={this._pause}
          currentTime={currentTime}
          onSeek={this._onSeek}
        />
      </div>
    );
  }

  componentDidUpdate(prevProps) {
    if (prevProps.audios != this.props.audios) {
      this._render();
    }
  }

  componentWillUnmount() {
    this.setState({
      currentTime: 0,
      isPlaying: false
    });
    this.ctx.pause();
    this.ctx.currentTime = 0;
    this.ctx.unregisterCallback(this._onUpdate);
    this.ctx.unregisterCallback(this._onEnded);
    this.ctx.unregisterCallback(this._onStalled);
  }

  componentDidMount() {
    this.ctx = new VideoContext(document.getElementById("playerCanvas"));
    this._render();
  }

  _render = () => {
    let { video, audios } = this.props;
    const ctx = this.ctx;

    ctx.reset();
    ctx.unregisterCallback(this._onUpdate);
    ctx.unregisterCallback(this._onEnded);
    ctx.unregisterCallback(this._onStalled);

    ctx.registerCallback("update", this._onUpdate);
    ctx.registerCallback("stalled", this._onStalled);
    ctx.registerCallback("ended", this._onEnded);

    let node = null;

    video.map(clip => {
      let duration = _.clone(ctx.duration);

      //if duration not set then null makes it have its duration, infinite if an image
      //if was trimed initial duration is maintained so trimEnd is used for calculation

      let stop = clip.trimEnd - clip.trimStart + duration || null;

      switch (clip.type) {
        //when defined the startFrom the videos will automatically start from that timestamp
        case "video":
          node = ctx.video(clip.url, clip.trimStart, 0.1);
          node.connect(ctx.destination);
          node.start(duration);
          node.stop(stop);
          clip.muted == true ? node.volume = 0 : node.volume = 1;
          break;
        default:
          break;
      }
    });

    _.each(audios, audio => {
      let node, maxMarkout;
      node = ctx.audio(audio.metaData.url);
      node.connect(ctx.destination);
      node.start(audio.metaData.start);
      node.stop(audio.metaData.start + audio.metaData.duration);
    });

    ctx.currentTime = this.state.currentTime || 0;

    this.setState({ totalTime: ctx.duration }, () => {
      this.props.onDuration(ctx.duration);
    });
  };

  _onUpdate = value => {
    let { onTimeUpdate } = this.props;

    if (value != this.state.currentTime) {
      this.setState(
        {
          currentTime: value,
          isPlaying: this._isPlaying()
        },
        () => {
          onTimeUpdate(value);
        }
      );
    }
  };

  _onStalled = value => {
    this.setState({
      isPlaying: false
    });
  };

  _onEnded = () => {
    this.ctx.currentTime = 0;
    this.setState({
      isPlaying: false
    });
  };

  _pauseStart = () => {
    if (!_.isEmpty(this.ctx)) {
      if (this._isPlaying()) {
        this.ctx.pause();
        this.setState({
          isPlaying: false
        });
      } else {
        this.ctx.play();
        this.setState({
          isPlaying: true
        });
      }
    }
  };

  _pause = () => {
    if (!_.isEmpty(this.ctx)) {
      this.ctx.pause();
    }
  };

  _play = () => {
    if (!_.isEmpty(this.ctx)) {
      this.ctx.play();
    }
  };

  _isPlaying = () => {
    return this.ctx.state == 0;
  };

  _onSeek = time => {
    this.ctx.currentTime = time;
  };

  _onVolumeChange = value => {
    this.ctx.volume = value;
    this.setState({ volume: value });
  };
}

export default compose(
  connect(
    null,
    actions
  )
)(PlayerForEdition);