import React from "react";
import { Box } from "@chakra-ui/react";

const PlayerLegendOverlay = ({ children, fontSize, position }) => {
  return (
    <Box
      p={2}
      position="absolute"
      color="white"
      bg="rgba(0, 0, 0, 0.5)"
      alignSelf="center"
      textAlign="center"
      fontSize={fontSize}
      bottom={position}
    >
      {children}
    </Box>
  );
};

export default PlayerLegendOverlay;
