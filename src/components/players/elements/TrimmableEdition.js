import React from "react";
import { Box } from "@chakra-ui/react";
import Slider from "rc-slider";

import createSliderWithTooltip from "components/players/HOC/createSliderWithTooltip";
const Range = createSliderWithTooltip(Slider.Range);

const TrimmableEdition = (props) => {
  const { min, max, trimStart, trimEnd } = props;

  const _onChange = (value) => {
    props.onTrim({
      trimStart: value[0],
      trimEnd: value[1],
    });
  };

  const _onBeforeChange = () => {
    let { onBeforeChange } = props;

    if (onBeforeChange) {
      onBeforeChange();
    }
  };

  const _onAfterChange = (value) => {
    let { duration, onAfterChange } = props;

    if (onAfterChange) {
      onAfterChange();
    }

    props.onTrimEnd({
      trimStart: (value[0] * duration) / 100,
      trimEnd: (value[1] * duration) / 100,
    });
  };

  const _formatTip = (value) => {
    let { duration, ratio } = props;

    value = (value * duration) / ratio;
    let minutes = Math.floor(value / 60);
    let seconds = Math.floor(value - minutes * 60);

    if (minutes < 10) {
      minutes = "0" + minutes;
    }
    if (seconds < 10) {
      seconds = "0" + seconds;
    }
    return minutes + ":" + seconds;
  };

  return (
    <Box
      as={Range}
      min={min}
      max={max}
      value={[trimStart, trimEnd]}
      tipFormatter={(value) => _formatTip(value)}
      onChange={_onChange}
      onBeforeChange={_onBeforeChange}
      onAfterChange={_onAfterChange}
      sx={{
        ".rc-slider-rail": {
          bgColor: "primary.100",
        },
        ".rc-slider-track": {
          bg: "primary.300",
        },
        ".rc-slider-handle": {
          top: "-1px",
          width: "15px",
          height: "15px",
          border: "none",
          transform: "scale(1)",
          bgColor: "primary.400",
          borderRadius: "50%",
          margin: 0,
        },
        ".rc-slider-handle:focus": {
          border: "none",
          boxShadow: "0 0 0 3px rgb(99 175 238 / 50%)",
        },
        ".rc-slider-handle:active": {
          boxShadow: "0 0 0 3px rgb(0 140 255 / 50%)",
          border: "none",
        },
      }}
    />
  );
};

export default TrimmableEdition;
