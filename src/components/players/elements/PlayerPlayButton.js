import React from "react";
import { Icon, IconButton } from "@chakra-ui/react";
import { FaPlay } from "react-icons/fa";

const PlayerPlayButton = ({ onClick }) => {
  return (
    <IconButton
      px={0}
      zIndex="3"
      isRound={true}
      color="primary.500"
      position="absolute"
      bg="rgba(255, 255, 255, 0.5)"
      h={["4.5rem", "5rem", "7rem"]}
      w={["4.5rem", "5rem", "7rem"]}
      onClick={() => onClick()}
      icon={<Icon h={[6, 8, 10]} w={[6, 8, 10]} as={FaPlay} />}
    />
  );
};

export default PlayerPlayButton;
