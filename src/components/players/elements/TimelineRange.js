import React from "react";

import Slider from "rc-slider";
import createSliderWithTooltip from "components/players/HOC/createSliderWithTooltip";
const Range = createSliderWithTooltip(Slider.Range);

const TimelineRange = (props) => {
  const _formatTip = (value) => {
    let { duration, ratio } = props;

    value = (value * duration) / ratio;
    let minutes = Math.floor(value / 60);
    let seconds = Math.floor(value - minutes * 60);

    if (minutes < 10) {
      minutes = "0" + minutes;
    }
    if (seconds < 10) {
      seconds = "0" + seconds;
    }
    return minutes + ":" + seconds;
  };

  return (
    <Range
      min={0}
      max={props.videoDuration}
      className={props.className}
      // allowCross={true}
      pushable={2}
      tipFormatter={(value) => _formatTip(value)}
      value={[props.trimStart, props.trimEnd]}
      onChange={(value) => {
        props.onTrim({
          trimStart: value[0],
          trimEnd: value[1],
        });
      }}
      onAfterChange={(value) =>
        props.onTrimEnd({
          trimStart: value[0],
          trimEnd: value[1],
        })
      }
    />
  );
};

export default TimelineRange;
