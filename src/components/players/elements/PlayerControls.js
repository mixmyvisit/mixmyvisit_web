import React from "react";
import {
  Box,
  Flex,
  IconButton,
  Slider,
  SliderTrack,
  SliderFilledTrack,
  SliderThumb,
} from "@chakra-ui/react";
import { FaPause, FaPlay, FaVolumeMute, FaVolumeUp } from "react-icons/fa";

const PlayerControlsEdition = (props) => {
  const {
    onSeekStart,
    onSeek,
    onSeekEnd,
    onVolumeChange,
    volume,
    currentTime,
    duration,
    isPlaying,
    onPlay,
    onPause,
  } = props;

  const _play = () => {
    onPlay();
  };

  const _pause = () => {
    onPause();
  };

  const _onSeek = (value) => {
    onSeek((value * duration) / 100);
  };

  const _formatTime = () => {
    function str_pad_left(string, pad, length) {
      return (new Array(length + 1).join(pad) + string).slice(-length);
    }

    return (
      <Box>
        {isNaN(currentTime)
          ? "00"
          : str_pad_left(Math.floor(currentTime / 60), "0", 2)}
        :
        {isNaN(currentTime)
          ? "00"
          : str_pad_left(Math.floor(currentTime % 60), "0", 2)}
        /
        {isNaN(currentTime)
          ? "00"
          : str_pad_left(Math.floor(duration / 60), "0", 2)}
        :
        {isNaN(currentTime)
          ? "00"
          : str_pad_left(Math.floor(duration % 60), "0", 2)}
      </Box>
    );
  };

  const setTimeline = () => {
    return (
      <Slider
        onFocus={onSeekStart}
        onChange={(value) => _onSeek(value)}
        onChangeEnd={onSeekEnd}
        duration={duration}
        value={(currentTime * 100) / duration || 0}
        min={0}
        max={100}
        defaultValue={0}
      >
        <SliderTrack bg="primary.100">
          <SliderFilledTrack bg="primary.300" />
        </SliderTrack>
        <SliderThumb bg="primary.400" />
      </Slider>
    );
  };

  return (
    <Box position="relative" bg="grey.600" px={4} py={2} zIndex="1">
      {setTimeline()}
      <Flex justify="flex-start" flexWrap="wrap" align="center">
        {isPlaying ? (
          <IconButton
            icon={<FaPause />}
            variant="transparent"
            color="white"
            mr={2}
            onClick={() => _pause()}
          />
        ) : (
          <IconButton
            icon={<FaPlay />}
            variant="transparent"
            color="white"
            mr={2}
            onClick={() => _play()}
          />
        )}

        <IconButton
          mr={2}
          color="white"
          variant="transparent"
          icon={volume === 0 ? <FaVolumeMute /> : <FaVolumeUp />}
          onClick={() => (volume === 0 ? onVolumeChange(1) : onVolumeChange(0))}
        />

        <Slider
          w={12}
          mr={6}
          min={0}
          max={100}
          defaultValue={100}
          value={volume * 100}
          onChange={(value) => onVolumeChange(value / 100)}
        >
          <SliderTrack bg="primary.100">
            <SliderFilledTrack bg="primary.300" />
          </SliderTrack>
          <SliderThumb bg="primary.400" />
        </Slider>
        <Box color="white">{_formatTime()}</Box>
      </Flex>
    </Box>
  );
};

export default PlayerControlsEdition;
