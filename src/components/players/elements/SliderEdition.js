import React, { Component } from "react";
import { Box } from "@chakra-ui/react";
import Slider from "rc-slider";

class SliderEdition extends Component {
  selectedHandle = null;

  render() {
    const { min, max, trimEnd, className } = this.props;

    return (
      <Box
        as={Slider}
        min={min}
        max={max}
        className={className}
        value={trimEnd}
        tipFormatter={(value) => this._formatTip(value)}
        onChange={this._onChange}
        onBeforeChange={this._onBeforeChange}
        onAfterChange={this._onAfterChange}
        sx={{
          ".rc-slider-rail": {
            bgColor: "primary.100",
          },
          ".rc-slider-track": {
            bg: "primary.300",
          },
          ".rc-slider-handle": {
            top: "-1px",
            width: "15px",
            height: "15px",
            border: "none",
            transform: "scale(1)",
            bgColor: "primary.400",
            borderRadius: "50%",
            margin: 0,
          },
          ".rc-slider-handle:focus": {
            border: "none",
            boxShadow: "0 0 0 3px rgb(99 175 238 / 50%)",
          },
          ".rc-slider-handle:active": {
            boxShadow: "0 0 0 3px rgb(0 140 255 / 50%)",
            border: "none",
          },
        }}
      />
    );
  }

  _onChange = (value) => {
    this.props.onTrim({
      trimStart: 0,
      trimEnd: value,
    });
  };

  _onBeforeChange = (value) => {
    let { onBeforeChange } = this.props;

    if (onBeforeChange) {
      onBeforeChange();
    }
  };

  _onAfterChange = (value) => {
    let { duration, onAfterChange } = this.props;

    if (onAfterChange) {
      onAfterChange();
    }

    this.props.onTrimEnd({
      trimStart: 0,
      trimEnd: (value * duration) / 100,
    });
  };

  _formatTip = (value) => {
    let { duration, ratio } = this.props;

    value = (value * duration) / ratio;
    let minutes = Math.floor(value / 60);
    let seconds = Math.floor(value - minutes * 60);

    if (minutes < 10) {
      minutes = "0" + minutes;
    }
    if (seconds < 10) {
      seconds = "0" + seconds;
    }
    return minutes + ":" + seconds;
  };
}

export default SliderEdition;
