import React, { useState, useEffect } from "react";
import { Prompt } from "react-router-dom";
import {
  Button,
  AlertDialog,
  AlertDialogBody,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogContent,
  AlertDialogOverlay,
} from "@chakra-ui/react";

const ConfirmationLeaveModal = ({ when, navigate, shouldBlockNavigation }) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [lastLocation, setLastLocation] = useState(null);
  const [confirmedNavigation, setConfirmedNavigation] = useState(false);

  const closeModal = () => {
    setModalVisible(false);
  };

  const handleBlockedNavigation = (nextLocation) => {
    if (!confirmedNavigation && shouldBlockNavigation) {
      setModalVisible(true);
      setLastLocation(nextLocation);
      return false;
    }
    return true;
  };

  const handleConfirmNavigationClick = () => {
    setModalVisible(false);
    setConfirmedNavigation(true);
  };

  useEffect(() => {
    if (confirmedNavigation && lastLocation) {
      navigate(lastLocation.pathname);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [confirmedNavigation, lastLocation]);

  return (
    <>
      <Prompt when={when} message={handleBlockedNavigation} />
      <AlertDialog
        isCentered
        motionPreset="scale"
        isOpen={modalVisible}
        closeOnEsc={true}
        blockScrollOnMount={false}
        closeOnOverlayClick={true}
        scrollBehavior="inside"
        onClose={closeModal}
      >
        <AlertDialogOverlay>
          <AlertDialogContent borderRadius={6}>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Sair do editor?
            </AlertDialogHeader>

            <AlertDialogBody>
              Tem mudanças que não foram guardadas. Tem a certeza que quer sair
              sem guardar primeiro as suas alterações?
            </AlertDialogBody>

            <AlertDialogFooter>
              <Button variant="transparent" onClick={closeModal}>
                Cancelar
              </Button>
              <Button
                colorScheme="red"
                onClick={handleConfirmNavigationClick}
                ml={3}
              >
                Sair
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    </>
  );
};

export default ConfirmationLeaveModal;
