import React from "react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalFooter,
  ModalBody,
  Button,
  Heading,
  Text,
} from "@chakra-ui/react";
import { Link } from "react-router-dom";
import VideoPlayer from "components/players/videoPlayer/VideoPlayer";
import VisitShare from "components/feedback/VisitShare";

const VisitVideoModalPreview = (props) => {
  const { videoSrc, title, isModalOpen, toggleModal } = props;

  return (
    <Modal
      motionPreset="scale"
      onClose={toggleModal}
      isOpen={isModalOpen}
      scrollBehavior="inside"
      isCentered
      maxWidth="55rem"
    >
      <ModalOverlay>
        <ModalContent>
          <ModalBody p={6} textAlign="center">
            <Heading as="h2">{title}</Heading>
            {videoSrc ? (
              <>
                <VideoPlayer source={videoSrc} />
                <Button>Download da Visita</Button>
                <VisitShare url={videoSrc} title={title} />
              </>
            ) : (
              <>
                <Heading as="h4">
                  A sua visita ainda não foi renderizada.
                </Heading>
                <Text>
                  Para visualizar a sua visita nesta <i>preview</i> necessita de
                  renderizá-la primeiro. Para tal tem de realizar esse processo
                  ou pelo nosso{" "}
                  <a
                    target="_blank"
                    rel="noopener noreferrer"
                    href="http://m.me/Mixmyvisit"
                  >
                    bot no Facebook Messenger
                  </a>{" "}
                  ou no <Link to={`/editor/${title}`}>editor dessa visita</Link>
                </Text>
              </>
            )}
          </ModalBody>

          <ModalFooter>
            <Button variant="primary" mr={2} onClick={toggleModal}>
              Fechar
            </Button>
          </ModalFooter>
        </ModalContent>
      </ModalOverlay>
    </Modal>
  );
};

export default VisitVideoModalPreview;
