import React from "react";
import {
  Text,
  Button,
  AlertDialog,
  AlertDialogBody,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogContent,
  AlertDialogOverlay,
  Image,
} from "@chakra-ui/react";

const WarningRenderModal = ({ isModalOpen, handleClose }) => {
  return (
    <AlertDialog
      motionPreset="scale"
      blockScrollOnMount={false}
      closeOnOverlayClick={true}
      closeOnEsc={true}
      scrollBehavior="inside"
      isCentered={true}
      isOpen={isModalOpen}
      onClose={handleClose}
    >
      <AlertDialogOverlay>
        <AlertDialogContent borderRadius={6} p={4}>
          <AlertDialogHeader fontSize="lg" fontWeight="bold">
            <span>
              Não é possível enviar esta visita para o <i>render</i>
            </span>
          </AlertDialogHeader>

          <AlertDialogBody>
            <Image
              mb={8}
              h={40}
              mx="auto"
              objectFit="contain"
              alt="Informação"
              src={require("assets/icons/Info.svg")}
            />
            <Text>
              A sua visita ainda está a decorrer. Caso pretenda enviá-la para o
              nosso <i>render</i> necessitará de fazê-lo{" "}
              <b>
                pelo nosso Bot no Facebook Messenger com o comando -
                !concluir_visita -{" "}
              </b>{" "}
              ou então a partir da{" "}
              <b>
                nossa aplicação mobile após passar a pulseira associada NFC com
                esta pulseira
              </b>{" "}
              no dispositivo onde a aplicação encontra-se a ser disposta.
            </Text>
            <Text>
              Só poderá enviar a sua visita para o render a partir do nosso
              editor quando a sua visita já não estiver ativa.
            </Text>
          </AlertDialogBody>

          <AlertDialogFooter>
            <Button variant="primary" onClick={handleClose}>
              Fechar
            </Button>
          </AlertDialogFooter>
        </AlertDialogContent>
      </AlertDialogOverlay>
    </AlertDialog>
  );
};

export default WarningRenderModal;
