import React, { Component } from "react";
import {
  Box,
  Heading,
  Text,
  Button,
  AlertDialog,
  AlertDialogBody,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogContent,
  AlertDialogOverlay,
} from "@chakra-ui/react";
import Loader from "components/feedback/Loader";

class ConfirmationRenderModal extends Component {
  render() {
    const {
      isModalLoading,
      isModalOpen,
      handleClose,
      handleConfirmation,
      updateTypeItem,
      isRender,
    } = this.props;
    const controlCloseModal = !isModalLoading ? true : false;

    return (
      <AlertDialog
        motionPreset="scale"
        isOpen={isModalOpen}
        blockScrollOnMount={false}
        closeOnOverlayClick={controlCloseModal}
        closeOnEsc={controlCloseModal}
        scrollBehavior="inside"
        isCentered={true}
        onClose={handleClose}
      >
        <AlertDialogOverlay>
          {isModalLoading ? (
            <AlertDialogContent borderRadius={6}>
              <AlertDialogBody textAlign="center" p={8}>
                <Box mb={6}>
                  <Loader />
                </Box>
                <Heading pb={2} as="h3">
                  {isRender ? (
                    <>
                      A enviar visita para o <i>render</i>...
                    </>
                  ) : (
                    <>
                      A atualizar{" "}
                      {updateTypeItem === "visit"
                        ? "visita"
                        : updateTypeItem === "image"
                        ? "imagem"
                        : updateTypeItem}
                      ...
                    </>
                  )}
                </Heading>
                <Text>Por favor não recarregue a página</Text>
              </AlertDialogBody>
            </AlertDialogContent>
          ) : (
            <AlertDialogContent borderRadius={6}>
              <AlertDialogHeader fontSize="lg" fontWeight="bold">
                {isRender
                  ? `Renderizar visita`
                  : `Atualizar ${
                      updateTypeItem === "visit"
                        ? "visita"
                        : updateTypeItem === "image"
                        ? "imagem"
                        : updateTypeItem
                    }?`}
              </AlertDialogHeader>

              <AlertDialogBody>
                {isRender
                  ? `As alterações da sua visita serão guardadas (não poderá reverter estas alterações) e, posteriormente, o processo de renderização da sua visita irá começar. Este processo pode demorar alguns minutos, sendo que será notificado via bot Messenger e email quando o processo terminar`
                  : `Irá perder todo o progresso do ficheiro - ${updateTypeItem} - de
                como era no passado (não poderá reverter estas alterações).`}
              </AlertDialogBody>

              <AlertDialogFooter>
                <Button variant="transparent" onClick={handleClose}>
                  Cancelar
                </Button>
                <Button colorScheme="green" onClick={handleConfirmation} ml={3}>
                  {isRender ? "Enviar" : "Atualizar"}
                </Button>
              </AlertDialogFooter>
            </AlertDialogContent>
          )}
        </AlertDialogOverlay>
      </AlertDialog>
    );
  }
}

export default ConfirmationRenderModal;
