import React, { useState } from "react";
import Loader from "components/feedback/Loader";

import {
  Box,
  Modal,
  Text,
  ModalOverlay,
  ModalContent,
  ModalBody,
  Heading,
} from "@chakra-ui/react";

const UploadStatusModal = (props) => {
  const [state, setState] = useState(false);

  const _toggleModal = () => {
    setState(!state);
  };

  return (
    <Modal
      isOpen={props.isModalOpen}
      motionPreset="fadeIn"
      onClose={() => _toggleModal}
      closeOnEsc={false}
      closeOnOverlayClick={false}
      blockScrollOnMount={false}
      scrollBehavior="inside"
      isCentered={true}
    >
      <ModalOverlay>
        <ModalContent>
          <ModalBody
            p={8}
            bg="white"
            borderRadius={6}
            textAlign="center"
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
          >
            <Box mb={4}>
              <Loader />
            </Box>
            <Heading pb={2} mb={0} as="h3">
              A realizar o upload...
            </Heading>
            <Text pb={2}>
              Este processo pode demorar alguns segundos ou até minutos
              dependendo da sua conexão à internet e do tamanho da imagem/vídeo
              que inseriu
            </Text>
            <Text>Por favor aguarde e não recarregue a página</Text>
          </ModalBody>
        </ModalContent>
      </ModalOverlay>
    </Modal>
  );
};

export default UploadStatusModal;
