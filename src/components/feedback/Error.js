import React from "react";
import { Link, useLocation } from "react-router-dom";
import {
  Flex,
  Stack,
  Button,
  Image,
  Icon,
  Heading,
  Text,
} from "@chakra-ui/react";
import { FiRotateCw } from "react-icons/fi";
import SEO from "components/Seo";

const Error = () => {
  const location = useLocation();

  return (
    <Flex h="100%" w="100%" align="center" justify="center" flexDir="column">
      <SEO title="Erro" />
      <Image
        w={24}
        h="auto"
        alt="Erro"
        src={require(`assets/icons/ErrorVisits.svg`)}
      />
      <Heading as="h1" py={4}>
        Ocorreu um erro!
      </Heading>
      <Text textAlign="center" pb={8}>
        Algo correu mal, por favor tente mais tarde ou se o problema persistir
        contacte-nos.
      </Text>
      <Stack justifyContent="center" direction="column" spacing={4}>
        {location.pathname !== "/" && (
          <Flex justify="center" as={Link} to="/">
            <Button
              display="flex"
              variant="solid"
              alignItems="center"
              leftIcon={
                <Image
                  alt="Voltar"
                  src={require("assets/icons/Back.svg")}
                  objectFit="contain"
                  mr={2}
                  h={5}
                />
              }
            >
              Voltar à Home
            </Button>
          </Flex>
        )}
        <Button
          display="flex"
          alignItems="center"
          variant="primary"
          leftIcon={<Icon mr={2} as={FiRotateCw} />}
          onClick={() => window.location.reload()}
        >
          Recarregar a página
        </Button>
      </Stack>
    </Flex>
  );
};

export default Error;
