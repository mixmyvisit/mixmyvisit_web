import React from 'react'
import { ToastContainer } from 'react-toastify'
import { useWindowSize } from 'react-use'

const ToastProvider = () => {
  const { width } = useWindowSize()

  return (
    <ToastContainer
      position={width > 768 ? "top-right" : "bottom-center"}
      autoClose={4000}
      newestOnTop={true}
      closeOnClick
      rtl={false}
      pauseOnFocusLoss
      pauseOnHover
    />
  )
}

export default ToastProvider
