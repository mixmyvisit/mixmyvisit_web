import React from "react";

import {
  Box,
  HStack,
  Button,
  Icon,
  Heading,
  IconButton,
} from "@chakra-ui/react";
import {
  FacebookShareButton,
  TwitterShareButton,
  TelegramShareButton,
  WhatsappShareButton,
} from "react-share";
import { FiLink } from "react-icons/fi";
import {
  FaFacebook,
  FaTelegramPlane,
  FaTwitter,
  FaWhatsapp,
} from "react-icons/fa";

import PrintToast from "components/feedback/printToast";

const Share = (props) => {
  const { url, title } = props;

  const _copyURL = (text) => {
    let textArea = document.createElement("textarea");
    textArea.value = text;
    document.body.appendChild(textArea);
    textArea.focus();
    textArea.select();

    try {
      const successful = document.execCommand("copy");
      successful &&
        PrintToast("success", "O URL da sua Visita foi copiado com sucesso");
    } catch (err) {
      PrintToast(
        "error",
        "Ocorreu um erro e não foi possível copiar o URL da sua visita"
      );
    }
    document.body.removeChild(textArea);
  };

  return (
    <Box textAlign="center">
      <Heading pb={2} as="h4">
        Partilhe a sua visita:
      </Heading>
      <HStack wrap="wrap" align="center" justify="center" spacing={4}>
        <IconButton
          as={FacebookShareButton}
          isRound
          variant="transparent"
          display="flex"
          align="center"
          hashtag="#MixMyVisit"
          quote={`A minha visita personalizada - ${title} - realizada a partir da aplicação MixMyVisit`}
          resetButtonStyle={false}
          url={url}
        >
          <Icon as={FaFacebook} color="#4267B2" boxSize={6} />
        </IconButton>

        <IconButton
          as={TwitterShareButton}
          isRound
          variant="transparent"
          display="flex"
          align="center"
          title={title}
          url={url}
          resetButtonStyle={false}
          via="https://mixmyvisit.web.ua.pt"
          hashtags={["#MixMyVisit", "#Visita"]}
        >
          <Icon as={FaTwitter} color="#1DA1F2" boxSize={6} />
        </IconButton>

        <IconButton
          as={WhatsappShareButton}
          isRound
          variant="transparent"
          display="flex"
          align="center"
          title={title}
          url={url}
          resetButtonStyle={false}
        >
          <Icon as={FaWhatsapp} color="#25D366" boxSize={6} />
        </IconButton>

        <IconButton
          as={TelegramShareButton}
          isRound
          variant="transparent"
          display="flex"
          align="center"
          title={title}
          url={url}
          resetButtonStyle={false}
        >
          <Icon as={FaTelegramPlane} color="#0088CC" boxSize={6} />
        </IconButton>

        <Button
          variant="transparent"
          leftIcon={<Icon as={FiLink} />}
          onClick={() => _copyURL(url)}
        >
          <span>
            Copiar URL de <i>download</i> da Visita
          </span>
        </Button>
      </HStack>
    </Box>
  );
};

export default Share;
