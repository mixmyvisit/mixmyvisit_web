import React from "react";
import { CircularProgress, Flex, Heading } from "@chakra-ui/react";

const Loading = () => {
  return (
    <Flex
      w="100%"
      h="100%"
      display="flex"
      flexDir="column"
      justify="center"
      align="center"
      sx={{
        svg: {
          fill: "primary.500",
          height: 40,
          width: 40,
        },
      }}
    >
      <CircularProgress
        thickness="5px"
        capIsRound={true}
        isIndeterminate
        trackColor="primary.100"
        color="primary.500"
      />

      <Heading as="h4" mt={8} textAlign="center">
        A carregar ...
      </Heading>
    </Flex>
  );
};

export default Loading;
