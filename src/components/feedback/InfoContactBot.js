import React from "react";
import { Link } from "react-router-dom";
import { Text } from "@chakra-ui/react";

const InfoContactBot = () => {
  return (
    <Text>
      Pode começar uma visita ao mandar uma mensagem ao nosso Bot no Facebook
      Messenger ou ao obter uma pulseira na receção do local/museu,{" "}
      <Link to="/sobre">
        Carregue aqui para ver as intruções com mais detalhes na secção de FAQ
        (questões frequentemente perguntadas) na página de Sobre
      </Link>
    </Text>
  );
};

export default InfoContactBot;
