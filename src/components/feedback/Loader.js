import React from "react";
import { CircularProgress } from "@chakra-ui/react";

const Loader = () => {
  return (
    <CircularProgress
      isIndeterminate
      thickness="5px"
      color="primary.500"
      trackColor="primary.100"
    />
  );
};

export default Loader;
