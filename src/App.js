import React, { useContext } from "react";
import { connect } from "react-redux";
import { useLocation } from "react-router-dom";
import { useTransition, animated } from "react-spring";

import Routes from "routes/routes";
import ScrollToTop from "routes/ScrollToTop";

import { ThemeContext } from "theme/ThemeContext";
import { ChakraProvider, Box, Grid, Flex, Image } from "@chakra-ui/react";

import Header from "components/layout/header/Header";
import Footer from "components/layout/Footer";
import ToastProvider from "components/feedback/ToastProvider";

import "rc-slider/assets/index.css";
import "react-toastify/dist/ReactToastify.min.css";
import "@blueprintjs/datetime/lib/css/blueprint-datetime.css";

const MainContainer = animated(Box);

const App = ({ userData }) => {
  const { mmvTheme } = useContext(ThemeContext);

  // Transition between routes
  const location = useLocation();

  const transitions = useTransition(location, (location) => location.pathname, {
    from: { position: "absolute", opacity: 0, display: "none" },
    enter: { position: "static", opacity: 1, display: "block" },
    leave: { position: "absolute", opacity: 0, display: "none" },
  });

  // determine if the current route has a /redirect/ if so do not show header
  const splitRoute = location.pathname.split("/");
  const isRedirectRoute = splitRoute[1].includes("redirect");

  return (
    <ChakraProvider resetCSS={true} theme={mmvTheme}>
      <ScrollToTop />
      <ToastProvider />
      <Grid
        minH="100vh"
        gridTemplateRows={"auto 1fr auto"}
        gridTemplateColumns="100%"
      >
        {!isRedirectRoute ? (
          <Header />
        ) : (
          <Flex p={8} justify="center" align="center" bg="grey.400">
            <Image
              w={[32, 40]}
              objectFit="contain"
              alt="Mix My Visit Logo"
              src="/logo/png/mmv-logo-with-letters-small.png"
            />
          </Flex>
        )}
        {transitions.map(({ item, props, key }) => (
          <MainContainer
            key={key}
            style={props}
            py={userData && !isRedirectRoute ? [12, 20] : 0}
            px={userData && !isRedirectRoute ? [8, 16, 20, 56, 60] : 0}
          >
            <Routes location={item} />
          </MainContainer>
        ))}
        <Footer />
      </Grid>
    </ChakraProvider>
  );
};

function mapStateToProps(state) {
  return {
    userData: state.user.userSuccess,
  };
}

export default connect(mapStateToProps, null)(App);
