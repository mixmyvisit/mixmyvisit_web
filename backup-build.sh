#!/bin/bash
date=$(date +%Y%m%d_%H%M%S)
path=/var/jenkins_home/workspace/builds
backup_dir=build_$date
backup_fullpath="${path}/${backup_dir}"
mkdir -p $backup_fullpath
cp -rp "${path}/build" $backup_fullpath
echo "Backup completed | ${path}/build => $backup_fullpath"